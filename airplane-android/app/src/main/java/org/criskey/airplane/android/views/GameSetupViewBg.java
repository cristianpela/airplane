package org.criskey.airplane.android.views;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.game.GameSetupView;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.Map;

/**
 * Created by criskey on 17/11/2016.
 */
public class GameSetupViewBg extends BackgroundViewAdapter implements GameSetupView {

    public GameSetupViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void reset() {}

}
