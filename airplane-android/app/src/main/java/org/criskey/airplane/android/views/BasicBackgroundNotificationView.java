package org.criskey.airplane.android.views;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.android.MainActivity;
import org.criskey.airplane.android.receivers.ActionNotificationReceiver;
import org.criskey.airplane.common.local.router.Router;

import java.util.Map;

/**
 * Created by criskey on 1/12/2016.
 */
public class BasicBackgroundNotificationView implements BackgroundView {

    public static int BASE_ID = 1337;

    private int notificationId;

    private NotificationManager notificationManager;

    private NotificationCompat.Builder notificationBuilder;

    private PowerManager powerManager;

    private AirplaneContext context;

    private Router.Screen screen;

    public BasicBackgroundNotificationView(AirplaneContext context, Router.Screen screen, String title) {
        this.screen = screen;
        this.context = context;
        powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle(title)
                .setAutoCancel(true);
        // .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
    }

    private void showNotification(String contentInfo) {
        tryWakeUpScreen();
        //send notification
        Intent intent = MainActivity.createIntent(context, screen);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, nextId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = notificationBuilder
                .setContentInfo(contentInfo)
                .setContentIntent(pendingIntent)
                .build();

        notificationId = nextId();
        notificationManager.notify(notificationId, notification);
    }


    @Override
    public void showProgressMessage(int progress, int max, String endProgressMessage) {
        tryWakeUpScreen();
        if (progress != max) {
            notificationBuilder.setProgress(max, progress, false);
        } else {
            notificationBuilder.setContentText(endProgressMessage);
        }
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    @Override
    public void showActionMessage(String contentText, ActionNotificationInfo... infoArray) {
        tryWakeUpScreen();
        notificationId = nextId();
        //android versions lower than sdk 16 don't support actions;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            int broadcastId = nextId();
            notificationBuilder.setContentText(contentText);
            for (ActionNotificationInfo info : infoArray) {
                Intent intent = ActionNotificationReceiver.createIntent(info.action, false, info.extrasToPass);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, broadcastId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notificationBuilder.addAction(info.drawableId, context.i18n(info.action), pendingIntent);
            }
        } else {
            showNotification(contentText);
        }
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    @Override
    public void showMessage(String message, boolean pop) {
        showNotification(message);
    }

    private int nextId() {
        return BASE_ID++;
    }

    private void tryWakeUpScreen() {
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = powerManager.isInteractive();
        } else {
            isScreenOn = powerManager.isScreenOn();
        }

        //wake up screen & vibrate
        if (!isScreenOn) {
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(
                    PowerManager.FULL_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP
                            | PowerManager.ON_AFTER_RELEASE,
                    "PowerWakeLock");
            wakeLock.acquire(5000);
            PowerManager.WakeLock wakeLockCPU = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PowerCPULock");
            wakeLockCPU.acquire(5000);
            //TODO: re-enable vibration
            //notificationBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        }
    }

    //Unimplemented methods
    @Override
    public void onResume() {}

    @Override
    public void onPause() {}

    @Override
    public void showError(String s, boolean b) {}

    @Override
    public void restore(Map<String, ?> map) {}

}
