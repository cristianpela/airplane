package org.criskey.airplane.android.views;

import org.criskey.airplane.common.local.JoinView;

import java.util.Map;

/**
 * Created by criskey on 29/11/2016.
 */
public class JoinViewBg extends BackgroundViewAdapter implements JoinView {

    public JoinViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void onWait() {

    }

    @Override
    public void onJoined() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void showMessage(String s, boolean b) {

    }

    @Override
    public void showError(String s, boolean b) {

    }

    @Override
    public void restore(Map<String, ?> map) {

    }
}
