package org.criskey.airplane.android.router;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.criskey.airplane.android.fragments.FragmentFactory;
import org.criskey.airplane.common.local.router.Router;


/**
 * Created by criskey on 25/10/2016.
 */
public class FragmentRouter implements Router<Fragment> {

    private FragmentManager fragmentManager;

    private Router decorator;

    private int containerId;

    private FragmentFactory fragmentFactory;

    private RouteChangeListener routeChangeListener;

    public FragmentRouter(Router decorator) {
        this.decorator = decorator;
        fragmentFactory = new FragmentFactory();
    }

    public void setFragmentManager(FragmentManager fragmentManager, @IdRes int containerId, RouteChangeListener routeChangeListener) {
        this.fragmentManager = fragmentManager;
        this.containerId = containerId;
        this.routeChangeListener = routeChangeListener;
    }


    @Override
    public Screen show(Screen fromScreen, Screen toScreen, boolean bypassFilters) {
        Screen decoScreen = (decorator != null && !bypassFilters) ? decorator.show(fromScreen, toScreen, bypassFilters) : toScreen;
        if (fragmentManager != null) {
            Fragment fragment = fragmentManager.findFragmentByTag(decoScreen.toString());
            if (fragment == null) {
                fragment = (Fragment) fragmentFactory.createFragment(decoScreen);
            }
            if (fragment != null) {
                if (!fragment.isAdded()) {
                    //state is managed by presenters so it's safe to use commitAllowingStateLoss() in this case
                    fragmentManager.beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(containerId, fragment, decoScreen.toString())
                            .commitAllowingStateLoss();
                }
            }
        }
        if (routeChangeListener != null)
            routeChangeListener.onRouteChange(decoScreen);
        return decoScreen;
    }

    @Override
    public void hide(Screen screen) {
        //no-op
    }

    @Override
    public void addScreen(Screen screen, Fragment fragment) {
        //no-op
    }
}
