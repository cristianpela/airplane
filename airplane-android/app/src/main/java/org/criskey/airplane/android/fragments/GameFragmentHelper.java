package org.criskey.airplane.android.fragments;

import android.view.View;
import android.widget.Button;

import org.criskey.airplane.android.R;
import org.criskey.airplane.android.views.AndroidPlayerView;
import org.criskey.airplane.common.local.game.GamePresenter;

/**
 * Created by criskey on 17/11/2016.
 */
public final class GameFragmentHelper {

    public static void assist(View container, final GamePresenter presenter) {
        final View btnScaleUp = container.findViewById(R.id.btn_scale_up);
        final View btnScaleDown = container.findViewById(R.id.btn_scale_down);

        if (btnScaleUp != null && btnScaleDown != null) {
            final AndroidPlayerView mPlayerView = (AndroidPlayerView) container.findViewById(R.id.player_view);

            btnScaleUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPlayerView.scale(true);
                }
            });
            btnScaleDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPlayerView.scale(false);
                }
            });
        }
        View btnLeave = container.findViewById(R.id.btn_leave_session);
        if (btnLeave != null)
            btnLeave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.leaveSession();
                }
            });
    }

}
