package org.criskey.airplane.android.router;

import org.criskey.airplane.common.local.router.Router;

/**
 * Created by criskey on 16/11/2016.
 */
public interface RouteChangeListener {

    public void onRouteChange(Router.Screen screen);

}
