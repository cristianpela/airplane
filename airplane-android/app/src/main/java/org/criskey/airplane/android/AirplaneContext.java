package org.criskey.airplane.android;

import android.content.Context;
import android.content.ContextWrapper;

import org.criskey.airplane.android.i18n.AndroidLanguageSupport;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;

import java.util.Locale;

/**
 * Created by criskey on 7/12/2016.
 */
public class AirplaneContext extends ContextWrapper {

    public static final String LANGUAGE_SUPPORT_SERVICE = "LANGUAGE_SUPPORT_SERVICE";

    private LanguageSupport languageSupport;

    public AirplaneContext(Context base) {
        super(base);
        languageSupport = new AndroidLanguageSupport(new Locale("ro", "RO"), base);
    }

    public Object getCustomService(String name) {
        if (name == LANGUAGE_SUPPORT_SERVICE)
            return languageSupport;
        return null;
    }

    public LanguageSupport getLanguageSupport() {
        return languageSupport;
    }

    public String i18n(String key, Object... params) {
        return languageSupport.i18n(key, params);
    }

    public <P extends Presentable> P getPresenter(Class<P> clazz) {
        return PresenterManager.plugReflect(clazz);
    }
}
