package org.criskey.airplane.android.views;

import android.content.Context;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.game.GameHitHistoryView;
import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 2/12/2016.
 */
public class GameHitHistoryViewBg extends BackgroundViewAdapter implements GameHitHistoryView {


    public GameHitHistoryViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void incomingBlow(Shot shot) {
        showMessage("You were shot at: " + shot.toString(), true);
    }

    @Override
    public void setConfiguration(PlaneConfiguration planeConfiguration) {
    }
}
