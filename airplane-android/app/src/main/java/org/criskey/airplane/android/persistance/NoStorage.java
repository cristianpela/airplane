package org.criskey.airplane.android.persistance;

import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.util.AsyncCallback;

/**
 * Created by criskey on 5/12/2016.
 */
public class NoStorage implements Storage {

    @Override
    public <F> void save(Prefix prefix, String s, F f) {

    }

    @Override
    public <F> void load(Prefix prefix, String s, AsyncCallback<F> asyncCallback) {

    }

    @Override
    public void wipe(Prefix prefix) {

    }

    @Override
    public void commit(Prefix prefix) {

    }
}
