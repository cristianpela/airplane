package org.criskey.airplane.android.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import org.criskey.airplane.android.R;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.platform.InteractionEvent;
import org.criskey.airplane.common.local.platform.Painter;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.local.platform.PlayerUIOps;
import org.criskey.airplane.common.local.platform.RePainter;
import org.criskey.airplane.common.util.Position;


public class AndroidPlayerView extends View implements
        RePainter, PlayerUIOps {

    private static final float SCALE_FACTOR = 0.9f;
    private static final int DISABLED_MODE = 2;

    private TextPaint mTextPaint;

    private Paint paint;

    private PlayerUI mPlayerUI;
    private AndroidPainter mPainter;

    private GestureDetectorCompat mGestureDetector;

    private int mWidth, mHeight;
    private int mScaleStep;

    private int mMode = DISABLED_MODE;

    private GestureDetector.SimpleOnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Position touchPos = getTouchPosition(e);
            mPlayerUI.onInteraction(new InteractionEvent(touchPos, InteractionEvent.Type.CHECK));
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Position touchPos = getTouchPosition(e);
            if (mPlayerUI.isInPlaneResolution(touchPos))
                mPlayerUI.onInteraction(new InteractionEvent(touchPos, InteractionEvent.Type.ROTATION));
            return true;
        }
    };

    public AndroidPlayerView(Context context) {
        super(context);
        init(null, 0);
    }

    public AndroidPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public AndroidPlayerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        int dim = (int) getResources().getDimension(R.dimen.dimen_player_view_full);
        mScaleStep = (int) (dim - dim * SCALE_FACTOR);

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.AndroidPlayerView, defStyle, 0);
        mMode = a.getInt(R.styleable.AndroidPlayerView_mode, DISABLED_MODE);
        a.recycle();

        mGestureDetector = new GestureDetectorCompat(getContext(), mGestureListener);
        mGestureDetector.setOnDoubleTapListener(mGestureListener);
        mGestureDetector.setIsLongpressEnabled(false);

        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        paint = new Paint();
        paint.setColor(Color.BLACK);

        mPlayerUI = new PlayerUI(Position.asDimension(mWidth, mHeight), PlayerUI.Mode.values()[mMode], 50);
        mPlayerUI.registerRepainter(this);
        mPainter = new AndroidPainter(null, paint);

    }

    @NonNull
    private Position getTouchPosition(MotionEvent e) {
        final int pointerIndex = MotionEventCompat.getActionIndex(e);
        final float x = MotionEventCompat.getX(e, pointerIndex);
        final float y = MotionEventCompat.getY(e, pointerIndex);
        return Position.asXY((int) x, (int) y);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPainter.setCanvas(canvas);
        mPlayerUI.setResolution(Position.asDimension(mWidth, mHeight));
        mPlayerUI.draw(mPainter);
        mPainter.setCanvas(null);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w - getPaddingLeft() - 16;
        mHeight = h - getPaddingTop() - 16;
    }

    @Override
    public void paintAgain() {
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!mGestureDetector.onTouchEvent(event) == true) {
            Position touchPos = getTouchPosition(event);
            if (mPlayerUI.isInPlaneResolution(touchPos)) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        getParent().requestDisallowInterceptTouchEvent(true);//disable the scroll view while translation
                        mPlayerUI.onInteraction(new InteractionEvent(touchPos, InteractionEvent.Type.TRANSLATE_START));
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        mPlayerUI.onInteraction(new InteractionEvent(touchPos, InteractionEvent.Type.TRANSLATE));
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        mPlayerUI.onInteraction(new InteractionEvent(touchPos, InteractionEvent.Type.TRANSLATE_END));
                        getParent().requestDisallowInterceptTouchEvent(false);//translation is done so re-enable the scrolling
                    }
                }
            }
        }
        return true;
    }

    @Override
    public PlaneConfiguration getConfiguration() {
        return mPlayerUI.getConfiguration();
    }

    @Override
    public void block(boolean block) {
        mPlayerUI.block(block);
    }

    @Override
    public boolean isBlocked() {
        return mPlayerUI.isBlocked();
    }

    @Override
    public void mark(Shot shot, boolean repaint, boolean notifyCallback) {
        mPlayerUI.mark(shot, repaint, notifyCallback);
    }

    @Override
    public void configureBoard(PlaneConfiguration configuration) {
        mPlayerUI.configureBoard(configuration);
        invalidate();
    }

    @Override
    public void addBoardChangeCallback(BoardChangeCallback boardChangeCallback) {
        mPlayerUI.addBoardChangeCallback(boardChangeCallback);
    }

    @Override
    public void reset() {
        mPlayerUI.reset();
    }

    public void scale(boolean up) {
        int way = (up) ? 1 : -1;
        int newDim = mHeight + mScaleStep * way;
        newDim = newDim - newDim % 10;
        final int threshold = 900 + 300;
        if (newDim >= 270) {
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = newDim;
            params.width = newDim;
            requestLayout();
        }
    }

    public int getMode() {
        return mMode;
    }

    public void setMode(int mode) {
        mMode = mode;
        mPlayerUI.setMode(PlayerUI.Mode.values()[mMode]);
    }

    private static class AndroidPainter implements Painter {

        private Canvas canvas;
        private Paint paint;

        public AndroidPainter(Canvas canvas, Paint paint) {
            this.canvas = canvas;
            this.paint = paint;
        }

        public void setCanvas(Canvas canvas) {
            this.canvas = canvas;
        }


        @Override
        public void drawRect(int colLeft, int rowTop, int colRight, int rowBottom) {
            canvas.drawRect(colLeft, rowTop, colRight, rowBottom, paint);
        }

        @Override
        public void drawLine(int colFrom, int rowFrom, int colTo, int rowTo) {
            canvas.drawLine(colFrom, rowFrom, colTo, rowTo, paint);
        }

        @Override
        public void setColor(String colorString) {
            paint.setColor(Color.parseColor(colorString));
        }

        @Override
        public void drawString(String s, Position position) {
            canvas.drawText(s, position.column, position.row, paint);
        }

    }


}
