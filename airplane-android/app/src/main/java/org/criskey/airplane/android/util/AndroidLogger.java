package org.criskey.airplane.android.util;

import android.util.Log;

import org.criskey.airplane.android.BuildConfig;
import org.criskey.airplane.common.log.DebugLogger;

/**
 * Created by criskey on 25/10/2016.
 */
public class AndroidLogger extends DebugLogger {

    @Override
    public void print(Object messageHolder, String message, boolean showExecThread, Level level) {
        if (level == null) level = Level.INFO;

        String out = getOutputFormat(messageHolder, message, showExecThread);
        switch(level){
            case INFO: Log.i("AndroidLogger", out); break;
            case DEBUG: Log.d("AndroidLogger", out); break;
            case ERROR: Log.e("AndroidLogger", out);break;
        }

        notifyListeners(out, level);
    }


    @Override
    protected boolean isInDebugMode(){
        return BuildConfig.DEBUG;
    }
}
