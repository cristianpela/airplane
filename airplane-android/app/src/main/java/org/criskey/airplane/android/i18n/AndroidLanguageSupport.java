package org.criskey.airplane.android.i18n;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import org.criskey.airplane.common.local.i18n.LanguageSupport;

import java.util.Locale;

/**
 * Created by criskey on 23/11/2016.
 */
public class AndroidLanguageSupport extends LanguageSupport {

    private Context context;
    private Resources resources;

    public AndroidLanguageSupport(Locale locale, Context context) {
        super(locale);
        this.context = context;
        resources = context.getResources();
    }

    @Override
    public void onLocaleChanged() {
        if (resources != null) {
            Locale.setDefault(locale);
            Configuration config = resources.getConfiguration();
            config.locale = locale;
            resources.updateConfiguration(config, resources.getDisplayMetrics());
        }
    }


    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        onLocaleChanged();
    }

    @Override
    public String i18n(String messageKey, Object... params) {
        int resId = resources.getIdentifier(messageKey, "string", context.getPackageName());
        String string = resources.getString(resId);
        return params == null ? string : String.format(string, params);
    }

}
