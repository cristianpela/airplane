package org.criskey.airplane.android.platform;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;


import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.log.Logger;

import java.lang.ref.WeakReference;


/**
 * Created by criskey on 25/10/2016.
 */
public class OnUIAndroidRunCallback implements OnUIRunCallback {

    private Handler handler;

    public OnUIAndroidRunCallback() {
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void runOnUI(final OnUICodeInjector onUICodeInjector) {
        if (!isUIThread())
            handler.post(new Runnable() {
                @Override
                public void run() {
                    onUICodeInjector.inject();
                }
            });
        else
            onUICodeInjector.inject();
    }

    @Override
    public boolean isUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
