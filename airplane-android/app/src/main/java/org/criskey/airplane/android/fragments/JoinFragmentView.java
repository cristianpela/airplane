package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.android.R;
import org.criskey.airplane.android.platform.AndroidMessageDisplay;
import org.criskey.airplane.android.views.JoinViewBg;
import org.criskey.airplane.common.local.JoinPresenter;
import org.criskey.airplane.common.local.JoinView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import java.util.Map;

/**
 * Created by criskey on 27/10/2016.
 */
public class JoinFragmentView extends BaseFragment implements JoinView {

    private JoinPresenter presenter;

    private EditText mEditJoin;

    private Button mBtnJoin;

    private Button mBtnLogout;

    private TextView mTxtWait;

    private IView helper;

    private View mLayoutLogin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = getContext().getPresenter(JoinPresenter.class);
        helper = new ViewHelper(this, new JoinViewBg(getBaseActivity().getBackgroundViewStrategy()),
                presenter, getBaseActivity().getMessageDisplay());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.join_screen, container, false);
        mEditJoin = (EditText) view.findViewById(R.id.editJoin);
        mBtnJoin = (Button) view.findViewById(R.id.buttonJoin);
        mBtnLogout = (Button) view.findViewById(R.id.btn_logout);
        mTxtWait = (TextView) view.findViewById(R.id.progressWait);
        mLayoutLogin = view.findViewById(R.id.layout_logged_in);
        mBtnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.join(mEditJoin.getText().toString());
            }
        });
        mBtnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutLogin.setVisibility(View.VISIBLE);
                mBtnLogout.setVisibility(View.GONE);
                presenter.disconnect();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        helper.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        helper.onPause();
        super.onPause();
    }

    @Override
    public void onWait() {
        mBtnJoin.setEnabled(false);
        mTxtWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void onJoined() {
        mLayoutLogin.setVisibility(View.GONE);
        mBtnJoin.setEnabled(true);
        mTxtWait.setVisibility(View.INVISIBLE);
        mEditJoin.setText("");
        mBtnLogout.setVisibility(View.VISIBLE);
    }


    @Override
    public void showMessage(String message, boolean pop) {
        helper.showMessage(message, pop);
    }

    @Override
    public void showError(String message, boolean pop) {
        helper.showError(message, pop);
        mBtnJoin.setEnabled(true);
        mTxtWait.setVisibility(View.INVISIBLE);
        mLayoutLogin.setVisibility(View.VISIBLE);
        mBtnLogout.setVisibility(View.GONE);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        mEditJoin.setHint(presenter.i18n(LanguageSupport.SCREEN_JOIN_EDIT_USERNAME));
        mBtnJoin.setText(presenter.i18n(LanguageSupport.SCREEN_JOIN_BTN_JOIN));
        if (dataMap != null) {
            if ((Boolean) dataMap.get(JoinPresenter.KEY_BOOLEAN_WAITING))
                onWait();
            else if ((Boolean) dataMap.get(JoinPresenter.KEY_BOOLEAN_LOGGED_IN))
                onJoined();
            else// we're logged out
                mBtnLogout.setVisibility(View.GONE);
        }
    }
}
