package org.criskey.airplane.android.platform;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.TextView;
import android.widget.Toast;

import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;

import java.lang.ref.WeakReference;

/**
 * Created by criskey on 25/10/2016.
 */
public class AndroidMessageDisplay implements MessageDisplay {

    private WeakReference<CoordinatorLayout> coordinatorLayoutRef;

    //this must be a global context, otherwise we will have a leak!
    private Context context;

    public AndroidMessageDisplay(Context context, CoordinatorLayout coordinatorLayout) {
        this.context = context.getApplicationContext();
        coordinatorLayoutRef = new WeakReference<CoordinatorLayout>(coordinatorLayout);
    }

    @Override
    public void show(Kind kind, String title, String message) {
        if (!hasLayout()) {
            Logger.out.print(this, "Snackbar has no coordinator layout attached, switching to toast display", ILogger.Level.ERROR);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            return;
        }
        Snackbar snackbar = Snackbar.make(coordinatorLayoutRef.get(), message, Snackbar.LENGTH_SHORT);
        TextView txtMessage = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        txtMessage.setTextColor(Color.parseColor(kind.getColor()));
        snackbar.show();
    }

    public void show(String message) {
        show(Kind.INFO, "", message);
    }

    public void show(Kind kind, String message) {
        show(kind, "", message);
    }

    private boolean hasLayout() {
        return coordinatorLayoutRef.get() != null;
    }

    public void attachLayout(CoordinatorLayout coordinatorLayout) {
        if (!hasLayout())
            coordinatorLayoutRef = new WeakReference<CoordinatorLayout>(coordinatorLayout);
    }
}
