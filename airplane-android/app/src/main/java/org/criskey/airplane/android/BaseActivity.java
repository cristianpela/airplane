package org.criskey.airplane.android;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;

import org.criskey.airplane.android.platform.AndroidMessageDisplay;
import org.criskey.airplane.android.presenters.AndroidMainPresenter;
import org.criskey.airplane.android.router.RouteChangeListener;
import org.criskey.airplane.android.views.BackgroundView;
import org.criskey.airplane.android.views.BasicBackgroundSnackbarView;
import org.criskey.airplane.android.views.BasicBackgroundToastView;
import org.criskey.airplane.common.local.MainView;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import java.util.Map;

/**
 * Created by criskey on 5/12/2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements IView, RouteChangeListener {

    protected static final String EXTRA_SCREEN_ID = "EXTRA_SCREEN_ID";

    private AndroidMainPresenter presenter;

    private AirplaneApplication application;

    private AndroidMessageDisplay messageDisplay;

    private ViewHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());
        application = (AirplaneApplication) getApplication();

        presenter = getAirplaneContext().getPresenter(AndroidMainPresenter.class);
        helper = new ViewHelper(this, presenter);

        openScreenFromNotification(getIntent());
    }

    public AndroidMessageDisplay getMessageDisplay() {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.layout_coordinator);
        if (messageDisplay == null)
            messageDisplay = new AndroidMessageDisplay(this, coordinatorLayout);
        else {
            messageDisplay.attachLayout(coordinatorLayout);
        }
        return messageDisplay;
    }

    /**
     * Decides what type of background view should be displayed when the foreground view is not shown.
     * If the container activity has a coordinator layout, a snackbar based view will be offered, otherwise a toast based background view
     *
     * @return
     */
    @NonNull
    public BackgroundView getBackgroundViewStrategy() {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.layout_coordinator);
        return (coordinatorLayout == null) ?
                new BasicBackgroundToastView(this) :
                new BasicBackgroundSnackbarView(getAirplaneContext(), coordinatorLayout);
    }

    private void attachToApplication() {
        application.addFragmentManagerToRouter(getSupportFragmentManager(), R.id.screen_container, this);
        application.changePresentationToMessageMode(presenter.getCurrentScreen(),
                (CoordinatorLayout) findViewById(R.id.layout_coordinator));
    }

    private void dettachToApplication() {
        application.addFragmentManagerToRouter(null, R.id.screen_container, null);
        application.changePresentationToNotificationMode();
    }

    private void openScreenFromNotification(Intent intent) {
        int id = intent.getIntExtra(EXTRA_SCREEN_ID, -1);
        if (id > -1) {
            Router.Screen screen = Router.Screen.values()[id];
            presenter.showScreen(screen, false);
        }
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        getMessageDisplay().show(msg);
    }

    @Override
    public void showError(String err, boolean pop) {
        getMessageDisplay().show(MessageDisplay.Kind.ERROR, err);
    }

    @Override
    public void onResume() {
        super.onResume();
        attachToApplication();
        presenter.showScreen(presenter.getCurrentScreen(), false);
        helper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        helper.onPause();
        dettachToApplication();
    }

    @Override
    protected void onStop() {
        super.onStop();
        messageDisplay = null;
    }

    @Override
    public void onRouteChange(Router.Screen screen) {
        setTitle(getAirplaneContext().i18n(screen.id()));
        presenter.setCurrentScreen(screen);
    }

    @Override
    public void restore(Map<String, ?> map) {
        if (map == null) // we have a language change
            setTitle(getAirplaneContext().i18n(presenter.getCurrentScreen().id()));

    }

    @LayoutRes
    public abstract int getLayoutId();

    public AirplaneContext getAirplaneContext() {
        return (AirplaneContext) getApplication().getBaseContext();
    }

    public AndroidMainPresenter getMainPresenter(){
        return getAirplaneContext().getPresenter(AndroidMainPresenter.class);
    }

    public String i18n(String key, Object... params){
        return getAirplaneContext().i18n(key, params);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //force the language to refresh... and not let the sytem to reset to default locale
        getAirplaneContext().getLanguageSupport().onLocaleChanged();
    }

}
