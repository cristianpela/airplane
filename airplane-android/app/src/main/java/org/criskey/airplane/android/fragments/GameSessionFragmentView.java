package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.android.R;
import org.criskey.airplane.android.views.AndroidPlayerView;
import org.criskey.airplane.android.views.GameSessionViewBg;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSessionView;
import org.criskey.airplane.common.local.platform.PlayerUIOps;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.log.Logger;
import org.criskey.airplane.common.util.Position;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 17/11/2016.
 */
public class GameSessionFragmentView extends BaseFragment implements GameSessionView {

    private GameSessionPresenter presenter;
    private IView helper;

    private AndroidPlayerView mPlayerView;
    private ProgressBar mCountdownProgress;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = getContext().getPresenter(GameSessionPresenter.class);
        helper = new ViewHelper(this, new GameSessionViewBg(getBaseActivity().getBackgroundViewStrategy()),
                presenter, getBaseActivity().getMessageDisplay());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_session_screen, container, false);
        mPlayerView = (AndroidPlayerView) view.findViewById(R.id.player_view);
        mPlayerView.setMode(1);
        mPlayerView.addBoardChangeCallback(new PlayerUIOps.BoardChangeCallback() {
            @Override
            public void onBoardChanged(Position position) {
                presenter.attemptHit(position);
            }

            @Override
            public void onError(String err) {
                showError(getContext().i18n(err), true);
            }
        });
        mCountdownProgress = (ProgressBar) view.findViewById(R.id.prog_countdown);
        mCountdownProgress.setMax(GameSessionPresenter.TURN_SECONDS);

        GameFragmentHelper.assist(view, presenter);
        return view;
    }

    @Override
    public void responseAttempt(Shot shot) {
        if (shot.targetProtocol != TargetProtocol.SKIP)
            mPlayerView.mark(shot, true, false);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            List<Shot> attemptShots = (List<Shot>) dataMap.get(GameSessionPresenter.KEY_LIST_ATTEMPTS);
            for (Shot shot : attemptShots) {
                if (shot.targetProtocol != TargetProtocol.SKIP) {
                    mPlayerView.mark(shot, false, false);
                    mPlayerView.block(false);
                }
            }
            mPlayerView.invalidate();
            showMessage((String) dataMap.get(GameSessionPresenter.KEY_STRING_STATUS_MESSAGE), false);
            Boolean hasTurn = (Boolean) dataMap.get(GameSessionPresenter.KEY_BOOLEAN_HAS_TURN);
            takeTurn(hasTurn);
        }
        //leaveSessionBtn.setText(presenter.i18n(LanguageSupport.SCREEN_GAME_BTN_LEAVE));
    }

    @Override
    public void takeTurn(boolean hasTurn) {
        mPlayerView.block(!hasTurn);
        mCountdownProgress.setVisibility(hasTurn ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onTurnCountDown(int secondsToEnd) {
        if (secondsToEnd <= 10) {
            //TODO: play sound
            Logger.out.print(this, "Play sound " + secondsToEnd);
        }
        mCountdownProgress.setProgress(secondsToEnd);
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        helper.showMessage(msg, pop);
    }

    @Override
    public void showError(String msg, boolean pop) {
        helper.showError(msg, pop);
    }

    @Override
    public void onPause() {
        helper.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        helper.onResume();
        super.onResume();
    }

}
