package org.criskey.airplane.android.views;

import org.criskey.airplane.android.presenters.AndroidMainPresenter;
import org.criskey.airplane.android.router.RouteChangeListener;
import org.criskey.airplane.common.local.MainView;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;

import java.util.Map;

/**
 * Created by criskey on 29/11/2016.
 */
public class MainViewBg extends BackgroundViewAdapter implements MainView, RouteChangeListener {

    public MainViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void onLoggedIn(String s) {}

    @Override
    public void onSignOut() {}

    @Override
    public void onResume() {}

    @Override
    public void onPause() {}

    @Override
    public void onRouteChange(Router.Screen screen) {
        PresenterManager.plugReflect(AndroidMainPresenter.class).setCurrentScreen(screen);
    }
}
