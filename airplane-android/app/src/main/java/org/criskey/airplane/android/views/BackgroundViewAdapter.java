package org.criskey.airplane.android.views;

import android.content.Context;

import org.criskey.airplane.android.AirplaneContext;

import java.util.Map;

/**
 * Created by criskey on 5/12/2016.
 */
public class BackgroundViewAdapter implements BackgroundView {

    private BackgroundView backgroundView;

    public BackgroundViewAdapter(BackgroundView backgroundView) {
        this.backgroundView = backgroundView;
    }

    public void changeStrategy(BackgroundView backgroundView){
        this.backgroundView = backgroundView;
    }

    @Override
    public void onResume() {
        backgroundView.onResume();
    }

    @Override
    public void onPause() {
        backgroundView.onPause();
    }

    @Override
    public void showMessage(String s, boolean b) {
        backgroundView.showMessage(s, b);
    }

    @Override
    public void showError(String s, boolean b) {
        backgroundView.showError(s, b);
    }

    @Override
    public void restore(Map<String, ?> map) {
        backgroundView.restore(map);
    }

    @Override
    public void showProgressMessage(int progress, int max, String endProgressMessage) {
        backgroundView.showProgressMessage(progress, max, endProgressMessage);
    }

    @Override
    public void showActionMessage(String contentText, ActionNotificationInfo... infos) {
        backgroundView.showActionMessage(contentText, infos);
    }

}
