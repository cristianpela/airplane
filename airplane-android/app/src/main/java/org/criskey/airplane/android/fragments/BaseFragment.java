package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.Map;

/**
 * Created by criskey on 9/12/2016.
 */
public abstract class BaseFragment extends Fragment implements IView {

    /**
     * Return the Context this fragment is currently associated with. Context is converted to {@link AirplaneContext}
     * @return
     */
    @Override
    public AirplaneContext getContext() {
        BaseActivity baseActivity = (BaseActivity) super.getContext();
        return (baseActivity == null) ? null : baseActivity.getAirplaneContext();
    }

    public BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    @Override
    public void showMessage(String s, boolean b) {}

    @Override
    public void showError(String s, boolean b) {}

    @Override
    public void restore(Map<String, ?> map) {}

}
