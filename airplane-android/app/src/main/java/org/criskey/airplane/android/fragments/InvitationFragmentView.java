package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.criskey.airplane.android.R;
import org.criskey.airplane.android.views.InvitationListViewBg;
import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.InvitationListView;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 27/10/2016.
 */
public class InvitationFragmentView extends BaseFragment implements InvitationListView {

    private InvitationListPresenter presenter;

    private IView helper;

    private InvitationListAdapter mListAdapter;

    private InvitationListAdapter.OnInvitationDecisionCallback onInvitationDecisionCb = new InvitationListAdapter.OnInvitationDecisionCallback() {
        @Override
        public void onDecide(String invitation, boolean accepted) {
            if (accepted) presenter.acceptInvite(invitation);
            else presenter.rejectInvite(invitation);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = getContext().getPresenter(InvitationListPresenter.class);
        helper = new ViewHelper(this,
                new InvitationListViewBg(getBaseActivity().getBackgroundViewStrategy()),
                presenter, getBaseActivity().getMessageDisplay());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invitations_screen, container, false);
        ListView invList = (ListView) view.findViewById(R.id.list_invitations);
        mListAdapter = new InvitationListAdapter(getActivity(), onInvitationDecisionCb);
        invList.setAdapter(mListAdapter);
        return view;
    }

    @Override
    public void onResume() {
        helper.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        helper.onPause();
        super.onPause();
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        List<String> list = (List<String>) dataMap.get(InvitationListPresenter.KEY_LIST_INVITATIONS);
        setEntriesEnabled((Boolean) dataMap.get(InvitationListPresenter.KEY_BOOLEAN_ENTRIES_ENABLED));
        mListAdapter.addAll(list);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewInvitation(String s) {
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onInvitationAccepted(String s) {
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setEntriesEnabled(boolean b) {
        mListAdapter.notifyDataSetChanged();
    }


    @Override
    public void showMessage(String s, boolean b) {

    }

    @Override
    public void showError(String message, boolean b) {
    }

    private static class InvitationListAdapter extends BaseAdapter {

        private List<String> model;

        private LayoutInflater inflater;
        private OnInvitationDecisionCallback cb;

        public InvitationListAdapter(Context context, OnInvitationDecisionCallback cb) {
            this.cb = cb;
            inflater = LayoutInflater.from(context);
        }

        public void addAll(List<String> invites) {
            if (this.model == null)
                this.model = invites;
        }

        @Override
        public int getCount() {
            return model == null ? 0 : model.size();
        }

        @Override
        public String getItem(int position) {
            return model == null ? null : model.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final String invitation = getItem(position);
            View view = inflater.inflate(R.layout.invitation_list_item, parent, false);
            TextView txtInvitation = (TextView) view.findViewById(R.id.text_invitation_item_name);
            Button btnAccept = (Button) view.findViewById(R.id.btn_invitation_item_accept);
            Button btnReject = (Button) view.findViewById(R.id.btn_invitation_item_reject);
            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cb != null) {
                        cb.onDecide(invitation, true);
                    }
                }
            });
            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cb != null) {
                        cb.onDecide(invitation, false);
                        notifyDataSetChanged();
                    }
                }
            });
            txtInvitation.setText(invitation);
            return view;
        }

        interface OnInvitationDecisionCallback {
            void onDecide(String invitation, boolean accepted);
        }
    }
}
