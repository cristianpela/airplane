package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.android.R;
import org.criskey.airplane.android.databinding.GameSetupScreenBinding;
import org.criskey.airplane.android.views.AndroidPlayerView;
import org.criskey.airplane.android.views.GameSetupViewBg;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.game.GameSetupPresenter;
import org.criskey.airplane.common.local.game.GameSetupView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.PlayerUIOps;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.util.Position;

import java.util.Map;

/**
 * Created by criskey on 2/11/2016.
 */
public class GameSetupFragmentView extends BaseFragment implements GameSetupView {

    private GameSetupPresenter presenter;
    private IView helper;

    private AndroidPlayerView mPlayerView;
    private Button mBtnSubmit;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = getContext().getPresenter(GameSetupPresenter.class);
        helper = new ViewHelper(this, new GameSetupViewBg(getBaseActivity().getBackgroundViewStrategy()),
                presenter, getBaseActivity().getMessageDisplay());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_setup_screen, container, false);
        mPlayerView = (AndroidPlayerView) view.findViewById(R.id.player_view);
        mPlayerView.setMode(0);
        mPlayerView.addBoardChangeCallback(new PlayerUIOps.BoardChangeCallback() {
            @Override
            public void onBoardChanged(Position position) {
                //update the presenter plane configuration when board has changed
                presenter.setConfiguration(mPlayerView.getConfiguration());
            }

            @Override
            public void onError(String s) {/*no-op*/}
        });
        mBtnSubmit = (Button) view.findViewById(R.id.btn_submit_config);
        if (mBtnSubmit != null)
            mBtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setEnabled(false);
                    presenter.submitConfiguration();
                }
            });
        GameFragmentHelper.assist(view, presenter);
        return view;
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            PlaneConfiguration planeConfiguration = (PlaneConfiguration) dataMap.get(GameSetupPresenter.KEY_PLANE_CONFIGURATION);
            if (planeConfiguration != null)
                mPlayerView.configureBoard(planeConfiguration);
            Boolean isConfigSubmit = (Boolean) dataMap.get(GameSetupPresenter.KEY_BOOLEAN_IS_CONFIG_SUBMIT);
            if (mBtnSubmit != null) mBtnSubmit.setEnabled(!isConfigSubmit);
            //showMessage((String) dataMap.get(GameSetupPresenter.KEY_STRING_STATUS_MESSAGE), true);
        }
        if (mBtnSubmit != null) mBtnSubmit.setText(presenter.i18n(LanguageSupport.SCREEN_GAME_SETUP_BTN_SUBMIT));
    }

    @Override
    public void reset() {}

    @Override
    public void showMessage(String msg, boolean pop) {
        helper.showMessage(msg, pop);
    }

    @Override
    public void showError(String msg, boolean pop) {
        helper.showMessage(msg, pop);
        if (mBtnSubmit != null) mBtnSubmit.setEnabled(true);
    }

    @Override
    public void onPause() {
        helper.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        helper.onResume();
        super.onResume();
    }
}
