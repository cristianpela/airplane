package org.criskey.airplane.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.criskey.airplane.android.AirplaneApplication;
import org.criskey.airplane.android.AirplaneContext;

import java.util.HashMap;
import java.util.Map;

public class ActionNotificationReceiver extends BroadcastReceiver {

    private static final String EXTRAS = "EXTRAS";
    private static final String EXTRA_ACTIVITY_IN_FRONT = "EXTRA_ACTIVITY_IN_FRONT";

    private Map<String, ActionNotificationHandler> handlers;

    public ActionNotificationReceiver() {
        handlers = new HashMap<>();
        handlers.put(Action.Of.INVITE_ACCEPT, new InvitationAcceptHandler());
        handlers.put(Action.Of.INVITE_REJECT, new InvitationRejectHandler());
    }

    public static Intent createIntent(@Action String action, boolean isActivityInFront, String... extras) {
        return new Intent(action)
                .putExtra(EXTRAS, extras)
                .putExtra(EXTRA_ACTIVITY_IN_FRONT, isActivityInFront)
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!context.getClass().equals(AirplaneApplication.class)) {
            throw new IllegalStateException("The BroadcastReiceiver " + this.getClass().getSimpleName() +
                    " must be registered from the AirplaneApplication base context, but is registered from " + context.getClass().getSimpleName());
        }
        ActionNotificationHandler handler = handlers.get(intent.getAction());
        handler.handle((AirplaneContext) ((AirplaneApplication) context).getBaseContext(),
                intent.getBooleanExtra(EXTRA_ACTIVITY_IN_FRONT, false),
                intent.getStringArrayExtra(EXTRAS));
    }

}
