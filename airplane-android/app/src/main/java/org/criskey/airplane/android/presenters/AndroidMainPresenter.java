package org.criskey.airplane.android.presenters;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.local.MainPresenter;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.websocket.Stomp;

import java.util.Arrays;
import java.util.List;

/**
 * Created by criskey on 4/11/2016.
 */
public class AndroidMainPresenter extends MainPresenter {

    private Router.Screen currentScreen;

    /**
     * Screens that will be shown in fullscreen when device is in landscape
     */
    private List<Router.Screen> fullScreenList = Arrays.asList(new Router.Screen[]{
            Router.Screen.GAME_SESSION_,
            Router.Screen.GAME_SETUP,
            Router.Screen.GAME_HIT_HISTORY
    });


    public AndroidMainPresenter(ArgsBatch batch) {
        super(batch);
        currentScreen = Router.Screen.MAIN;
    }


    @Override
    public void onEvent(AppEvent event) {
        super.onEvent(event);
        AppEventType type = event.type;
        switch (type) {
            case NETWORK_CONNECTION: {
                Integer networkState = (Integer) event.data;
                switch(networkState){
                    case Stomp.DECONNECTED_FROM_APP:
                    case Stomp.DECONNECTED_FROM_OTHER:{
                        router.show(currentScreen, Router.Screen.MAIN, true);
                        break;
                    }
                }
                break;
            }
            case GAME_SESSION: {
                GameNetworkProtocol protocol = (GameNetworkProtocol) event.data;
                switch (protocol.getPhase()) {
                    case GameNetworkProtocol.PHASE_END:
                    case GameNetworkProtocol.PHASE_SESSION_LOST: {
                        router.show(currentScreen, Router.Screen.MAIN, true);
                        break;
                    }
                }
                break;
            }
        }
    }

    /**
     * @return true if the current screen can be shown in fullscreen
     */
    public boolean isScreenForFull() {
        return fullScreenList.contains(currentScreen);
    }

    @Override
    public void showScreen(Router.Screen screen, boolean bypassFilters) {
        this.router.show(Router.Screen.MAIN, screen, bypassFilters |  screen.equals(Router.Screen.MAIN));
    }

    public Router.Screen getCurrentScreen(){
        return currentScreen;
    }

    public void setCurrentScreen(Router.Screen currentScreen) {
        this.currentScreen = currentScreen;
    }
}
