package org.criskey.airplane.android.views;

import org.criskey.airplane.android.R;
import org.criskey.airplane.android.receivers.Action;
import org.criskey.airplane.common.local.InvitationListView;

/**
 * Created by criskey on 27/10/2016.
 */
public class InvitationListViewBg extends BackgroundViewAdapter implements InvitationListView {

    public InvitationListViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void onNewInvitation(String invitation) {
        String message = "New invitation to play with " + invitation;
        ActionNotificationInfo actionAccept = new ActionNotificationInfo(R.drawable.ic_menu_send, Action.Of.INVITE_ACCEPT, invitation);
        ActionNotificationInfo actionReject = new ActionNotificationInfo(R.drawable.ic_menu_share, Action.Of.INVITE_REJECT, invitation);
        showActionMessage(message, new ActionNotificationInfo[]{actionAccept, actionReject});
    }

    @Override
    public void onInvitationAccepted(String s) {
    }

    @Override
    public void setEntriesEnabled(boolean b) {
    }

}
