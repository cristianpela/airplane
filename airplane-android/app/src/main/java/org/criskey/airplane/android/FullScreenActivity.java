package org.criskey.airplane.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.TextView;

import org.criskey.airplane.android.presenters.AndroidMainPresenter;
import org.criskey.airplane.common.local.MainView;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;

import java.util.Map;

public class FullScreenActivity extends BaseActivity implements MainView {

    AndroidMainPresenter presenter = PresenterManager.plugReflect(AndroidMainPresenter.class);

    public static Intent createIntent(Context context, Router.Screen screen) {
        return new Intent(context, FullScreenActivity.class)
                .putExtra(EXTRA_SCREEN_ID, screen.ordinal());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView screenName = ((TextView) findViewById(R.id.txt_screen_name));
        screenName.setText(presenter.i18n(presenter.getCurrentScreen().id()));
    }

    @Override
    public void showError(String err, boolean pop) {
        super.showError(err, pop);
        finish();
    }

    @Override
    public void restore(Map<String, ?> map) {
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_full_screen;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT &&
                presenter.isScreenForFull()) {
            finish();
        }
    }

    @Override
    public void onRouteChange(Router.Screen screen) {

    }

    @Override
    public void onLoggedIn(String s) {

    }

    @Override
    public void onSignOut() {

    }
}
