package org.criskey.airplane.android.views;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.android.platform.AndroidMessageDisplay;

import java.util.Map;

/**
 * Created by criskey on 2/12/2016.
 */
public class BasicBackgroundToastView implements BackgroundView {

    private Context context;

    public BasicBackgroundToastView(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void showMessage(String message, boolean b) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String err, boolean b) {
        Toast.makeText(context, err, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void restore(Map<String, ?> map) {
    }

    @Override
    public void showProgressMessage(int progress, int max, String endProgressMessage) {
        if (progress == max) {
            showMessage(endProgressMessage, true);
        }
    }

    @Override
    public void showActionMessage(String contentText, ActionNotificationInfo... infos) {
        showMessage(contentText, true);
    }
}
