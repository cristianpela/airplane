package org.criskey.airplane.android.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.criskey.airplane.android.R;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.Map;

/**
 * Created by criskey on 27/10/2016.
 */
public class MainFragmentView extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_screen, container, false);
        return view;
    }

}
