package org.criskey.airplane.android.receivers;

import org.criskey.airplane.android.AirplaneContext;

/**
 * Created by criskey on 6/12/2016.
 */
public interface ActionNotificationHandler {

    void handle(AirplaneContext context, boolean isActivityInFront, String... extras);
}
