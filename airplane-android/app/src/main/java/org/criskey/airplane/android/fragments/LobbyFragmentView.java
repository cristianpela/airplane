package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.android.R;
import org.criskey.airplane.android.views.LobbyListViewBg;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.LobbyListView;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.log.Logger;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 27/10/2016.
 */
public class LobbyFragmentView extends BaseFragment implements LobbyListView {

    private LobbyListPresenter presenter;

    private IView helper;

    private ListView mListView;

    private UserInfoListAdapter mListAdapter;

    private UserInfoListAdapter.OnInviteCallback onInviteCallback = new UserInfoListAdapter.OnInviteCallback() {
        @Override
        public void onInvite(UserInfo user) {
            presenter.sendInvitation(user);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = getContext().getPresenter(LobbyListPresenter.class);
        helper = new ViewHelper(this,
                new LobbyListViewBg(getBaseActivity().getBackgroundViewStrategy()),
                presenter,
                getBaseActivity().getMessageDisplay());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lobby_screen, container, false);
        mListView = (ListView) view.findViewById(R.id.list_users);
        mListAdapter = new UserInfoListAdapter(getActivity(), onInviteCallback);
        mListView.setAdapter(mListAdapter);
        return view;
    }

    @Override
    public void onResume() {
        helper.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        helper.onPause();
        super.onPause();
    }

    @Override
    public void onUsersFetched(final List<UserInfo> list) {
        mListAdapter.addAll(list);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        List<UserInfo> list = (List<UserInfo>) dataMap.get(LobbyListPresenter.KEY_LIST_LOBBY);
        Logger.out.printd(this, "Restored Users: " + list, true);
        mListAdapter.addAll(list);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onUserJoined(UserInfo userInfo) {
        Logger.out.printd(this, "User Joined: " + userInfo, true);
        mListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onUserLeft(UserInfo userInfo) {
        Logger.out.printd(this, "User Left: " + userInfo);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onUserChanged(UserInfo userInfo) {
        Logger.out.printd(this, "User Changed: " + userInfo);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setEntriesEnabled(boolean enabled) {
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        helper.showMessage(msg, pop);
    }

    @Override
    public void showError(String msg, boolean pop) {
        helper.showError(msg, pop);
    }

    private void notifyDataSetChanged() {
        mListAdapter.notifyDataSetChanged();
    }

    private static class UserInfoListAdapter extends BaseAdapter {

        private List<UserInfo> model;
        private LayoutInflater inflater;
        private OnInviteCallback cb;

        public UserInfoListAdapter(Context context, OnInviteCallback cb) {
            this.cb = cb;
            inflater = LayoutInflater.from(context);
        }

        public void addAll(List<UserInfo> model) {
            if (this.model == null)
                this.model = model;
        }

        @Override
        public int getCount() {
            return model == null ? 0 : model.size();
        }

        @Override
        public UserInfo getItem(int position) {
            return model == null ? null : model.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //TODO: use holder pattern?
            final UserInfo user = getItem(position);
            View view = inflater.inflate(R.layout.lobby_list_item, parent, false);
            TextView textName = (TextView) view.findViewById(R.id.text_lobby_item_name);
            textName.setText(user.username);
            Button btnInvite = (Button) view.findViewById(R.id.btn_lobby_item_invite);
            btnInvite.setEnabled(!user.isPlaying);
            btnInvite.setText((user.isPlaying) ? "Playing..." : "Invite");
            btnInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cb != null)
                        cb.onInvite(user);
                }
            });
            return view;
        }

        public interface OnInviteCallback {
            void onInvite(UserInfo user);
        }
    }
}
