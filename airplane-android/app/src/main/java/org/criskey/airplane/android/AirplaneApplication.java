package org.criskey.airplane.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.preference.PreferenceManager;

import com.google.gson.Gson;

import org.criskey.airplane.android.persistance.NoStorage;
import org.criskey.airplane.android.platform.OnUIAndroidRunCallback;
import org.criskey.airplane.android.presenters.AndroidMainPresenter;
import org.criskey.airplane.android.receivers.Action;
import org.criskey.airplane.android.receivers.ActionNotificationReceiver;
import org.criskey.airplane.android.router.FragmentRouter;
import org.criskey.airplane.android.router.RouteChangeListener;
import org.criskey.airplane.android.util.AndroidLogger;
import org.criskey.airplane.android.views.BackgroundViewAdapter;
import org.criskey.airplane.android.views.BasicBackgroundToastView;
import org.criskey.airplane.android.views.BasicBackgroundNotificationView;
import org.criskey.airplane.android.views.BasicBackgroundSnackbarView;
import org.criskey.airplane.android.views.GameHitHistoryViewBg;
import org.criskey.airplane.android.views.GameSessionViewBg;
import org.criskey.airplane.android.views.GameSetupViewBg;
import org.criskey.airplane.android.views.InvitationListViewBg;
import org.criskey.airplane.android.views.JoinViewBg;
import org.criskey.airplane.android.views.LobbyListViewBg;
import org.criskey.airplane.android.views.MainViewBg;
import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.JoinPresenter;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.bus.DataBus;
import org.criskey.airplane.common.local.game.GameHitHistoryPresenter;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSetupPresenter;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.router.GameAwareRouter;
import org.criskey.airplane.common.local.router.LoginAwareRouter;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.AirplaneStompService;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.criskey.airplane.common.local.service.ServiceSubscriberHolder;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.log.Logger;

import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by criskey on 18/10/2016.
 */
public class AirplaneApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String HOST_NAME = "192.168.203.2:8080";

    private Router<Fragment> router;
    private OnUIRunCallback uiRunCb;
    private SharedPreferences preferences;
    private IAirplaneStompService service;
    private LanguageSupport languageSupport;

    private List<PresenterBackgroundViewHolder> pvHolderList;
    private AirplaneContext context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(new AirplaneContext(base));
    }


    @Override
    public void onCreate() {

        Logger.change(new AndroidLogger());

        Logger.out.print(this, "Create application", true);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener(this);

        try {
            //services and dependencies
            Gson gson = new Gson();
            context = (AirplaneContext) getBaseContext();

            registerActionNotificationReceiver(context);

            uiRunCb = new OnUIAndroidRunCallback();

            final DataBus dataBus = new DataBus(uiRunCb);
            service = new AirplaneStompService(getHostName(), dataBus.getListenerWSNetwork(), gson);
            ServiceSubscriberHolder subscriberHolder = new ServiceSubscriberHolder(service, dataBus, gson, uiRunCb);
            dataBus.addListener(subscriberHolder);

            //routers [order is important] loginAware -> gameAware - > MainRouter
            LoginAwareRouter loginAwareRouter = new LoginAwareRouter();
            GameAwareRouter gameAwareRouter = new GameAwareRouter(loginAwareRouter);
            router = new FragmentRouter(gameAwareRouter);
            dataBus.addListener(loginAwareRouter);
            dataBus.addListener(gameAwareRouter);

            //language
            languageSupport = (LanguageSupport) context.getCustomService(AirplaneContext.LANGUAGE_SUPPORT_SERVICE);
            setLanguage();

            //presenters
            PresenterManager.createInstance(service, router, uiRunCb, gson, dataBus, new NoStorage(), languageSupport);

            pvHolderList = new LinkedList<>();
            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.MAIN,
                    PresenterManager.plugReflect(AndroidMainPresenter.class),
                    new MainViewBg(new BasicBackgroundToastView(context)),
                    null
            ));
            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.JOIN,
                    PresenterManager.plugReflect(JoinPresenter.class),
                    new JoinViewBg(new BasicBackgroundToastView(context)),
                    null
            ));
            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.LOBBY,
                    PresenterManager.plugReflect(LobbyListPresenter.class),
                    new LobbyListViewBg(new BasicBackgroundToastView(context)),
                    "Lobby")
            );

            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.INVITES,
                    PresenterManager.plugReflect(InvitationListPresenter.class),
                    new InvitationListViewBg(new BasicBackgroundToastView(context)),
                    "Invitation")
            );


            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.GAME_SETUP,
                    PresenterManager.plugReflect(GameSetupPresenter.class),
                    new GameSetupViewBg(new BasicBackgroundToastView(context)),
                    "Game Setup")
            );

            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.GAME_SESSION_,
                    PresenterManager.plugReflect(GameSessionPresenter.class),
                    new GameSessionViewBg(new BasicBackgroundToastView(context)),
                    "Game Session")
            );

            pvHolderList.add(new PresenterBackgroundViewHolder(
                    Router.Screen.GAME_HIT_HISTORY,
                    PresenterManager.plugReflect(GameHitHistoryPresenter.class),
                    new GameHitHistoryViewBg(new BasicBackgroundToastView(context)),
                    "Game Hit History")
            );


        } catch (URISyntaxException ex) {
            Logger.out.print(this, ex.getMessage());
        }
        super.onCreate();
    }

    /**
     * Getting this error if the broadcast receiver is registered statically in the AndroidManifest.xml.</br>
     * <p>
     * <code>Caused by: java.lang.ClassCastException: org.criskey.airplane.android.AirplaneContext cannot be cast to
     * android.app.ContextImpl</code>
     * </p>
     *
     * @param context - our context wrapper
     */
    private void registerActionNotificationReceiver(AirplaneContext context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Action.Of.INVITE_ACCEPT);
        intentFilter.addAction(Action.Of.INVITE_REJECT);
        context.registerReceiver(new ActionNotificationReceiver(), intentFilter);
    }

    private void setLanguage() {
        String savedLang = preferences.getString("language_preference", "en_US");
        languageSupport.setLocale(LanguageSupport.getLocaleFromFormat(savedLang));
        Logger.out.print(this, "Language Changed to " + savedLang);
    }

    private String getHostName() {
        return preferences.getString("edit_server_ip", "192.168.203.2:8080");
    }


    public void addFragmentManagerToRouter(FragmentManager fragmentManager, @IdRes int containerId, RouteChangeListener routeChangeListener) {
        if (routeChangeListener == null)
            routeChangeListener = (RouteChangeListener) pvHolderList.get(0).backgroundView;
        ((FragmentRouter) router).setFragmentManager(fragmentManager, containerId, routeChangeListener);
    }

    public void changePresentationToNotificationMode() {
        for (PresenterBackgroundViewHolder pvh : pvHolderList) {
            pvh.attachNotificationView(context);
        }
    }

    public void changePresentationToMessageMode(Router.Screen exceptedScreen, CoordinatorLayout coordinatorLayout) {
        for (PresenterBackgroundViewHolder pvh : pvHolderList) {
            if (pvh.screen != exceptedScreen)
                pvh.attachMessageView(context, coordinatorLayout);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("edit_server_ip")) {
            Logger.out.print(this, "Server configuration changed to: " + getHostName());
            service.changeHost(getHostName());
        } else if (key.equals("language_preference")) {
            setLanguage();
        }
    }


    private static class PresenterBackgroundViewHolder implements Comparable<Router.Screen> {

        final Router.Screen screen;
        final Presentable presenter;
        final BackgroundViewAdapter backgroundView;
        final String notificationTitle;

        public PresenterBackgroundViewHolder(@NonNull Router.Screen screen,
                                             @NonNull Presentable presenter,
                                             @NonNull BackgroundViewAdapter backgroundView,
                                             String notificationTitle) {
            this.presenter = presenter;
            this.screen = screen;
            this.backgroundView = backgroundView;
            this.notificationTitle = notificationTitle;
        }

        public void attachMessageView(AirplaneContext context, CoordinatorLayout coordinatorLayout) {
            if (coordinatorLayout != null) {
                backgroundView.changeStrategy(new BasicBackgroundSnackbarView(context, coordinatorLayout));
            } else {
                backgroundView.changeStrategy(new BasicBackgroundToastView(context));
            }
            presenter.onAttach(backgroundView);
        }

        public void attachNotificationView(AirplaneContext context) {
            if (notificationTitle == null) {
                backgroundView.changeStrategy(new BasicBackgroundToastView(context));
            } else {
                backgroundView.changeStrategy(new BasicBackgroundNotificationView(context, screen, notificationTitle));
            }
            presenter.onAttach(backgroundView);
        }

        @Override
        public int compareTo(Router.Screen another) {
            return screen.compareTo(another);
        }
    }


}
