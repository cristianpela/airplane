package org.criskey.airplane.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.criskey.airplane.android.presenters.AndroidMainPresenter;
import org.criskey.airplane.android.router.RouteChangeListener;
import org.criskey.airplane.common.local.MainView;
import org.criskey.airplane.common.local.router.Router;

import java.util.Map;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   MainView,
                   RouteChangeListener {

    private static final String EXTRA_SCREEN_ID = "EXTRA_SCREEN_ID";

    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;

    public static Intent createIntent(Context context, Router.Screen screen) {
        return new Intent(context, MainActivity.class).putExtra(EXTRA_SCREEN_ID, screen.ordinal());
    }

    public static Intent createIntent(Context context, Router.Screen screen, int flags) {
        return createIntent(context, screen).addFlags(flags);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        AndroidMainPresenter presenter = getMainPresenter();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE && presenter.isScreenForFull()) {
            startActivity(FullScreenActivity.createIntent(getApplicationContext(), presenter.getCurrentScreen()));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setPalette(toolbar, true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.menu_auth, R.string.menu_lobby);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setPalette(final Toolbar toolbar, boolean cached) {

        final SharedPreferences prefs = getSharedPreferences("main_activity_storage", Context.MODE_PRIVATE);
        final String PALETTE_SWATCH_RGB_KEY = "palette_swatch_rgb";
        final String PALETTE_TITLE_TEXT_COLOR = "palette_title_txt_color";
        final int rgbVal = prefs.getInt(PALETTE_SWATCH_RGB_KEY, Integer.MAX_VALUE);
        final int titleTxtColorVal = prefs.getInt(PALETTE_TITLE_TEXT_COLOR, Integer.MAX_VALUE);

        if (cached && rgbVal != Integer.MAX_VALUE && titleTxtColorVal != Integer.MAX_VALUE) {
            toolbar.setBackgroundColor(rgbVal);
            toolbar.setTitleTextColor(titleTxtColorVal);
            return;
        }

        final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background_placeholder);
        if (bitmap != null && !bitmap.isRecycled())
            Palette.from(bitmap).generate(
                    new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            Palette.Swatch swatch = palette.getVibrantSwatch();
                            int paletteRgb = swatch.getRgb();
                            int paletteTitleTxtColor = swatch.getTitleTextColor();

                            toolbar.setBackgroundColor(paletteRgb);
                            toolbar.setTitleTextColor(paletteTitleTxtColor);
                            bitmap.recycle();

                            SharedPreferences.Editor editor = prefs.edit()
                                    .putInt(PALETTE_SWATCH_RGB_KEY, paletteRgb)
                                    .putInt(PALETTE_TITLE_TEXT_COLOR, paletteTitleTxtColor);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                                editor.apply();
                            } else
                                editor.commit();

                        }
                    }
            );
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_auth) {
            getMainPresenter().auth();
            return true;
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        AndroidMainPresenter presenter = getMainPresenter();
        int id = item.getItemId();
        if (id == R.id.nav_join) {
            presenter.showScreen(Router.Screen.JOIN, true);
        } else if (id == R.id.nav_lobby) {
            presenter.showScreen(Router.Screen.LOBBY, false);
        } else if (id == R.id.nav_invites) {
            presenter.showScreen(Router.Screen.INVITES, false);
        } else if (id == R.id.nav_game_session) {
            presenter.showScreen(Router.Screen.GAME_SESSION_, false);
        } else if (id == R.id.nav_game_hit_history) {
            presenter.showScreen(Router.Screen.GAME_HIT_HISTORY, false);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        //just update the menu when whe have a language change
        updateNavigationMenu();
    }

    @Override
    public void onLoggedIn(String username) {
        ((TextView)navigationView
                .getHeaderView(0)
                .findViewById(R.id.text_nav_logged_user))
                .setText(username);
    }

    @Override
    public void onSignOut() {
        ((TextView)navigationView
                .getHeaderView(0)
                .findViewById(R.id.text_nav_logged_user))
                .setText("");
    }

    private void updateNavigationMenu(){
        Menu navMenu = navigationView.getMenu();
        navMenu.findItem(R.id.nav_join).setTitle(R.string.menu_auth);
        navMenu.findItem(R.id.nav_lobby).setTitle(R.string.menu_lobby);
        navMenu.findItem(R.id.nav_invites).setTitle(R.string.menu_invites);
        navMenu.findItem(R.id.nav_game_session).setTitle(R.string.menu_game_session);
        navMenu.findItem(R.id.nav_game_hit_history).setTitle(R.string.menu_game_hit_history);
    }

}
