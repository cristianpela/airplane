package org.criskey.airplane.android.receivers;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by criskey on 7/12/2016.
 */
@StringDef({Action.Of.INVITE_ACCEPT, Action.Of.INVITE_REJECT})
@Retention(RetentionPolicy.SOURCE)
public @interface Action {
    interface Of {

        String INVITE_ACCEPT = "org.criskey.airplane.android.action.invite.Accept";

        String INVITE_REJECT = "org.criskey.airplane.android.action.invite.Reject";

    }
}
