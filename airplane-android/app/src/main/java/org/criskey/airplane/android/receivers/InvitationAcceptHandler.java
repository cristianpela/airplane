package org.criskey.airplane.android.receivers;

import android.content.Intent;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.android.MainActivity;
import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;

/**
 * Created by criskey on 6/12/2016.
 */
public class InvitationAcceptHandler implements ActionNotificationHandler {

    @Override
    public void handle(AirplaneContext context, boolean isActivityInFront, String... extras) {
        String username = extras[0];
        if (!isActivityInFront) {
            int flags = Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK;
            context.startActivity(MainActivity.createIntent(context, Router.Screen.INVITES,
                    flags));
        }
        context.getPresenter(InvitationListPresenter.class).acceptInvite(username);
    }
}
