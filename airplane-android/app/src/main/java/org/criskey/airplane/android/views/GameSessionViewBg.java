package org.criskey.airplane.android.views;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSessionView;

/**
 * Created by criskey on 17/11/2016.
 */
public class GameSessionViewBg extends BackgroundViewAdapter implements GameSessionView {

    public GameSessionViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void responseAttempt(Shot shot) {
        showMessage("Your shot was a " + shot.targetProtocol, true);
    }

    @Override
    public void takeTurn(boolean takeTurn) {
        //nothing to do here, because the take turn message is spread via show message callback
    }

    @Override
    public void onTurnCountDown(int i) {
        showProgressMessage((GameSessionPresenter.TURN_SECONDS - i), GameSessionPresenter.TURN_SECONDS, "You missed your turn");
    }

}
