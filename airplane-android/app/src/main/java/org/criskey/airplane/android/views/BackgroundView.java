package org.criskey.airplane.android.views;

import android.os.Parcelable;
import android.support.annotation.DrawableRes;

import org.criskey.airplane.android.receivers.Action;
import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 6/12/2016.
 */
public interface BackgroundView extends IView {

    void showProgressMessage(int progress, int max, String endProgressMessage);

    void showActionMessage(String contentText, ActionNotificationInfo... infos);

    /**
     * Created by criskey on 7/12/2016.
     */
    class ActionNotificationInfo {
        final int drawableId;
        final String action;
        final String[] extrasToPass;

        public ActionNotificationInfo(@DrawableRes int drawableId, @Action String action, String... extrasToPass) {
            this.drawableId = drawableId;
            this.action = action;
            this.extrasToPass = extrasToPass;
        }
    }
}
