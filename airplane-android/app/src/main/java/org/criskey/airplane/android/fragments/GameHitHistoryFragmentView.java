package org.criskey.airplane.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.criskey.airplane.android.BaseActivity;
import org.criskey.airplane.android.R;
import org.criskey.airplane.android.views.AndroidPlayerView;
import org.criskey.airplane.android.views.GameHitHistoryViewBg;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.game.GameHitHistoryPresenter;
import org.criskey.airplane.common.local.game.GameHitHistoryView;
import org.criskey.airplane.common.local.i18n.TranslatorUtil;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 2/12/2016.
 */
public class GameHitHistoryFragmentView extends BaseFragment implements GameHitHistoryView {

    /**
     * view-presenter linker
     */
    private ViewHelper mHelper;

    private AndroidPlayerView mAndroidPlayerView;

    private HitHistoryListAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHelper = new ViewHelper(this,
                new GameHitHistoryViewBg(getBaseActivity().getBackgroundViewStrategy()),
                getContext().getPresenter(GameHitHistoryPresenter.class),
                getBaseActivity().getMessageDisplay());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_hit_history_screen, container, false);
        mAndroidPlayerView = (AndroidPlayerView) view.findViewById(R.id.player_view);
        ListView hitHistoryList = (ListView) view.findViewById(R.id.list_hit_history);
        hitHistoryList.setAdapter(mAdapter = new HitHistoryListAdapter(getContext()));
        return view;
    }


    @Override
    public void incomingBlow(Shot shot) {
        incomingBlow(shot, true);
    }

    @Override
    public void setConfiguration(PlaneConfiguration planeConfiguration) {
        mAndroidPlayerView.configureBoard(planeConfiguration);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHelper.onPause();
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        mHelper.showMessage(msg, pop);
    }

    @Override
    public void showError(String err, boolean pop) {
        mHelper.showError(err, pop);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            PlaneConfiguration planeConfiguration = (PlaneConfiguration) dataMap.get(GameHitHistoryPresenter.KEY_PLANE_CONFIGURATION);
            List<Shot> list = (List<Shot>) dataMap.get(GameHitHistoryPresenter.KEY_LIST_HIT_HISTORY);
            mAndroidPlayerView.configureBoard(planeConfiguration);
            mAdapter.setModel(list);
            for (Shot shot : list)
                incomingBlow(shot, false);
            mAdapter.notifyDataSetChanged();
            mAndroidPlayerView.invalidate();
        }
    }

    private void incomingBlow(Shot shot, boolean repaint) {
        if (shot.targetProtocol != TargetProtocol.SKIP) {
            mAndroidPlayerView.mark(shot, repaint, false);
            mAndroidPlayerView.block(false);
        }

        if (repaint) {
            mAdapter.notifyDataSetChanged();
            mAndroidPlayerView.invalidate();
        }
    }


    private static class HitHistoryListAdapter extends BaseAdapter {

        private List<Shot> shots;

        private LayoutInflater inflater;

        public HitHistoryListAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setModel(List<Shot> shots) {
            if (this.shots == null)
                this.shots = shots;
        }

        @Override
        public int getCount() {
            return (shots != null) ? shots.size() : 0;
        }

        @Override
        public Shot getItem(int position) {
            return (shots != null) ? shots.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = inflater.inflate(R.layout.game_hit_history_item_list, parent, false);
            TextView txtView = (TextView) view.findViewById(R.id.text_hit);
//            txtView.setText(TranslatorUtil.getInstance().translateShot(getItem(position)));
            txtView.setText(getItem(position).toString());
            return view;
        }
    }
}
