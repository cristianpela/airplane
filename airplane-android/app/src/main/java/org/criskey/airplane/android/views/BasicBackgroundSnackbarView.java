package org.criskey.airplane.android.views;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.android.receivers.ActionNotificationReceiver;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;

import java.lang.ref.WeakReference;
import java.util.Map;

/**
 * Created by criskey on 8/12/2016.
 */
public class BasicBackgroundSnackbarView implements BackgroundView {

    private WeakReference<CoordinatorLayout> coordinatorLayoutRef;

    private AirplaneContext context;

    public BasicBackgroundSnackbarView(AirplaneContext context, CoordinatorLayout coordinatorLayout) {
        coordinatorLayoutRef = new WeakReference<CoordinatorLayout>(coordinatorLayout);
        this.context = context;
    }

    @Override
    public void showProgressMessage(int progress, int max, String endProgressMessage) {
    }

    @Override
    public void showActionMessage(String contentText, ActionNotificationInfo... infoActions) {
        if (!hasLayout()) return;
        Snackbar snackbar = Snackbar.make(coordinatorLayoutRef.get(), contentText, Snackbar.LENGTH_LONG);
        final ActionNotificationInfo acceptAction = infoActions[0];
        snackbar.setAction(context.i18n(acceptAction.action), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.sendBroadcast(ActionNotificationReceiver.createIntent(acceptAction.action, true,
                        acceptAction.extrasToPass));
            }
        });
        snackbar.show();
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void showMessage(String message, boolean pop) {
        if (hasLayout())
            Snackbar.make(coordinatorLayoutRef.get(), message, Snackbar.LENGTH_SHORT).show();
        else
            printError();
    }


    @Override
    public void showError(String error, boolean pop) {
        if (hasLayout()) {
            Snackbar snackbar = Snackbar.make(coordinatorLayoutRef.get(), error, Snackbar.LENGTH_SHORT);
            View sbView = snackbar.getView();
            TextView txtError = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            txtError.setTextColor(Color.RED);
            snackbar.show();
        } else {
            printError();
        }
    }


    @Override
    public void restore(Map<String, ?> map) {
    }

    private boolean hasLayout() {
        return coordinatorLayoutRef.get() != null;
    }

    private void printError() {
        Logger.out.print(this, "No coordinator layout attached to snackbar", ILogger.Level.ERROR);
    }
}
