package org.criskey.airplane.android.receivers;

import org.criskey.airplane.android.AirplaneContext;
import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;

/**
 * Created by criskey on 7/12/2016.
 */
public class InvitationRejectHandler implements ActionNotificationHandler {
    @Override
    public void handle(AirplaneContext context, boolean isActivityInFront, String... extras) {
        String username = extras[0];
        PresenterManager.plugReflect(InvitationListPresenter.class)
                .rejectInvite(username);
    }
}
