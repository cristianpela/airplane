package org.criskey.airplane.android.views;

import android.content.Context;

import org.criskey.airplane.android.platform.AndroidMessageDisplay;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListView;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.log.Logger;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 1/11/2016.
 */
public class LobbyListViewBg extends BackgroundViewAdapter implements LobbyListView {

    public LobbyListViewBg(BackgroundView backgroundView) {
        super(backgroundView);
    }

    @Override
    public void onUserJoined(UserInfo userInfo) {
        showMessage(userInfo.username + " has joined the lobby", true);
    }

    @Override
    public void onUserLeft(UserInfo userInfo) {
        showMessage(userInfo.username + " has left the lobby", true);
    }

    @Override
    public void onUserChanged(UserInfo userInfo) {}

    @Override
    public void setEntriesEnabled(boolean b) {}

    @Override
    public void onUsersFetched(List<UserInfo> list) {}

}
