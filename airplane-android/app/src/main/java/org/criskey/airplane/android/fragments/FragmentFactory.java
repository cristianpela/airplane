package org.criskey.airplane.android.fragments;

import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 27/10/2016.
 */
public class FragmentFactory {


    public FragmentFactory() {

    }

    public IView createFragment(Router.Screen screen) {
        IView fragment = null;
        switch (screen) {
            case JOIN:
                fragment = new JoinFragmentView();
                break;
            case INVITES:
                fragment = new InvitationFragmentView();
                break;
            case LOBBY:
                fragment = new LobbyFragmentView();
                break;
            case GAME_SETUP:
                fragment = new GameSetupFragmentView();
                break;
            case GAME_SESSION_:
                fragment = new GameSessionFragmentView();
                break;
            case GAME_HIT_HISTORY:
                fragment = new GameHitHistoryFragmentView();
                break;
            default:
                fragment = new MainFragmentView();
        }
        return fragment;
    }

}
