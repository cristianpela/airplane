package org.criskey.airplane.desktop.persistance;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.log.Logger;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by criskey on 29/11/2016.
 */
public class LangFilesStorage implements Storage {

    private String[] codes = {"en_US", "ro_RO"};

    private String[] langs = {LanguageSupport.LANG_EN, LanguageSupport.LANG_RO};

    public LangFilesStorage() {

        List<String> fileCodes = new LinkedList<>();
        File root = new File(".");
        for (String name : root.list()) {
            if (name.endsWith("properties") && name.startsWith("strings")) {
                if (name.indexOf("_") > 0) {// we have a specific lang file
                    String lang = name.substring(name.indexOf("_") + 1, name.indexOf("."));
                    fileCodes.add(lang);
                } else {
                    fileCodes.add("en_US"); // default
                }
            }
        }

        codes = fileCodes.toArray(new String[fileCodes.size()]);
        langs = fileCodes.stream()
                .map((code) -> "lang_" + code.split("_")[0])
                .collect(Collectors.toList())
                .toArray(new String[fileCodes.size()]);

        Logger.out.print(this, fileCodes.toString() + " | " + Arrays.toString(langs));
    }

    @Override
    public <F> void save(Prefix prefix, String fieldKey, F f) {
        //no-op
    }

    @Override
    public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
        if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_CODES)) {
            asyncCallback.onReturn((F) codes, null);
        } else if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_NAME_KEYS)) {
            asyncCallback.onReturn((F) langs, null);
        }
    }

    @Override
    public void wipe(Prefix prefix) {
        //no-op
    }

    @Override
    public void commit(Prefix prefix) {
        //no-op
    }
}
