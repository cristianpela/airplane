package org.criskey.airplane.desktop.persistance;

import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.util.AsyncCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by criskey on 28/11/2016.
 */
public class PropertiesSettingsStorage implements Storage {

    public static final File FILE = new File("settings.properties");

    private Properties properties;

    public PropertiesSettingsStorage() {
        try {
            properties = new Properties();
            if (!FILE.exists()) {
                FILE.createNewFile();
            }
            properties.load(new FileInputStream(FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public <F> void save(Prefix prefix, String fieldKey, F field) {
        properties.put(fieldKey, field);

    }

    @Override
    public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
        final F prop = (F) properties.get(fieldKey);
        Error error = (prop == null) ? new Error("Property not found") : null;
        asyncCallback.onReturn(prop, error);
    }

    @Override
    public void wipe(Prefix prefix) {

    }

    @Override
    public void commit(Prefix prefix) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(FILE);
            properties.store(out, "Settings file");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            ;
        }
    }
}
