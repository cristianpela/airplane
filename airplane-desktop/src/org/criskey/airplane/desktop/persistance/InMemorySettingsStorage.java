package org.criskey.airplane.desktop.persistance;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.util.AsyncCallback;

/**
 * Created by criskey on 28/11/2016.
 */
public class InMemorySettingsStorage implements Storage {

    private String[] codes = {"en_US", "ro_RO"};

    private String[] langs = {LanguageSupport.LANG_EN, LanguageSupport.LANG_RO};

    private String currLangKey = LanguageSupport.LANG_EN;

    private String currLangCode = codes[0];

    private String server = "localhost:8080";

    @Override
    public <F> void save(Prefix prefix, String fieldKey, F field) {
        if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_NAME_KEY))
            currLangKey = (String) field;
        else if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_CODE))
            currLangCode = (String) field;
        else if (fieldKey.equals(Storage.PRF_SERVER_ADDRESS))
            server = (String) field;
    }

    @Override
    public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
        if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_NAME_KEY)) {
            asyncCallback.onReturn((F) currLangKey, null);
        } else if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_CODE)) {
            asyncCallback.onReturn((F) currLangCode, null);
        } else if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_CODES)) {
            asyncCallback.onReturn((F) codes, null);
        } else if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_NAME_KEYS)) {
            asyncCallback.onReturn((F) langs, null);
        } else if (fieldKey.equals(Storage.PRF_SERVER_ADDRESS)) {
            asyncCallback.onReturn((F) server, null);
        }
    }

    @Override
    public void wipe(Prefix prefix) {
    }

    @Override
    public void commit(Prefix prefix) {

    }
}
