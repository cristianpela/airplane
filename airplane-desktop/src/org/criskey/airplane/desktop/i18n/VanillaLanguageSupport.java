package org.criskey.airplane.desktop.i18n;

import org.criskey.airplane.common.local.i18n.LanguageSupport;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by criskey on 21/11/2016.
 */
public class VanillaLanguageSupport extends LanguageSupport {

    private static final String BASE_NAME = "strings";

    private ResourceBundle messages;

    public VanillaLanguageSupport(Locale locale) {
        super(locale);
    }

    public VanillaLanguageSupport() {
        super();
    }

    @Override
    public String i18n(String resourceKey, Object... params) {
        try {
            String msg = messages.getString(resourceKey);
            return (params == null) ? msg :
                    MessageFormat.format(msg, params);
        } catch (MissingResourceException ex) {
            return resourceKey;
        }
    }

    @Override
    public void onLocaleChanged() {
        messages = ResourceBundle.getBundle(BASE_NAME, locale);
    }

}
