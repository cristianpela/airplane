package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import java.awt.*;

/**
 * Created by criskey on 23/10/2016.
 */
public final class IFrameFactory {

    private Position resolution;

    private LanguageSupport lang;

    public IFrameFactory(Position resolution, LanguageSupport lang) {
        this.resolution = resolution;
        this.lang = lang;
    }

    public ScreenContainer createFrame(Screen screen) {
        ScreenContainer frame = null;
        switch (screen) {
            case JOIN:
                frame = createFrame(screen, LanguageSupport.MENU_AUTH,
                        new JJoinView(),
                        Position.asDimension(350, 130),
                        new Position(10, 10),
                        false, false, false, false);
                break;
            case SETTINGS:
                frame = createFrame(screen, LanguageSupport.MENU_SETTINGS,
                        new JSettingsView(),
                        Position.asDimension(450, 150),
                        Position.asXY(500, 10),
                        false, false, false, true);
                break;
            case GAME_SETUP:
                frame = createFrame(screen, LanguageSupport.MENU_GAME_SETUP,
                        new JGameSetupView(resolution),
                        Position.asDimension(593, 468),
                        new Position(10, 10),
                        false, true, true, true);
                break;
            case GAME_SESSION_:
                frame = createFrame(screen, LanguageSupport.MENU_GAME_SESSION,
                        new JGameSessionView(resolution),
                        Position.asDimension(593, 468),
                        new Position(10, 10),
                        false, true, true, true);
                break;
            case LOBBY:
                frame = createFrame(screen, LanguageSupport.MENU_LOBBY,
                        new JLobbyListView(),
                        resolution.div(new Position(1, 3)),
                        new Position(10, 10),
                        true, false, false, true);
                break;
            case INVITES:
                frame = createFrame(screen, LanguageSupport.MENU_INVITES,
                        new JInvitationListView(),
                        resolution.div(new Position(1, 3)),
                        new Position(10, resolution.column - 300),
                        true, false, false, true);
                break;
        }
        return frame;
    }


    private ScreenContainer createFrame(Screen screen,
                                        String titleLangKey,
                                        IView view,
                                        Position size,
                                        Position positionOnScreen,
                                        boolean hasScroll,
                                        boolean resizable,
                                        boolean maximizable,
                                        boolean closable) {
        ScreenContainer frame = new ScreenContainer(screen, lang, titleLangKey, resizable, closable, maximizable, false);
        frame.setSize(new Dimension(size.column, size.row));
        frame.setLocation(positionOnScreen.column, positionOnScreen.row);
        Container container = null;
        if (hasScroll) {
            container = new JScrollPane((Container) view);
            container.setPreferredSize(frame.getPreferredSize());
        } else {
            container = (Container) view;
        }
        frame.setContentPane(container);
        return frame;
    }
}
