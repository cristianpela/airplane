package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.game.GameSetupPresenter;
import org.criskey.airplane.common.local.game.GameSetupView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.local.platform.PlayerUIOps;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Created by criskey on 20/10/2016.
 */
public class JGameSetupView extends JPanel implements GameSetupView {

    private GameSetupPresenter presenter;

    private JPlayerUI playerUI;
    private JButton submitBtn;
    private JButton leaveSessionBtn;
    private JLabel messageBarLbl;

    private IView viewHelper;

    public JGameSetupView(Position resolution) {

        viewHelper = new ViewHelper(this,
                new JGameSetupViewBg(),
                presenter = PresenterManager.plugReflect(GameSetupPresenter.class),
                new JMessageDisplay(this));

        playerUI = new JPlayerUI(Position.asDimension(450, 390), PlayerUI.Mode.SETUP, 40);
        playerUI.addBoardChangeCallback(new PlayerUIOps.BoardChangeCallback() {

            @Override
            public void onBoardChanged(Position position) {
                presenter.setConfiguration(playerUI.getConfiguration());
            }

            @Override
            public void onError(String errKey) {
                viewHelper.showError(presenter.i18n(errKey), true);
            }
        });

        messageBarLbl = new JLabel();
        submitBtn = new JButton();
        leaveSessionBtn = new JButton();

        setListeners();

        setLayout(new BorderLayout());
        add(playerUI, BorderLayout.CENTER);
        JPanel bottomBar = new JPanel(new FlowLayout());
        bottomBar.add(messageBarLbl);
        bottomBar.add(submitBtn);
        bottomBar.add(leaveSessionBtn);
        add(bottomBar, BorderLayout.SOUTH);

        onResume();
    }

    private void setListeners() {
        leaveSessionBtn.addActionListener((e) -> {
            presenter.leaveSession();
        });
        submitBtn.addActionListener((e) -> {
            submitBtn.setEnabled(false);
            presenter.submitConfiguration();
        });
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        if (pop) viewHelper.showMessage(msg, pop);
        else messageBarLbl.setText(msg);
    }

    @Override
    public void showError(String msg, boolean pop) {
        submitBtn.setEnabled(true);
        if (pop) viewHelper.showError(msg, pop);
        else messageBarLbl.setText(msg);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            PlaneConfiguration planeConfiguration = (PlaneConfiguration) dataMap.get(GameSetupPresenter.KEY_PLANE_CONFIGURATION);
            if (planeConfiguration != null)
                playerUI.configureBoard(planeConfiguration);
            Boolean isConfigSubmit = (Boolean) dataMap.get(GameSetupPresenter.KEY_BOOLEAN_IS_CONFIG_SUBMIT);
            submitBtn.setEnabled(!isConfigSubmit);
            messageBarLbl.setText((String) dataMap.get(GameSetupPresenter.KEY_STRING_STATUS_MESSAGE));
        }
        submitBtn.setText(presenter.i18n(LanguageSupport.SCREEN_GAME_SETUP_BTN_SUBMIT));
        leaveSessionBtn.setText(presenter.i18n(LanguageSupport.SCREEN_GAME_BTN_LEAVE));
    }

    @Override
    public void reset() {
        playerUI.reset();
        submitBtn.setEnabled(true);
        messageBarLbl.setText("");
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }
}
