package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.i18n.LanguageChangeListener;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.local.router.AbstractRouter;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.RouterMiddleware;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.log.Logger;

import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 22/9/2016.
 */
public class IFrameRouter extends AbstractRouter<ScreenContainer> implements LanguageChangeListener {

    private JMainView window;

    private IFrameFactory iFrameFactory;

    private Map<Screen, ScreenContainer> cachedFrames;

    private MessageDisplay messageDisplay;

    public IFrameRouter(JMainView window, IFrameFactory iFrameFactory, RouterMiddleware routerMiddleware) {
        super(routerMiddleware);
        this.window = window;
        this.iFrameFactory = iFrameFactory;
        this.cachedFrames = new HashMap<>();
        messageDisplay = new JMessageDisplay(window);
    }


    @Override
    public Screen show(Screen toScreen) {
        Screen to = showInternal(toScreen);
        ScreenContainer frame = cachedFrames.get(to);
        if (frame == null) {
            frame = iFrameFactory.createFrame(to);
            if (frame != null)
                addFrameToContent(to, frame);
        }
        Logger.out.printd(this, "Requested screen is " + toScreen + ". Opening " + to, true);
        if (frame != null)
            frame.setVisible(true);
        else
            messageDisplay.show(MessageDisplay.Kind.ERROR, "Error", "Can't open " + toScreen);
        return to;
    }


    @Override
    public void hide(Screen screen) {
        ScreenContainer frame = cachedFrames.get(screen);
        if (frame != null) {
            Logger.out.printd(this, "Removing screen " + screen, true);
            cachedFrames.remove(screen);
            window.removeFrame(frame);
        }
    }

    @Override
    public void addScreen(Screen screen, ScreenContainer frame) {
        addFrameToContent(screen, frame);
    }

    private void addFrameToContent(Screen screen, ScreenContainer frame) {
        cachedFrames.put(screen, frame);
        window.addFrame(frame);
        frame.addInternalFrameListener(new InternalFrameAdapter() {
            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
                for (Component component : frame.getComponents()) {
                    notifyOnClosing(component);
                }
                cachedFrames.remove(((ScreenContainer) e.getInternalFrame()).screen);
            }
        });

    }

    private void notifyOnClosing(Component component) {
        if (component instanceof IView) {
            ((IView) component).onPause();
        }
        if (component instanceof Container) {
            for (Component child : ((Container) component).getComponents()) {
                notifyOnClosing(child);
            }
        }
    }

    @Override
    public void onLanguageChanged() {
        for (ScreenContainer screenContainer : cachedFrames.values()) {
            screenContainer.updateTitle();
        }
    }
}
