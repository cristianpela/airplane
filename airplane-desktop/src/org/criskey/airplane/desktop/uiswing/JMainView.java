package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.MainPresenter;
import org.criskey.airplane.common.local.MainView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;
import org.criskey.airplane.common.log.PrintListener;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import static org.criskey.airplane.common.local.i18n.LanguageSupport.*;

/**
 * Created by criskey on 21/9/2016.
 */
public class JMainView extends JFrame implements MainView {

    private MainPresenter presenter;

    private IView viewHelper;
    private JDesktopPane desktopPane;
    private JTextPane logArea;

    private JMenu authMenu;
    private JMenu gameMenu;
    private JMenu lobbyMenu;
    private JMenu invitesMenu;
    private JMenu settingsMenu;


    private PrintListener printListener = (log, level) -> {
        if (SwingUtilities.isEventDispatchThread()) {
            appendLog(log, level);
        } else {
            SwingUtilities.invokeLater(() ->
                    appendLog(log, level));
        }
    };


    public JMainView(Position resolution) {

        presenter = PresenterManager.plugReflect(MainPresenter.class);
        viewHelper = new ViewHelper(this, presenter, new JMessageDisplay(this));

        setTitle(presenter.i18n(TITLE));

        JScrollPane desktopScrollPane = new JScrollPane(desktopPane = new JDesktopPane());
        JScrollPane logScrollPane = new JScrollPane(logArea = new JTextPane());
        Logger.out.addPrintListener(printListener);

        //Create a split pane with the two scroll panes in it.
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                desktopScrollPane, logScrollPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(550);

        //Provide minimum sizes for the two components in the split pane
        Dimension minimumSize = new Dimension(100, 50);
        desktopPane.setMinimumSize(minimumSize);
        logScrollPane.setMinimumSize(minimumSize);

        this.setContentPane(splitPane);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // this.setBounds(30, 30, resolution.column, resolution.row);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        JMenuBar menuBar = createMenu();
        this.setJMenuBar(menuBar);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                presenter.close();
                System.exit(0);
            }
        });
        onResume();
    }

    private void appendLog(String log, ILogger.Level level) {
        Color color = Color.BLACK;
        switch (level) {
            case DEBUG:
                color = Color.BLUE;
                break;
            case ERROR:
                color = Color.RED;
                break;
        }
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet attSet = sc.addAttribute(SimpleAttributeSet.EMPTY,
                StyleConstants.Foreground, color);
        int len = logArea.getDocument().getLength();
        logArea.setCaretPosition(len);
        logArea.setCharacterAttributes(attSet, false);
        logArea.replaceSelection(level + " " + log);
    }

    private JMenuBar createMenu() {
        JMenuBar menuBar = new JMenuBar();

        authMenu = new JMenu(presenter.i18n(MENU_AUTH));
        authMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuSelected(MenuEvent e) {
                presenter.auth();
            }
        });

        gameMenu = new JMenu(presenter.i18n(MENU_GAME_SESSION));
        gameMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuSelected(MenuEvent e) {
                presenter.showScreen(Screen.GAME_SESSION_);
            }
        });

        lobbyMenu = new JMenu(presenter.i18n(MENU_LOBBY));
        lobbyMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuSelected(MenuEvent e) {
                presenter.showScreen(Screen.LOBBY);
            }
        });
        invitesMenu = new JMenu(presenter.i18n(MENU_INVITES));
        invitesMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuSelected(MenuEvent e) {presenter.showScreen(Screen.INVITES);}
        });

        settingsMenu = new JMenu(presenter.i18n(MENU_SETTINGS));
        settingsMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuSelected(MenuEvent e) {presenter.showScreen(Screen.SETTINGS);}
        });


        menuBar.add(authMenu);
        menuBar.add(gameMenu);
        menuBar.add(lobbyMenu);
        menuBar.add(invitesMenu);
        menuBar.add(settingsMenu);
        return menuBar;
    }

    @Override
    public void onLoggedIn(String username) {
        setTitle(presenter.i18n(TITLE_LOGGED, username));
        authMenu.setText(presenter.i18n(MENU_AUTH_OUT));
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        String username = (String) dataMap.get(MainPresenter.KEY_STRING_USERNAME);
        String titleKey = (username != null) ? LanguageSupport.TITLE_LOGGED : LanguageSupport.TITLE;
        setTitle(presenter.i18n(titleKey, username));
        authMenu.setText(presenter.i18n((String) dataMap.get(MainPresenter.KEY_STRING_LANG_AUTH_STATE)));
        lobbyMenu.setText(presenter.i18n(MENU_LOBBY));
        invitesMenu.setText(presenter.i18n(MENU_INVITES));
        gameMenu.setText(presenter.i18n(MENU_GAME_SESSION));
        settingsMenu.setText(presenter.i18n(MENU_SETTINGS));
    }

    @Override
    public void onSignOut() {
        setTitle(presenter.i18n(LanguageSupport.TITLE));
        authMenu.setText(presenter.i18n(MENU_AUTH));
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }


    @Override
    public void showMessage(String message, boolean b) {
        viewHelper.showMessage(message, b);
    }

    @Override
    public void showError(String message, boolean b) {
        viewHelper.showError(message, b);
    }


    public void addFrame(JInternalFrame frame) {
        desktopPane.add(frame);
    }

    public void removeFrame(JInternalFrame frame) {
        desktopPane.remove(frame);
        repaint();
    }


    private static abstract class MenuAdapter implements MenuListener {

        @Override
        public void menuSelected(MenuEvent e) {

        }

        @Override
        public void menuDeselected(MenuEvent e) {

        }

        @Override
        public void menuCanceled(MenuEvent e) {

        }
    }
}
