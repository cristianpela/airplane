package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListView;
import org.criskey.airplane.common.local.platform.MessageDisplay;

import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 24/10/2016.
 */
public class JLobbyListViewBg implements LobbyListView {

    private MessageDisplay messageDisplay;

    public JLobbyListViewBg() {
        messageDisplay = new JMessageDisplay(null);
    }

    @Override
    public void onUserJoined(UserInfo userInfo) {
        showMessage(userInfo.username + " has joined the lobby", true);
    }

    @Override
    public void onUserLeft(UserInfo userInfo) {
        showMessage(userInfo.username + " has left the lobby", true);
    }

    @Override
    public void onUserChanged(UserInfo userInfo) {
        String toPlay = (userInfo.isPlaying) ? " is now in a game session" : " is out of a game session";
        showMessage(userInfo.username + toPlay, true);
    }

    @Override
    public void setEntriesEnabled(boolean b) {
    }

    @Override
    public void onUsersFetched(List<UserInfo> list) {
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void showMessage(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.INFO, "Info", s);
    }

    @Override
    public void showError(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.ERROR, "Error", s);
    }

}
