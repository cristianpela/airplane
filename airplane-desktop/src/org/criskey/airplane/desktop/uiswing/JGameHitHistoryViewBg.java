package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.game.GameHitHistoryView;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.util.Position;

import java.util.Map;

/**
 * Created by criskey on 24/10/2016.
 */
public class JGameHitHistoryViewBg implements GameHitHistoryView {

    private MessageDisplay messageDisplay;

    public JGameHitHistoryViewBg() {
        messageDisplay = new JMessageDisplay(null);
    }

    @Override
    public void incomingBlow(Shot shot) {
        TargetProtocol tp = shot.targetProtocol;
        if (tp == TargetProtocol.WINNER) {
            showMsg("You won the game");
        } else if (tp == TargetProtocol.LOSER)
            showMsg("You lost the game");
        else
            showMsg("You were shot at " + Position.toAlgebraicNotation(shot.position) + ". Shot was a " + tp);
    }

    private void showMsg(String message){
        messageDisplay.show(MessageDisplay.Kind.INFO, "History Info", message);
    }
    @Override
    public void setConfiguration(PlaneConfiguration planeConfiguration) {
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void showMessage(String s, boolean b) {}

    @Override
    public void showError(String s, boolean b) {}
}
