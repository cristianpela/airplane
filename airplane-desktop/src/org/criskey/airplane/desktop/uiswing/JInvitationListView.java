package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.InvitationListView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.desktop.uiswing.widget.AccessibleListView;
import org.criskey.airplane.desktop.uiswing.widget.BasicCell;
import org.criskey.airplane.desktop.uiswing.widget.CellEvent;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Created by criskey on 23/9/2016.
 */
public class JInvitationListView extends AccessibleListView<String> implements InvitationListView {

    private static class InvitationCell extends BasicCell<String> {

        private static final String ACCEPT_EVENT = "Accept";

        private static final String REJECT_EVENT = "Reject";

        private String invitation;
        private final JButton accept;
        private final JButton reject;

        InvitationCell(String invitation, InvitationListPresenter presenter) {

            this.invitation = invitation;
            this.setLayout(new BorderLayout());

            JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            accept = new JButton(presenter.i18n(LanguageSupport.BTN_ACCEPT));
            reject = new JButton(presenter.i18n(LanguageSupport.BTN_REJECT));
            buttons.add(accept);
            buttons.add(reject);

            accept.addActionListener((e) -> {
                notifyListener(new CellEvent(ACCEPT_EVENT, invitation));
            });
            reject.addActionListener((e) -> {
                notifyListener(new CellEvent(REJECT_EVENT, invitation));
            });

            this.add(new JLabel(invitation), BorderLayout.WEST);
            this.add(buttons, BorderLayout.CENTER);
        }

        @Override
        public void update(String data) {
            invitation = data;
        }

        @Override
        public boolean dataEquals(String data) {
            return invitation.equals(data);
        }

        @Override
        public void setEnabled(boolean enabled) {
            accept.setEnabled(enabled);
            reject.setEnabled(enabled);
        }

    }

    private InvitationListPresenter presenter;

    private IView viewHelper;

    public JInvitationListView() {
        super((data) -> new InvitationCell(data, PresenterManager.plugReflect(InvitationListPresenter.class)));
        viewHelper = new ViewHelper(this,
                new JInvitationListViewBg(),
                presenter = PresenterManager.plugReflect(InvitationListPresenter.class),
                new JMessageDisplay(this));
        onResume();
    }

    @Override
    public void onCellInteraction(CellEvent e) {
        String username = e.data.toString();
        if (e.eventType.equals(InvitationCell.ACCEPT_EVENT)) {
            presenter.acceptInvite(username);
        } else if (e.eventType.equals(InvitationCell.REJECT_EVENT)) {
            presenter.rejectInvite(username);
        }
        removeEntry(username);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        removeAll();
        java.util.List<String> list = (java.util.List<String>) dataMap.get(InvitationListPresenter.KEY_LIST_INVITATIONS);
        setEntriesEnabled(!(Boolean) dataMap.get(InvitationListPresenter.KEY_BOOLEAN_ENTRIES_DISABLED));
        for (String inv : list) {
            addEntry(inv);
        }
        refresh();
    }

    @Override
    public void onNewInvitation(String invitation) {
        addEntry(invitation);
        refresh();
    }

    @Override
    public void onInvitationAccepted(String invitation) {
        removeEntry(invitation);
        refresh();
    }

    @Override
    public void setEntriesEnabled(boolean enabled) {
        setCellsEnabled(enabled);
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }

    @Override
    public void showMessage(String s, boolean b) {
        viewHelper.showMessage(s, b);
    }

    @Override
    public void showError(String s, boolean b) {
        viewHelper.showError(s, b);
    }

}
