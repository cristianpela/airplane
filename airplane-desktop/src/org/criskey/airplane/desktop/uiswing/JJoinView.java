package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.JoinPresenter;
import org.criskey.airplane.common.local.JoinView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Map;

/**
 * Created by criskey on 20/9/2016.
 */
public class JJoinView extends JPanel implements JoinView {

    private JoinPresenter presenter;

    private IView viewHelper;

    private final JButton joinButton;
    private final JButton cancelButton;

    private final JLabel lblUser;
    private final JTextField txtUser;

    private final JLabel lblPass;
    private final JPasswordField passField;

    private final JLabel waitMessage;


    public JJoinView() {

        presenter = PresenterManager.plugReflect(JoinPresenter.class);
        viewHelper = new ViewHelper(this, presenter, new JMessageDisplay(this));

        setLayout(new BorderLayout());
        JPanel grid = new JPanel(new GridBagLayout());

        final KeyAdapter enterListener = new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    presenter.join(txtUser.getText(), new String(passField.getPassword()));
            }
        };

        add(grid, BorderLayout.NORTH);

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;

        //first row
        c.gridx = 0;
        c.gridy = 0;
        grid.add(lblUser = new JLabel(), c);
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 0.5;
        grid.add(txtUser = new JTextField(), c);
        txtUser.requestFocusInWindow();
        txtUser.addKeyListener(enterListener);

        //2nd row
        c.gridx = 0;
        c.gridy = 1;
        grid.add(lblPass = new JLabel(), c);
        c.gridx = 1;
        c.gridy = 1;
        grid.add(passField = new JPasswordField(), c);
        passField.addKeyListener(enterListener);

        //3rd row
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.0;
        grid.add(waitMessage = new JLabel(), c);
        waitMessage.setVisible(false);

        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        grid.add(cancelButton = new JButton(), c);
        cancelButton.setVisible(false);
        cancelButton.addActionListener((e) -> {
            presenter.disconnect();
        });

        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 0.5;
        grid.add(joinButton = new JButton(), c);
        joinButton.addActionListener((e) -> {
            presenter.join(txtUser.getText(), new String(passField.getPassword()));
        });
        onResume();
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }

    @Override
    public void showMessage(String message, boolean b) {
        viewHelper.showMessage(message, b);
    }

    @Override
    public void showError(String error, boolean pop) {
        viewHelper.showError(error, pop);
        joinButton.setEnabled(true);
        waitMessage.setVisible(false);
        cancelButton.setVisible(false);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        lblUser.setText(presenter.i18n(LanguageSupport.SCREEN_JOIN_LBL_USERNAME));
        lblPass.setText(presenter.i18n(LanguageSupport.SCREEN_JOIN_LBL_PASSWORD));
        waitMessage.setText(presenter.i18n(LanguageSupport.SCREEN_JOIN_LBL_WAIT));
        txtUser.setToolTipText(presenter.i18n(LanguageSupport.SCREEN_JOIN_EDIT_USERNAME));
        joinButton.setText(presenter.i18n(LanguageSupport.SCREEN_JOIN_BTN_JOIN));
        cancelButton.setText(presenter.i18n(LanguageSupport.BTN_CANCEL));
        txtUser.setText((String) dataMap.get(JoinPresenter.KEY_STRING_USERNAME));
        passField.setText((String) dataMap.get(JoinPresenter.KEY_STRING_PASSWORD));
    }

    @Override
    public void onWait() {
        joinButton.setEnabled(false);
        cancelButton.setVisible(true);
        waitMessage.setVisible(true);
    }

    @Override
    public void onJoined() {
        joinButton.setEnabled(true);
        waitMessage.setVisible(false);
        cancelButton.setVisible(false);
    }

}
