package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.service.OnUIListenerSubscription;

/**
 * Created by criskey on 19/9/2016.
 */
public abstract class JListenerSubscription<T> extends OnUIListenerSubscription<T> {

    protected JListenerSubscription(Class<T> clazz) {
        super(clazz, OnSwingThreadRunCallback.INSTANCE);
    }
}
