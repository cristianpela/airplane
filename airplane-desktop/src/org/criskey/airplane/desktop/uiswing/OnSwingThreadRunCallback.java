package org.criskey.airplane.desktop.uiswing;


import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;


import javax.swing.*;

/**
 * Created by criskey on 20/9/2016.
 */
public class OnSwingThreadRunCallback implements OnUIRunCallback {

    public static final OnSwingThreadRunCallback INSTANCE = new OnSwingThreadRunCallback();

    private OnSwingThreadRunCallback() {
    }

    @Override
    public void runOnUI(OnUICodeInjector injector) {
        SwingUtilities.invokeLater(() -> {
            injector.inject();
        });
    }

    @Override
    public boolean isUIThread() {
        return SwingUtilities.isEventDispatchThread();
    }
}
