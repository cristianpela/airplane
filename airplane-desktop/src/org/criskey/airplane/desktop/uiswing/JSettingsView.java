package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.SettingsPresenter;
import org.criskey.airplane.common.local.SettingsView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Created by criskey on 25/11/2016.
 */
public class JSettingsView extends JPanel implements SettingsView {

    private SettingsPresenter presenter;

    private IView helper;


    private JLabel lblServer;

    private JLabel lblLangs;

    private JTextField editServer;

    private JComboBox<String> comboLangs;

    private JButton btnOK;

    public JSettingsView() {
        presenter = PresenterManager.plugReflect(SettingsPresenter.class);
        helper = new ViewHelper(this, presenter);

        setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        setLayout(new BorderLayout());
        JPanel grid = new JPanel(new GridBagLayout());
        add(grid, BorderLayout.NORTH);

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;

        //first row
        c.gridx = 0;
        c.gridy = 0;
        grid.add(lblServer = new JLabel(), c);
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        grid.add(editServer = new JTextField(), c);

        //2nd row
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0;
        grid.add(lblLangs = new JLabel(), c);
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.5;
        grid.add(comboLangs = new JComboBox<>(), c);


        JPanel commitPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        commitPanel.add(btnOK = new JButton());
        add(commitPanel, BorderLayout.SOUTH);
        btnOK.addActionListener(e -> {
            presenter.commit(comboLangs.getSelectedIndex(), editServer.getText());
        });
        onResume();
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            comboLangs.removeAllItems();
            String[] languages = (String[]) dataMap.get(SettingsPresenter.KEY_STRING_ARR_AVAILABLE_LANGUAGES);
            for (String lang : languages)
                comboLangs.addItem(lang);
            comboLangs.setSelectedIndex((Integer) dataMap.get(SettingsPresenter.KEY_STRING_CURR_LANGUAGE_IDX));
            editServer.setText((String) dataMap.get(SettingsPresenter.KEY_STRING_SERVER_ADDRESS));
        }
        lblServer.setText(presenter.i18n(LanguageSupport.SCREEN_SETTINGS_LBL_SERVER));
        lblServer.setToolTipText(presenter.i18n(LanguageSupport.SCREEN_SETTINGS_LBL_SERVER_DESC));
        lblLangs.setText(presenter.i18n(LanguageSupport.SCREEN_SETTINGS_LBL_LANGUAGE));
        btnOK.setText(presenter.i18n(LanguageSupport.BTN_OK));
    }

    @Override
    public void onResume() {
        helper.onResume();
    }

    @Override
    public void onPause() {
        helper.onPause();
    }

    @Override
    public void showMessage(String s, boolean b) {
        helper.showMessage(s, b);
    }

    @Override
    public void showError(String s, boolean b) {
        helper.showError(s, b);
    }

    @Override
    public void onCurrentLanguageChanged(String language) {

    }

    @Override
    public void onServerAddressChanged(String address) {
        editServer.setText(address);
    }
}
