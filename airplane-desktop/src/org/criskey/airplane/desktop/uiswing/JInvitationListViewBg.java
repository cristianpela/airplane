package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.InvitationListView;
import org.criskey.airplane.common.local.platform.MessageDisplay;

import java.util.Map;

/**
 * Created by criskey on 24/10/2016.
 */
public class JInvitationListViewBg implements InvitationListView {

    private MessageDisplay messageDisplay;

    public JInvitationListViewBg() {
        messageDisplay = new JMessageDisplay(null);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {}

    @Override
    public void onNewInvitation(String username) {
        showMessage("New invitation from " + username, true);
    }

    @Override
    public void onInvitationAccepted(String s) {}

    @Override
    public void setEntriesEnabled(boolean b) {}

    @Override
    public void onResume() {}

    @Override
    public void onPause() {}

    @Override
    public void showMessage(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.INFO, "Info", s);
    }

    @Override
    public void showError(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.ERROR, "Error", s);
    }
}
