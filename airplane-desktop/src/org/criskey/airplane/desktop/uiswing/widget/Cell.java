package org.criskey.airplane.desktop.uiswing.widget;

import javax.swing.*;

/**
 * Created by criskey on 23/9/2016.
 */
public abstract class Cell<D> extends JPanel {

    private CellListener listener;

    public abstract void update(D data);

    public abstract boolean dataEquals(D data);

    public abstract void setEnabled(boolean enabled);

    public void addCellListener(CellListener listener) {
        this.listener = listener;
    }

    protected void notifyListener(CellEvent event) {
        listener.onCellInteraction(event);
    }

}
