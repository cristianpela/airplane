package org.criskey.airplane.desktop.uiswing.widget;

/**
 * Created by criskey on 23/9/2016.
 */
public interface CellRenderer<D> {

    public Cell<D> render(D data);

}
