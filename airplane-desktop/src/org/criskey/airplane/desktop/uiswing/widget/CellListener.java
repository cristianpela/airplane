package org.criskey.airplane.desktop.uiswing.widget;

/**
 * Created by criskey on 14/10/2016.
 */
interface CellListener {

    void onCellInteraction(CellEvent e);

}
