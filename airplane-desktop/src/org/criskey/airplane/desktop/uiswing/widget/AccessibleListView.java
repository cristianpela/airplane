package org.criskey.airplane.desktop.uiswing.widget;


import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Collection;


/**
 * Created by criskey on 23/9/2016.
 */
public abstract class AccessibleListView<D> extends JPanel implements CellListener {

    private CellRenderer<D> cellRenderer;

    private boolean areCellsEnabled;

    public AccessibleListView(CellRenderer<D> cellRenderer) {
        this.cellRenderer = cellRenderer;
        areCellsEnabled = true;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentHidden(ComponentEvent e) {
                removeAll();
                refresh();
            }
        });
    }

    protected void removeEntry(D data) {
        Cell toRemove = getEntry(data);
        if (toRemove != null) {
            this.remove(toRemove);
            refresh();
        }
    }

    protected void refresh() {
        repaint();
        revalidate();
    }

    protected Cell getEntry(D data) {
        Cell line = null;
        for (int i = 0; i < getComponentCount(); i++) {
            Cell curr = (Cell) getComponent(i);
            if (curr.dataEquals(data)) {
                line = curr;
                break;
            }
        }
        return line;
    }

    protected void changeEntry(D data) {
        Cell entry = getEntry(data);
        if (entry != null) {
            entry.update(data);
        }
    }

    protected void addEntry(D data) {
        Cell entry = cellRenderer.render(data);
        if (entry != null) {
            entry.addCellListener(this);
            this.add(entry);
            entry.setEnabled(areCellsEnabled);
        }
    }

    protected void setCellsEnabled(boolean enabled) {
        areCellsEnabled = enabled;
        for (int i = 0; i < getComponentCount(); i++) {
            Cell curr = (Cell) getComponent(i);
            curr.setEnabled(enabled);
        }
    }

    protected void addAll(Collection<D> dataCol) {
        for (D data : dataCol) {
            addEntry(data);
        }
    }

}
