package org.criskey.airplane.desktop.uiswing.widget;

/**
 * Created by criskey on 14/10/2016.
 */
public class CellEvent {
    public final String eventType;
    public final Object data;

    public CellEvent(String eventType, Object data) {
        this.eventType = eventType;
        this.data = data;
    }
}
