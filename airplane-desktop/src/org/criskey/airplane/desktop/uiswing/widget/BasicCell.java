package org.criskey.airplane.desktop.uiswing.widget;

import javax.swing.*;
import java.awt.*;

/**
 * Created by criskey on 23/9/2016.
 */
public abstract class BasicCell<D> extends Cell<D> {

    public BasicCell() {
        this.setSize(new Dimension(300, 50));
        this.setMaximumSize(this.getSize());
        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.black),
                BorderFactory.createEmptyBorder(5, 10, 5, 10)));
    }

}
