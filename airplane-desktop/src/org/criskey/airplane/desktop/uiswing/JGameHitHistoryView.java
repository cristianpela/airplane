package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.game.GameHitHistoryPresenter;
import org.criskey.airplane.common.local.game.GameHitHistoryView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.i18n.TranslatorUtil;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 21/10/2016.
 */
public class JGameHitHistoryView extends JPanel implements GameHitHistoryView {

    private final JTextArea hitHistory;

    private final JPlayerUI jPlayerUI;

    private GameHitHistoryPresenter presenter;

    private IView viewHelper;

    private TitledBorder titledBorder;

    public JGameHitHistoryView(Position resolution) {
        setPreferredSize(new Dimension(resolution.column + 40, 0));

        presenter = PresenterManager.plugReflect(GameHitHistoryPresenter.class);
        viewHelper = new ViewHelper(this, new JGameHitHistoryViewBg(), presenter);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        jPlayerUI = new JPlayerUI(resolution, PlayerUI.Mode.DISABLED, 40);

        add(jPlayerUI);
        add(hitHistory = new JTextArea());
        hitHistory.setEditable(false);

        titledBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
                presenter.i18n(LanguageSupport.MENU_GAME_HIT_HISTORY));
        this.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createEmptyBorder(0, 10, 0, 10),
                        titledBorder)
        );
        onResume();
    }

    @Override
    public void incomingBlow(Shot shot) {
        incomingBlow(shot, true);
    }

    private void incomingBlow(Shot shot, boolean repaint) {
        String translatedShot = TranslatorUtil.getInstance().translateShot(shot);
        if (shot.targetProtocol != TargetProtocol.SKIP) {
            jPlayerUI.mark(shot, repaint, false);
            jPlayerUI.block(false);
            hitHistory.append(translatedShot + "\n");
        } else {
            hitHistory.append(translatedShot + "\n");
        }
    }

    @Override
    public void setConfiguration(PlaneConfiguration planeConfiguration) {
        jPlayerUI.configureBoard(planeConfiguration);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        hitHistory.setText("");
        if (dataMap != null) {
            PlaneConfiguration planeConfiguration = (PlaneConfiguration) dataMap.get(GameHitHistoryPresenter.KEY_PLANE_CONFIGURATION);
            jPlayerUI.configureBoard(planeConfiguration);
            List<Shot> list = (List<Shot>) dataMap.get(GameHitHistoryPresenter.KEY_LIST_HIT_HISTORY);
            for (Shot shot : list)
                incomingBlow(shot, false);
            jPlayerUI.repaint();
        }
        titledBorder.setTitle(presenter.i18n(LanguageSupport.MENU_GAME_HIT_HISTORY));
        repaint();//update the title changing
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }

    @Override
    public void showMessage(String s, boolean b) {
        viewHelper.showMessage(s, b);
    }

    @Override
    public void showError(String s, boolean b) {
        viewHelper.showError(s, b);
    }
}
