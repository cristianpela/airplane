package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.Screen;

import javax.swing.*;

/**
 * Created by criskey on 24/10/2016.
 */
public class ScreenContainer extends JInternalFrame {

    public final Screen screen;

    private String titleLangKey;

    private LanguageSupport lang;

    public ScreenContainer(Screen screen, LanguageSupport lang, String titleLangKey) {
        super(lang.i18n(titleLangKey));
        this.screen = screen;
        this.titleLangKey = titleLangKey;
        this.lang = lang;
    }

    public ScreenContainer(Screen screen, LanguageSupport lang, String titleLangKey, boolean resizable) {
        super(lang.i18n(titleLangKey), resizable);
        this.screen = screen;
        this.titleLangKey = titleLangKey;
        this.lang = lang;
    }

    public ScreenContainer(Screen screen, LanguageSupport lang, String titleLangKey, boolean resizable, boolean closable) {
        super(lang.i18n(titleLangKey), resizable, closable);
        this.screen = screen;
        this.titleLangKey = titleLangKey;
        this.lang = lang;
    }

    public ScreenContainer(Screen screen, LanguageSupport lang, String titleLangKey, boolean resizable, boolean closable, boolean maximizable) {
        super(lang.i18n(titleLangKey), resizable, closable, maximizable);
        this.screen = screen;
        this.titleLangKey = titleLangKey;
        this.lang = lang;
    }

    public ScreenContainer(Screen screen, LanguageSupport lang, String titleLangKey, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
        super(lang.i18n(titleLangKey), resizable, closable, maximizable, iconifiable);
        this.screen = screen;
        this.titleLangKey = titleLangKey;
        this.lang = lang;
    }

    public void updateTitle() {
        setTitle(lang.i18n(titleLangKey));
    }

}
