package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.game.GameSetupView;
import org.criskey.airplane.common.local.platform.MessageDisplay;

import java.util.Map;

/**
 * Created by criskey on 24/10/2016.
 */
public class JGameSetupViewBg implements GameSetupView {

    private MessageDisplay messageDisplay;

    public JGameSetupViewBg(){
        messageDisplay = new JMessageDisplay(null);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {}

    @Override
    public void reset() {}

    @Override
    public void onResume() {}

    @Override
    public void onPause() {}

    @Override
    public void showMessage(String msg, boolean pop) {
        messageDisplay.show(MessageDisplay.Kind.INFO, "", msg);
    }

    @Override
    public void showError(String err, boolean pop) {
        messageDisplay.show(MessageDisplay.Kind.INFO, "", err);
    }
}
