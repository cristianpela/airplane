package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.LobbyListView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.desktop.uiswing.widget.AccessibleListView;
import org.criskey.airplane.desktop.uiswing.widget.BasicCell;
import org.criskey.airplane.desktop.uiswing.widget.CellEvent;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created by criskey on 19/9/2016.
 */
public class JLobbyListView extends AccessibleListView<UserInfo>
        implements LobbyListView {

    private LobbyListPresenter presenter;

    private IView viewHelper;

    private static class LobbyCell extends BasicCell<UserInfo> {

        private final JButton invite;
        private final JLabel nameLabel;

        private boolean isPlaying, isCellEnabled;

        private LobbyListPresenter presenter;

        LobbyCell(UserInfo user, LobbyListPresenter presenter) {

            this.presenter = presenter;

            isCellEnabled = true;
            isPlaying = user.isPlaying;

            this.setLayout(new BorderLayout());
            invite = new JButton((isPlaying)
                    ? presenter.i18n(LanguageSupport.SCREEN_LOBBY_BTN_PLAYING)
                    : presenter.i18n(LanguageSupport.SCREEN_LOBBY_BTN_INVITE));
            invite.setEnabled(!user.isPlaying);
            nameLabel = new JLabel(user.username);

            this.add(nameLabel, BorderLayout.WEST);
            this.add(invite, BorderLayout.EAST);

            invite.addActionListener((e) -> {
                notifyListener(new CellEvent("", user));
            });

        }

        @Override
        public void update(UserInfo data) {
            isPlaying = data.isPlaying;
            invite.setText((isPlaying)
                    ? presenter.i18n(LanguageSupport.SCREEN_LOBBY_BTN_PLAYING)
                    : presenter.i18n(LanguageSupport.SCREEN_LOBBY_BTN_INVITE));
            if (isCellEnabled)
                invite.setEnabled(!isPlaying);
        }

        @Override
        public boolean dataEquals(UserInfo data) {
            return nameLabel.getText().equals(data.username);
        }

        @Override
        public void setEnabled(boolean enabled) {
            isCellEnabled = enabled;
            if (isCellEnabled)
                invite.setEnabled(!isPlaying);
            else
                invite.setEnabled(isCellEnabled);
        }

    }

    public JLobbyListView() {
        super((data) -> new LobbyCell(data, PresenterManager.plugReflect(LobbyListPresenter.class)));
        viewHelper = new ViewHelper(this,
                new JLobbyListViewBg(),
                presenter = PresenterManager.plugReflect(LobbyListPresenter.class),
                new JMessageDisplay(null));
        onResume();
    }


    @Override
    public void onUsersFetched(List<UserInfo> users) {
        addUsers(users);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        addUsers((List<UserInfo>) dataMap.get(LobbyListPresenter.KEY_LIST_LOBBY));
        setEntriesEnabled(!(Boolean) dataMap.get(LobbyListPresenter.KEY_BOOLEAN_ENTRIES_DISABLED));
    }

    @Override
    public void onUserJoined(UserInfo user) {
        addEntry(user);
        refresh();
    }

    @Override
    public void onUserLeft(UserInfo user) {
        removeEntry(user);
    }

    @Override
    public void onUserChanged(UserInfo user) {
        changeEntry(user);
        refresh();
    }


    @Override
    public void setEntriesEnabled(boolean enabled) {
        setCellsEnabled(enabled);
    }

    @Override
    public void onCellInteraction(CellEvent e) {
        presenter.sendInvitation((UserInfo) e.data);
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }

    @Override
    public void showMessage(String s, boolean b) {
        viewHelper.showMessage(s, b);
    }

    @Override
    public void showError(String error, boolean b) {
        viewHelper.showError(error, b);
    }

    private void addUsers(List<UserInfo> users) {
        removeAll();
        if (!users.isEmpty()) {
            for (UserInfo info : users) {
                addEntry(info);
            }
        }
        refresh();
    }
}
