package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.Screen;

import java.util.LinkedList;
import java.util.List;


/**
 * Created by criskey on 20/9/2016.
 */
public class CompositeRouter implements Router<Router<Object>> {

    private List<Router> routers;

    public CompositeRouter() {
        routers = new LinkedList<>();
    }

    @Override
    public Screen show(Screen toScreen) {
        for (Router r : routers) {
            r.show(toScreen);
        }
        return toScreen;
    }

    @Override
    public void hide(Screen screen) {
        for (Router r : routers) {
            r.hide(screen);
        }
    }

    @Override
    public void addScreen(Screen screen, Router<Object> router) {
        routers.add(router);
    }

}
