package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.InteractivePlayer;
import org.criskey.airplane.common.local.platform.InteractionEvent;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.local.platform.PlayerUIOps;
import org.criskey.airplane.common.local.platform.RePainter;
import org.criskey.airplane.common.log.Logger;
import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by criskey on 13/9/2016.
 */
public class JPlayerUI extends JComponent implements PlayerUIOps {

    private PlayerUI playerUI;

    private JPainter painter;

    private MouseAdapter mouseAdapter = new MouseAdapter() {
        @Override
        public void mouseDragged(MouseEvent e) {
            Position mousePos = Position.asXY(e.getX(), e.getY());
            playerUI.onInteraction(new InteractionEvent(mousePos, InteractionEvent.Type.TRANSLATE));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            Position mousePos = Position.asXY(e.getX(), e.getY());
            if (e.getButton() == MouseEvent.BUTTON3) {
                playerUI.onInteraction(new InteractionEvent(mousePos, InteractionEvent.Type.ROTATION));
            } else if (e.getButton() == MouseEvent.BUTTON1) {
                playerUI.onInteraction(new InteractionEvent(mousePos, InteractionEvent.Type.CHECK));
                playerUI.onInteraction(new InteractionEvent(mousePos, InteractionEvent.Type.TRANSLATE_END));
            }

        }

        @Override
        public void mousePressed(MouseEvent e) {
            Position mousePos = Position.asXY(e.getX(), e.getY());
            playerUI.onInteraction(new InteractionEvent(mousePos, InteractionEvent.Type.TRANSLATE_START));
        }
    };

    private ComponentAdapter componentAdapter = new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
            //TODO: implement resize?
            Component component = e.getComponent();
            playerUI.onResize(Position.asDimension(component.getWidth() - 16, component.getHeight() - 16));
        }

    };

    public JPlayerUI(PlayerUI playerUI) {
        init(playerUI);
    }

    public JPlayerUI(Position resolution, PlayerUI.Mode mode, int margin) {
        init(new PlayerUI(resolution, mode, margin));
    }

    private void init(PlayerUI playerUI) {
        this.playerUI = playerUI;
        Position playerUISize = playerUI.getSize();
        this.setPreferredSize(new Dimension(playerUISize.column, playerUISize.row + 10));
        playerUI.registerRepainter(new JRePainter());
        playerUI.registerMessageDisplay(new JMessageDisplay(this));
        painter = new JPainter();
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);
        addComponentListener(componentAdapter);
    }

    @Override
    public void paintComponent(Graphics g) {
        painter.g2 = (Graphics2D) g;
        painter.g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        playerUI.draw(painter);
    }


    private class JRePainter implements RePainter {
        @Override
        public void paintAgain() {
            repaint();
            revalidate();
        }
    }

    private class JPainter implements org.criskey.airplane.common.local.platform.Painter {

        Graphics2D g2;

        @Override
        public void drawRect(int colLeft, int rowTop, int colRight, int rowBottom) {
            int w = Math.abs(colRight - colLeft);
            int h = Math.abs(rowBottom - rowTop);
            g2.fillRect(colLeft, rowTop, w, h);
        }

        @Override
        public void drawLine(int colFrom, int rowFrom, int colTo, int rowTo) {
            g2.drawLine(colFrom, rowFrom, colTo, rowTo);
        }

        @Override
        public void setColor(String colorCode) {
            g2.setPaint(Color.decode(colorCode));
        }

        @Override
        public void drawString(String string, Position position) {
            g2.drawString(string, position.column, position.row);
        }
    }

    @Override
    public PlaneConfiguration getConfiguration() {
        return playerUI.getConfiguration();
    }

    @Override
    public void block(boolean block) {
        playerUI.block(block);
    }

    @Override
    public boolean isBlocked() {
        return playerUI.isBlocked();
    }

    @Override
    public void mark(Shot shot, boolean repaint, boolean notifyCallback) {
        playerUI.mark(shot, repaint, notifyCallback);
    }

    @Override
    public void configureBoard(PlaneConfiguration configuration) {
        playerUI.configureBoard(configuration);
    }

    @Override
    public void addBoardChangeCallback(BoardChangeCallback boardChangeCallback) {
        playerUI.addBoardChangeCallback(boardChangeCallback);
    }

    @Override
    public void reset() {
        playerUI.reset();
    }

}
