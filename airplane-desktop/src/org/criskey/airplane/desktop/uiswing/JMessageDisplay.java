package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.local.platform.MessageDisplay;

import javax.swing.*;
import java.awt.*;

/**
 * Created by criskey on 20/9/2016.
 */
public class JMessageDisplay implements MessageDisplay {

    private Container container;

    public JMessageDisplay(Container container) {
        this.container = container;
    }

    @Override
    public void show(Kind kind, String title, String message) {
        JOptionPane.showMessageDialog(container, message, title, kind.ordinal());
    }
}
