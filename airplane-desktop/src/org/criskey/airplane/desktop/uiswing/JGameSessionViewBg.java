package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.game.GameSessionView;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.util.Position;

import java.util.Map;

/**
 * Created by criskey on 24/10/2016.
 */
public class JGameSessionViewBg implements GameSessionView {

    private MessageDisplay messageDisplay;

    public JGameSessionViewBg() {
        messageDisplay = new JMessageDisplay(null);
    }

    @Override
    public void responseAttempt(Shot shot) {
        messageDisplay.show(
                MessageDisplay.Kind.INFO,
                "Shot Message",
                "Your shot at " + Position.toAlgebraicNotation(shot.position) + " was a " + shot.targetProtocol);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
    }

    @Override
    public void takeTurn(boolean hasTurn) {
        if (hasTurn)
            showMessage("Take your turn...", true);
    }

    @Override
    public void onTurnCountDown(int i) {
        if (i == 0)
            showError("Your turn has expired!", true);
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void showMessage(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.INFO, "Info", s);
    }

    @Override
    public void showError(String s, boolean b) {
        messageDisplay.show(MessageDisplay.Kind.ERROR, "Error", s);
    }


}
