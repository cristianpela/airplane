package org.criskey.airplane.desktop.uiswing;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSessionView;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.MessageDisplay;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.local.viewpresenter.ViewHelper;
import org.criskey.airplane.common.util.Position;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.local.game.GameSessionPresenter.TURN_SECONDS;

/**
 * Created by criskey on 20/10/2016.
 */
public class JGameSessionView extends JPanel implements GameSessionView {

    private GameSessionPresenter presenter;

    private JPlayerUI playerUI;
    private JButton leaveSessionBtn;
    private JLabel messageBarLbl;
    private JProgressBar turnCountDownBar;

    private MessageDisplay messageDisplay;
    private IView viewHelper;

    private PlayerUI.BoardChangeCallback boardChangeCallback = new PlayerUI.BoardChangeCallback() {
        @Override
        public void onBoardChanged(Position position) {
            presenter.attemptHit(position);
        }

        @Override
        public void onError(String errKey) {
            viewHelper.showError(presenter.i18n(errKey), true);
        }
    };

    public JGameSessionView(Position resolution) {

        messageDisplay = new JMessageDisplay(this);
        viewHelper = new ViewHelper(this,
                new JGameSessionViewBg(),
                presenter = PresenterManager.plugReflect(GameSessionPresenter.class),
                new JMessageDisplay(this));

        playerUI = new JPlayerUI(Position.asDimension(390, 390), PlayerUI.Mode.GAME, 40);
        playerUI.addBoardChangeCallback(boardChangeCallback);

        messageBarLbl = new JLabel();
        leaveSessionBtn = new JButton();

        turnCountDownBar = new JProgressBar(0, TURN_SECONDS);
        turnCountDownBar.setStringPainted(true);
        turnCountDownBar.setVisible(false);

        setListeners();

        setLayout(new BorderLayout());
        add(playerUI, BorderLayout.CENTER);
        JPanel bottomBar = new JPanel();
        bottomBar.setLayout(new BoxLayout(bottomBar, BoxLayout.X_AXIS));
        Dimension rigidDim = new Dimension(10, 0);
        bottomBar.add(Box.createRigidArea(rigidDim));
        bottomBar.add(messageBarLbl);
        bottomBar.add(Box.createRigidArea(rigidDim));
        bottomBar.add(turnCountDownBar);
        bottomBar.add(Box.createHorizontalGlue());
        bottomBar.add(leaveSessionBtn);
        bottomBar.add(Box.createRigidArea(rigidDim));
        add(bottomBar, BorderLayout.SOUTH);

        bottomBar.setBorder(BorderFactory.createRaisedSoftBevelBorder());

        JGameHitHistoryView hist = new JGameHitHistoryView(Position.asDimension(150, 150));
        add(hist, BorderLayout.EAST);

        onResume();
    }

    private void setListeners() {
        leaveSessionBtn.addActionListener((e) -> {
            presenter.leaveSession();
        });
    }

    @Override
    public void responseAttempt(Shot shot) {
        if (shot.targetProtocol != TargetProtocol.SKIP)
            playerUI.mark(shot, true, false);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        if (dataMap != null) {
            List<Shot> attemptShots = (List<Shot>) dataMap.get(GameSessionPresenter.KEY_LIST_ATTEMPTS);
            for (Shot shot : attemptShots) {
                if (shot.targetProtocol != TargetProtocol.SKIP) {
                    playerUI.mark(shot, false, false);
                    playerUI.block(false);
                }
            }
            playerUI.repaint();
            showMessage((String) dataMap.get(GameSessionPresenter.KEY_STRING_STATUS_MESSAGE), false);
            Boolean hasTurn = (Boolean) dataMap.get(GameSessionPresenter.KEY_BOOLEAN_HAS_TURN);
            takeTurn(hasTurn);
        }
        leaveSessionBtn.setText(presenter.i18n(LanguageSupport.SCREEN_GAME_BTN_LEAVE));
    }

    @Override
    public void takeTurn(boolean hasTurn) {
        playerUI.block(!hasTurn);
        turnCountDownBar.setVisible(hasTurn);
    }

    @Override
    public void onTurnCountDown(int secondsToEnd) {
        if (secondsToEnd <= 10)
            Toolkit.getDefaultToolkit().beep();
        turnCountDownBar.setValue(secondsToEnd);
    }

    @Override
    public void onResume() {
        viewHelper.onResume();
    }

    @Override
    public void onPause() {
        viewHelper.onPause();
    }

    @Override
    public void showMessage(String msg, boolean pop) {
        if (pop) viewHelper.showMessage(msg, pop);
        else messageBarLbl.setText(msg);
    }

    @Override
    public void showError(String msg, boolean pop) {
        if (pop) viewHelper.showMessage(msg, pop);
        else messageBarLbl.setText(msg);
    }

}
