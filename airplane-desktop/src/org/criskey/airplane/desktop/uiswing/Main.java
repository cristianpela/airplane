package org.criskey.airplane.desktop.uiswing;

import com.google.gson.Gson;
import org.criskey.airplane.common.local.*;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.DataBus;
import org.criskey.airplane.common.local.bus.DataBusListenerWSNetwork;
import org.criskey.airplane.common.local.bus.StateContainer;
import org.criskey.airplane.common.local.game.GameHitHistoryPresenter;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSetupPresenter;
import org.criskey.airplane.common.local.i18n.LanguageChangeListener;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.i18n.TranslatorUtil;
import org.criskey.airplane.common.local.persistance.CompoundStorage;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.PrefixedStorageProxy;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.router.GameMiddleware;
import org.criskey.airplane.common.local.router.LoginMiddleware;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.AirplaneStompService;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.criskey.airplane.common.local.service.ServiceSubscriberHolder;
import org.criskey.airplane.common.local.util.FluentMap;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.criskey.airplane.common.util.Position;
import org.criskey.airplane.desktop.i18n.VanillaLanguageSupport;
import org.criskey.airplane.desktop.persistance.LangFilesStorage;
import org.criskey.airplane.desktop.persistance.PropertiesSettingsStorage;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 6/9/2016.
 */
public class Main {


    public static void main(String[] args) throws Exception {

        OnUIRunCallback uiRunCb = OnSwingThreadRunCallback.INSTANCE;
        Gson gson = new Gson();

        Storage storage = new CompoundStorage(
                new PrefixedStorageProxy(Prefix.PREF, new PropertiesSettingsStorage()),
                new PrefixedStorageProxy(Prefix.FILES, new LangFilesStorage()));

        //setting language from storage
        LanguageSupport languageSupport = new VanillaLanguageSupport();
        storage.load(Prefix.PREF, Storage.PRF_CURR_LANGUAGE_CODE,
                (String data, Error err) -> {
                    if (data != null)
                        languageSupport.setLocale(LanguageSupport.getLocaleFromFormat(data));
                });
        TranslatorUtil.init(languageSupport);

        //databus and services setup
        DataBus dataBus = new DataBus(uiRunCb);
        StateContainer stateContainer = new StateContainer(dataBus, new FluentMap()
                .put(AppEventType.LOGGED, JoinPresenter.JoinState.EMPTY)
                .put(AppEventType.LOBBY, LobbyListPresenter.LobbyState.EMPTY)
                .get());

        IAirplaneStompService service = new AirplaneStompService(/*"192.168.0.13:8080",*/new DataBusListenerWSNetwork(stateContainer), gson);
        storage.load(Prefix.PREF, Storage.PRF_SERVER_ADDRESS,
                (String data, Error err) -> service.changeHost(data));
        ServiceSubscriberHolder subscriberHolder = new ServiceSubscriberHolder(service, stateContainer, gson, uiRunCb);
        stateContainer.addDataBusListener(subscriberHolder);

        Position resolution = Position.asDimension(800, 600);
        Router compositeRouter = new CompositeRouter();

        //routing setup
        GameMiddleware gameMiddleware = new GameMiddleware(stateContainer);
        LoginMiddleware loginMiddleware = new LoginMiddleware(stateContainer);
        gameMiddleware.setNextInChain(loginMiddleware);
        loginMiddleware.setPreviousInChain(gameMiddleware);

        stateContainer.addDataBusListener(gameMiddleware);
        stateContainer.addDataBusListener(loginMiddleware);

        PresenterManager.createInstance(service,
                compositeRouter,
                uiRunCb,
                gson,
                stateContainer, storage, languageSupport);

        PresenterManager.plugReflect(MainPresenter.class);
        LobbyListPresenter lobbyListPresenter = PresenterManager.plugReflect(LobbyListPresenter.class);
        lobbyListPresenter.onAttach(new JLobbyListViewBg());
        GameSetupPresenter gameSetupPresenter = PresenterManager.plugReflect(GameSetupPresenter.class);
        gameSetupPresenter.onAttach(new JGameSetupViewBg());
        GameSessionPresenter gameSessionPresenter = PresenterManager.plugReflect(GameSessionPresenter.class);
        gameSessionPresenter.enableTurnCountDownTimer(true);
        gameSessionPresenter.onAttach(new JGameSessionViewBg());
        GameHitHistoryPresenter gameHitHistoryPresenter = PresenterManager.plugReflect(GameHitHistoryPresenter.class);
        gameHitHistoryPresenter.onAttach(new JGameSessionViewBg());
        Presentable invitationPresenter = PresenterManager.plugReflect(InvitationListPresenter.class);
        invitationPresenter.onAttach(new JInvitationListViewBg());
        PresenterManager.plugReflect(SettingsPresenter.class);


        SwingUtilities.invokeLater(() -> {
            JMainView mainView = new JMainView(resolution);
            Router windowRouter = new IFrameRouter(mainView, new IFrameFactory(resolution, languageSupport), gameMiddleware);
            languageSupport.addLanguageChangeListener((LanguageChangeListener) windowRouter);
            compositeRouter.addScreen(null, windowRouter);
            mainView.setVisible(true);
        });

        //compositeRouter.show(Router.Screen.MAIN, Router.Screen.GAME_FRAME);
//        new Thread("Change lang thread") {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(TimeUnit.SECONDS.toMillis(50));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                uiRunCb.runOnUI(() -> {
//                    languageSupport.setLocale(Locale.getDefault());
//                });
//            }
//        }.start();

    }

}
