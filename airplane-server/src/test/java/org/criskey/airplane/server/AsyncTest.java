package org.criskey.airplane.server;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by criskey on 26/1/2017.
 */
public class AsyncTest {

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public <T> T async(Asyncable<T> completable) {
        CompletableFuture<T> results = new CompletableFuture<>();
        completable.promise = results;
        try {
            executor.submit(completable);
            return results.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }


    abstract class Asyncable<T> implements Runnable {

        private CompletableFuture<T> promise;

        abstract void next(CompletableFuture<T> promise);

        @Override
        public void run() {
            next(promise);
        }
    }

    public interface Result<T>{
        void result(T result);
    }
}
