package org.criskey.airplane.server;

import com.google.gson.Gson;
import org.criskey.airplane.common.NetContract;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.service.AirplaneStompService;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.criskey.airplane.common.NetContract.*;
import static org.criskey.airplane.common.NetContract.WebSocketBrokers.PREFIX_USER_DEST;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by criskey on 25/1/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        WithSecurityContextTestExecutionListener.class})
public class AirplaneControllerTest extends AsyncTest {

    @LocalServerPort
    int port;

    private IAirplaneStompService mService;
    private Gson mGson;

    // @Test
    public void request_Test() throws Exception {
        RestTemplate template = new BasicAuthRestTemplate("foo", "bar");
        String url = NetContract.paramedRequest(createHttpRoot("localhost:" + port), "lobby/join/{name}", "foo");
        ResponseEntity<String> entity = template.getForEntity(url, String.class);
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));


        template = new BasicAuthRestTemplate("moo", "var");
        url = NetContract.paramedRequest(createHttpRoot("localhost:" + port), "lobby/join/{name}", "moo");
        entity = template.getForEntity(url, String.class);
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));
        UserInfo[] users = new Gson().fromJson(entity.getBody(), UserInfo[].class);
        assertThat(users.length, is(1));
        assertThat(users[0].username, is("foo"));
    }

    //  @Test
    public void websocket() {
//        RestTemplate template = new BasicAuthRestTemplate("foo", "bar");
//        String url = NetContract.paramedRequest(createHttpRoot("localhost:" + port), "lobby/join/{name}", "foo");
//        template.getForEntity(url, String.class);

        StandardWebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        SockJsClient sockJsClient = new SockJsClient(
                Arrays.asList(new WebSocketTransport(simpleWebSocketClient)));
        sockJsClient.setMessageCodec(new Jackson2SockJsMessageCodec());
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        StompHeaders stompHeaders = new StompHeaders();
        stompHeaders.add(StompHeaders.LOGIN, "foo");
        stompHeaders.add(StompHeaders.PASSCODE, "bar");
        WebSocketHttpHeaders wsHttpHeaders = new WebSocketHttpHeaders(authHeader("foo", "bar"));
        stompClient.connect("ws://localhost:" + port + "/airplane-ws",
                wsHttpHeaders,
                stompHeaders,
                new StompHandler());
        // stompClient.stop();


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        stompClient.stop();
    }


    @Before
    public void initCon() throws URISyntaxException {
        mGson = new Gson();
        mService = new AirplaneStompService("localhost:" + port, i -> {}, mGson);
    }

    @Test
    public void testJoin() throws URISyntaxException, ExecutionException, InterruptedException {
       String result = async(new Asyncable<String>() {
           @Override
           void next(CompletableFuture<String> promise) {
               mService.connect("foo", "bar", (data, error) -> {
                   mService.addSubscription(SUB_TOPIC_JOINED, (headers, body) -> {
                       UserInfo user = mGson.fromJson(body, UserInfo.class);
                       promise.complete(user.username);
                   });

                   try {
                       Thread.sleep(3000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   mService.joinLobby();
               });
           }
       });
        mService.disconnect();
        assertEquals("foo", result);
    }



    @Test
    public void testLobby() throws URISyntaxException, ExecutionException, InterruptedException {
        boolean result = async(new Asyncable<Boolean>() {
            @Override
            void next(CompletableFuture<Boolean> promise) {
                mService.connect("foo", "bar", (data, error) -> {
                    mService.addSubscription(PREFIX_USER_DEST + SUB_LOBBY, (headers, body) -> {
                        promise.complete(true);
                    });
                });
            }
        });
        mService.disconnect();
        assertTrue(result);

    }


    private HttpHeaders authHeader(String user, String password) {
        String token = new String(Base64.getEncoder().encode((user + ":" + password).getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + token);
        return headers;
    }


    class StompHandler implements StompSessionHandler {

        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            session.subscribe(PREFIX_USER_DEST, new StompFrameHandler() {
                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return null;
                }

                @Override
                public void handleFrame(StompHeaders headers, Object payload) {
                    boolean subscribed = true;
                    System.out.println("Subscribed to lobby");
                }
            });


        }

        @Override
        public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {

        }

        @Override
        public void handleTransportError(StompSession session, Throwable exception) {

        }

        @Override
        public Type getPayloadType(StompHeaders headers) {
            return null;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {

        }
    }


    class BasicAuthRestTemplate extends RestTemplate {

        public BasicAuthRestTemplate(String username, String password) {
            addAuthentication(username, password);
        }

        private void addAuthentication(String username, String password) {
            if (username == null) {
                return;
            }
            List<ClientHttpRequestInterceptor> interceptors = Collections
                    .<ClientHttpRequestInterceptor>singletonList(
                            new BasicAuthorizationInterceptor(username, password));
            setRequestFactory(new InterceptingClientHttpRequestFactory(getRequestFactory(),
                    interceptors));
        }

        class BasicAuthorizationInterceptor implements
                ClientHttpRequestInterceptor {

            private final String username;

            private final String password;

            public BasicAuthorizationInterceptor(String username, String password) {
                this.username = username;
                this.password = (password == null ? "" : password);
            }

            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] body,
                                                ClientHttpRequestExecution execution) throws IOException {
                byte[] token = Base64.getEncoder().encode(
                        (this.username + ":" + this.password).getBytes());
                request.getHeaders().add("Authorization", "Basic " + new String(token));
                return execution.execute(request, body);
            }


        }

    }


}