package org.criskey.airplane.server;

import org.criskey.airplane.common.Lobby;
import org.criskey.airplane.common.User;
import org.criskey.airplane.common.log.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

import java.util.concurrent.locks.Lock;

/**
 * Created by criskey on 4/10/2016.
 */
public class WebSocketChannelInterceptor extends ChannelInterceptorAdapter {

    @Autowired
    private Lobby lobby;

    @Autowired
    private Lock lock;

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
        String sessionId = headerAccessor.getSessionId();
        StompCommand command = headerAccessor.getCommand();
        if (command != null) {
            switch (command) {
                case CONNECTED:
                    break;
                case DISCONNECT:
//                    lock.lock();
//                    try {
//                        User user = lobby.getUserById(sessionId);
//                        if (user != null) {
//                            Logger.out.print(this, "User " + user + " disconnected");
//                            lobby.leave(user.username);
//
//                        }
//                    } finally {
//                        lock.unlock();
//                    }
                    break;
            }
        }
    }
}
