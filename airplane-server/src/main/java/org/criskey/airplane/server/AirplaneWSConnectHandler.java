package org.criskey.airplane.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;

import java.security.Principal;

/**
 * Created by criskey on 3/2/2017.
 */
@Component
public class AirplaneWSConnectHandler implements ApplicationListener<SessionConnectEvent> {

    @Autowired
    AirplaneServiceImpl mService;

    public AirplaneWSConnectHandler() {
    }

    @Override
    public void onApplicationEvent(SessionConnectEvent event) {
        MessageHeaders headers = event.getMessage().getHeaders();
        Principal user = SimpMessageHeaderAccessor.getUser(headers);
        String id = SimpMessageHeaderAccessor.getSessionId(headers);
        mService.join(user.getName(), id);
    }
}
