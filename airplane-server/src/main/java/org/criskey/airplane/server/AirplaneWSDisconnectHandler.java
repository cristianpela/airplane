package org.criskey.airplane.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;

/**
 * Created by criskey on 3/2/2017.
 */
@Component
public class AirplaneWSDisconnectHandler implements ApplicationListener<SessionDisconnectEvent> {
    @Autowired
    AirplaneServiceImpl mService;

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        MessageHeaders headers = event.getMessage().getHeaders();
        Principal user = SimpMessageHeaderAccessor.getUser(headers);
        mService.leave(user.getName());
    }
}
