package org.criskey.airplane.server;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.User;
import org.criskey.airplane.common.UserInfo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import java.util.List;

/**
 * Created by criskey on 1/12/2016.
 */
public interface AirplaneService {


    UserInfo join(String name, String sessionId);


    boolean isUsernameAvailable(String name);


    User leave(String name);


    void invite(String name, String sessionId);


    void invitationStatus(String name, boolean isAccepted, String sessionId);


    void gameConfig(GameNetworkProtocol protocol, String sessionId) throws Exception;


    void target(GameNetworkProtocol protocol, String sessionId) throws Exception;


    void leaveGameSession(GameNetworkProtocol protocol, String sessionId);


    List<UserInfo> lobby(String filterPrincipal);
}
