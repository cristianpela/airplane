package org.criskey.airplane.server;


import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableScheduling
public class AirplaneServerApplication {

    static SpringApplication springApplication;

    static ConfigurableApplicationContext context;

    static ExecutorService executor = Executors.newSingleThreadExecutor();

    static String[] appArgs;

    public static  JToggleButton btn = new JToggleButton("Start");

    private static ItemListener serverStarter = new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
            int state = e.getStateChange();
            if (state == ItemEvent.SELECTED) {
                updateUI("Starting...", false);
                executor.submit(() -> {
                    context = springApplication.run(AirplaneServerApplication.class, appArgs);
                });
            } else {
                updateUI("Stopping...", false);
                executor.submit(() -> {
                    springApplication.exit(context);
                });
            }
        }
    };

    public static void updateUI(String text, boolean enabled) {
        SwingUtilities.invokeLater(() -> {
            btn.setText(text);
            btn.setEnabled(enabled);
        });
    }


    public static void main(String[] args) {
        appArgs = args;
        springApplication = new SpringApplication(new Object[]{AirplaneServerApplication.class}, args);
        org.criskey.airplane.common.log.Logger.change(new SpringLogger());
        final Logger logger = Logger.getRootLogger();
        SwingUtilities.invokeLater(() -> {
            ServerInterface serverInterface = new ServerInterface();
            serverInterface.appender.setLayout(new PatternLayout("%5p [%t] (%F:%L) - %m%n"));
            logger.addAppender(serverInterface.appender);
            serverInterface.addServerStarter(serverStarter);
            serverInterface.setVisible(true);
        });

    }


    private static class ServerInterface extends JFrame {

        AppenderSkeleton appender = new AppenderSkeleton() {
            @Override
            public void close() {}

            @Override
            public boolean requiresLayout() {
                return true;
            }

            @Override
            protected void append(LoggingEvent event) {
                SwingUtilities.invokeLater(() -> logArea.append(event.getRenderedMessage() + "\n"));
            }
        };
        private JTextArea logArea;

        public ServerInterface() {
            setTitle("Server Interface");
            logArea = new JTextArea();
            this.setLayout(new BorderLayout());
            this.add(new JScrollPane(logArea), BorderLayout.CENTER);
            this.add(btn, BorderLayout.NORTH);

            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }

        public void addServerStarter(ItemListener serverStarter) {
            btn.addItemListener(serverStarter);
        }
    }
}
