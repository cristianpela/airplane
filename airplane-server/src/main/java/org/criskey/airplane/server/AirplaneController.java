package org.criskey.airplane.server;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.User;
import org.criskey.airplane.common.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

import static org.criskey.airplane.common.NetContract.*;


/**
 * Created by criskey on 6/9/2016.
 */

@RestController
public class AirplaneController {

    @Autowired
    private AirplaneService service;

//    @MessageMapping(MSG_JOIN)
//    @SendTo(SUB_TOPIC_JOINED)
//    public UserInfo join(Principal principal, SimpMessageHeaderAccessor header) {
//        return service.join(principal.getName(), header.getSessionId());
//    }

    @SubscribeMapping(SUB_LOBBY)
    public List<UserInfo> lobby(Principal principal, SimpMessageHeaderAccessor header) throws Exception {
     //   service.join(principal.getName(), header.getSessionId());
        return service.lobby(principal.getName());
    }


    @MessageMapping(MSG_LEFT)
    @SendTo(SUB_TOPIC_LEFT)
    public User leave(@DestinationVariable String name) {
        return service.leave(name);
    }


    @MessageMapping(PRIVATE_MSG_INVITE)
    public void invite(@DestinationVariable String name, SimpMessageHeaderAccessor header) {
        service.invite(name, header.getSessionId());
    }


    @MessageMapping(PRIVATE_MSG_INVITE_STATUS)
    public void invitationStatus(@DestinationVariable String name, boolean isAccepted, SimpMessageHeaderAccessor header) {
        service.invitationStatus(name, isAccepted, header.getSessionId());
    }


    @MessageMapping(PRIVATE_MSG_GAME_CONFIG)
    public void gameConfig(GameNetworkProtocol protocol, SimpMessageHeaderAccessor header) throws Exception {
        service.gameConfig(protocol, header.getSessionId());
    }


    @MessageMapping(PRIVATE_MSG_GAME_TARGET)
    public void target(GameNetworkProtocol protocol, SimpMessageHeaderAccessor header) throws Exception {
        service.target(protocol, header.getSessionId());
    }


    @MessageMapping(PRIVATE_MSG_GAME_LEAVE)
    public void leaveGameSession(GameNetworkProtocol protocol, SimpMessageHeaderAccessor header) {
        service.leaveGameSession(protocol, header.getSessionId());
    }

    @RequestMapping(REQ_LOGOUT)
    public void logout(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            service.leave(principal.getName());
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

}
