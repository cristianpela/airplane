package org.criskey.airplane.server;

import org.criskey.airplane.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by criskey on 7/10/2016.
 */
@Component
public class UserLeftWatcher {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private Lobby lobby;

    @Autowired
    private GameManager gameManager;

   // @Scheduled(fixedDelay = 1000)
    public void notifyForLeaving() {
        List<User> leftUsers = lobby.getLeftUsers();
        for (User user : leftUsers) {
            template.convertAndSend(NetContract.SUB_TOPIC_LEFT, user.getInfo());
            if (user.isPlaying) {
                User opponent = user.playingWith;
                opponent.setNotPlaying();
                user.setNotPlaying();

                template.convertAndSend(NetContract.SUB_TOPIC_CHANGE, opponent.getInfo());

                GameSession session = gameManager.getSession(opponent.username, user.username);
                WebSocketUtils.sendToSessionId(template, opponent.id, NetContract.SUB_QUEUE_GAME,
                        GameNetworkProtocol.createEmpty(session.getSessionKey(), user.id, GameNetworkProtocol.PHASE_SESSION_LOST));
                gameManager.removeSession(session.getSessionKey());
            }
        }
        leftUsers.clear();
    }

}
