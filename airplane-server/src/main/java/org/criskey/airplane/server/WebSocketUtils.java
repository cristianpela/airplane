package org.criskey.airplane.server;

import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;

/**
 * Created by criskey on 26/9/2016.
 */
public final class WebSocketUtils {

    private WebSocketUtils(){}

    public static void sendToSessionId(SimpMessagingTemplate template, String sessionId, String destination, Object payload){
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor
                .create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);

        template.convertAndSendToUser(sessionId, destination, payload,
                headerAccessor.getMessageHeaders());
    }

}
