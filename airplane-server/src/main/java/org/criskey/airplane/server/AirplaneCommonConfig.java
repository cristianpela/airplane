package org.criskey.airplane.server;

import com.google.gson.Gson;
import org.criskey.airplane.common.GameManager;
import org.criskey.airplane.common.Lobby;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.messaging.SessionConnectEvent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by criskey on 7/9/2016.
 */
@Configuration
public class AirplaneCommonConfig {

    @Bean
    public Lock getApplicationLock(){ return new ReentrantLock(true);}

    @Bean
    public Lobby getLobby() {
        return new Lobby();
    }

    @Bean
    public GameManager getGameManager() {
        return new GameManager();
    }

}
