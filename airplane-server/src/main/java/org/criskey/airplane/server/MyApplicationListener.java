package org.criskey.airplane.server;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.core.Ordered;

import javax.swing.*;

/**
 * Created by criskey on 3/11/2016.
 */
class MyApplicationListener implements ApplicationListener, Ordered {

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof ApplicationReadyEvent){
            AirplaneServerApplication.updateUI("Stop", true);
        }else if(event instanceof ContextClosedEvent){
            AirplaneServerApplication.updateUI("Start", true);
        }
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
