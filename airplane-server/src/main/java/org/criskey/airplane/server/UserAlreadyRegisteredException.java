package org.criskey.airplane.server;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by criskey on 25/1/2017.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class UserAlreadyRegisteredException extends Throwable {
    public UserAlreadyRegisteredException(String msg) {
        super(msg);
    }
}
