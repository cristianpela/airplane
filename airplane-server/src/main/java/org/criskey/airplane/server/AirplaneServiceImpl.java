package org.criskey.airplane.server;

import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

import static org.criskey.airplane.common.NetContract.*;

/**
 * Created by criskey on 1/12/2016.
 */

@Service
public class AirplaneServiceImpl implements AirplaneService {

    @Autowired
    private Lobby lobby;

    @Autowired
    private GameManager gameManager;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private Lock lock;

    @Override
    public UserInfo join(String name, String sessionId) {
        UserInfo userInfo;
        List<UserInfo> users;
        lock.lock();
        try {
            lobby.join(name, sessionId);
            userInfo = lobby.getUserByUsername(name).getInfo();
            Logger.out.print(this, userInfo.username + " has joined the lobby");
//            lobby = lobby.getUsers()
//                    .stream()
//                    .map(user -> user.getInfo())
//                    .filter(user -> user.id != sessionId)
//                    .collect(Collectors.toList());
        } finally {
            lock.unlock();
        }

        template.convertAndSend(SUB_TOPIC_JOINED, userInfo);
        return userInfo;
    }

    @Override
    public boolean isUsernameAvailable(String name) {
        boolean available;
        lock.lock();
        try {
            available = lobby.getUserByUsername(name) == null;
            Logger.out.print(this, "Requested username " + name + " is available");
        } finally {
            lock.unlock();
        }
        return available;
    }

    @Override
    public User leave(String name) {
        User user;
        lock.lock();
        try {
            user = lobby.getUserByUsername(name);
            lobby.leave(name);
            Logger.out.print(this, user.username + " has left the lobby");
            template.convertAndSend(NetContract.SUB_TOPIC_LEFT, user.getInfo());
            if (user.isPlaying) {
                User opponent = user.playingWith;
                opponent.setNotPlaying();
                user.setNotPlaying();

                template.convertAndSend(NetContract.SUB_TOPIC_CHANGE, opponent.getInfo());

                GameSession session = gameManager.getSession(opponent.username, user.username);
                WebSocketUtils.sendToSessionId(template, opponent.id, NetContract.SUB_QUEUE_GAME,
                        GameNetworkProtocol.createEmpty(session.getSessionKey(), user.id, GameNetworkProtocol.PHASE_SESSION_LOST));
                gameManager.removeSession(session.getSessionKey());
            }
        } finally {
            lock.unlock();
        }
        return user;
    }

    @Override
    public void invite(String name, String sessionId) {
        User[] participants = getParticipants(sessionId, name, null);
        User principal = participants[0];
        User toInvite = participants[1];

        if (toInvite == null) {
            Logger.out.print(this, "User " + name + " does not exist! ", ILogger.Level.ERROR);
            return;
        }

        if (!toInvite.equals(principal)) {
            boolean hasInvited = lobby.invite(principal.username, toInvite.username);
            if (hasInvited) {
                WebSocketUtils.sendToSessionId(template, toInvite.id, SUB_QUEUE_INVITES, principal.username);
                Logger.out.print(this, toInvite.username + " was invited by " + principal + ". Sending invitation");
            } else {
                WebSocketUtils.sendToSessionId(template, principal.id, SUB_QUEUE_ERROR, ERROR_INVITATION_ALREADY_SENT);
                Logger.out.print(this, toInvite.username + " was already invited by " + principal, ILogger.Level.ERROR);
            }
        } else {
            WebSocketUtils.sendToSessionId(template, principal.id, SUB_QUEUE_ERROR, ERROR_INVITATION_SELF);
            Logger.out.print(this, principal.username + " was trying to invite themselves", ILogger.Level.ERROR);
        }
    }

    @Override
    public void invitationStatus(String name, boolean isAccepted, String sessionId) {
        User[] participants = getParticipants(sessionId, name, null);
        User principal = participants[0];
        User accepted = participants[1];

        if (isAccepted) {
            //TODO: handle exception
            try {
                String key;
                lock.lock();
                try {
                    key = gameManager.createGame(principal.username, accepted.username);
                } finally {
                    lock.unlock();
                }

                WebSocketUtils.sendToSessionId(template,
                        principal.id,
                        SUB_QUEUE_GAME,
                        GameNetworkProtocol.createEmpty(key, accepted.id, GameNetworkProtocol.PHASE_PREPARE));
                WebSocketUtils.sendToSessionId(template,
                        accepted.id,
                        SUB_QUEUE_GAME,
                        GameNetworkProtocol.createEmpty(key, principal.id, GameNetworkProtocol.PHASE_PREPARE));

                Logger.out.print(this, principal.username + " accepted invitation by " + accepted.username);

                //notify all lobby that this game session has started (thus making the protagonists unavailable for invites)
                lock.lock();
                try {
                    accepted.setPlayingWith(principal);
                    principal.setPlayingWith(accepted);
                } finally {
                    lock.unlock();
                }

                //announce to all that these two players are in a game session
                template.convertAndSend(SUB_TOPIC_CHANGE, accepted.getInfo());
                template.convertAndSend(SUB_TOPIC_CHANGE, principal.getInfo());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        lock.lock();
        try {
            lobby.clearInvitation(accepted.username, principal.username);
        } finally {
            lock.unlock();
        }

    }

    @Override
    public void gameConfig(GameNetworkProtocol protocol, String sessionId) throws Exception {
        User principal;
        lock.lock();
        try {
            principal = lobby.getUserById(sessionId);
        } finally {
            lock.unlock();
        }

        String gameKey = protocol.getGameKey();
        String otherPlayerId = protocol.getOtherPlayerId();

        boolean gameReadyToStart;
        lock.lock();
        try {
            gameReadyToStart = gameManager.startGame(gameKey,
                    principal.username,
                    PlaneConfiguration.unpack(protocol.getData()));
        } finally {
            lock.unlock();
        }

        WebSocketUtils.sendToSessionId(template,
                principal.id,
                SUB_QUEUE_GAME,
                new GameNetworkProtocol(gameKey, otherPlayerId, GameNetworkProtocol.PHASE_PRINCIPAL_READY, protocol.getData()));

        Logger.out.print(this, "GameSession: " + gameKey + ": Principal " + principal + " is ready. Configuration : " + PlaneConfiguration.unpack(protocol.getData()));

        if (gameReadyToStart) {
            WebSocketUtils.sendToSessionId(template,
                    otherPlayerId,
                    SUB_QUEUE_GAME,
                    new GameNetworkProtocol(gameKey, principal.id, GameNetworkProtocol.PHASE_START, GameNetworkProtocol.PHASE_TAKE_TURN));
            WebSocketUtils.sendToSessionId(template,
                    principal.id,
                    SUB_QUEUE_GAME,
                    new GameNetworkProtocol(gameKey, otherPlayerId, GameNetworkProtocol.PHASE_START, GameNetworkProtocol.PHASE_WAIT_TURN));
            Logger.out.print(this, "GameSession: " + gameKey + " has started");
        } else {
            WebSocketUtils.sendToSessionId(template,
                    otherPlayerId,
                    SUB_QUEUE_GAME,
                    GameNetworkProtocol.createEmpty(gameKey, principal.id, GameNetworkProtocol.PHASE_OTHER_READY));
        }

    }

    @Override
    public void target(GameNetworkProtocol protocol, String sessionId) throws Exception {

        Shot shot = Shot.unpack(protocol.getData());

        User[] participants = getParticipants(sessionId, null, protocol.getOtherPlayerId());
        User opponent = participants[1];
        User principal = participants[0];

        String gameKey = protocol.getGameKey();

        TargetProtocol tp;
        lock.lock();
        try {
            tp = gameManager.target(gameKey, opponent.username, shot.position);
        } finally {
            lock.unlock();
        }


        //  Logger.out.print(this, "GameSession: " + gameKey + ": " + principal.username + "  shooting " + opponent.username + " at " + shot + ". Result: " + tp);

        //send feedback to principal about the shot (if they missed or not)
        WebSocketUtils.sendToSessionId(template,
                principal.id,
                SUB_QUEUE_GAME,
                new GameNetworkProtocol(gameKey,
                        opponent.id,
                        GameNetworkProtocol.PHASE_WAIT_TURN,
                        Shot.pack(new Shot(shot.position, tp))));

        //if tp is a winner (principal won the game) then announce opponent that they've lost the game
        TargetProtocol oppTp = (tp == TargetProtocol.WINNER) ? TargetProtocol.LOSER : tp;

        //announce opponent about principal's shot
        WebSocketUtils.sendToSessionId(template,
                opponent.id,
                SUB_QUEUE_GAME,
                new GameNetworkProtocol(gameKey,
                        principal.id,
                        GameNetworkProtocol.PHASE_TAKE_TURN,
                        Shot.pack(new Shot(shot.position, oppTp))));

        // if there is a loser/winner in this game then we need to end the game session
        if (tp == TargetProtocol.WINNER) {
            lock.lock();
            try {
                gameManager.removeSession(gameKey);
            } finally {
                lock.unlock();
            }
            WebSocketUtils.sendToSessionId(template,
                    principal.id,
                    SUB_QUEUE_GAME,
                    new GameNetworkProtocol(gameKey, opponent.id, GameNetworkProtocol.PHASE_END, tp.ordinal()));
            WebSocketUtils.sendToSessionId(template,
                    opponent.id,
                    SUB_QUEUE_GAME,
                    new GameNetworkProtocol(gameKey, principal.id, GameNetworkProtocol.PHASE_END, oppTp.ordinal()));

            Logger.out.print(this, "GameSession: " + gameKey + ": has ended");
            //notify all lobby that this game session has ended
            lock.lock();
            try {
                opponent.setNotPlaying();
                principal.setNotPlaying();
            } finally {
                lock.unlock();
            }
            template.convertAndSend(SUB_TOPIC_CHANGE, opponent.getInfo());
            template.convertAndSend(SUB_TOPIC_CHANGE, principal.getInfo());
        }

    }

    @Override
    public void leaveGameSession(GameNetworkProtocol protocol, String sessionId) {
        GameSession session;
        User opponent, principal;
        lock.lock();
        try {
            opponent = lobby.getUserById(protocol.getOtherPlayerId());
            principal = lobby.getUserById(sessionId);
            session = gameManager.getSession(opponent.username, principal.username);
        } finally {
            lock.unlock();
        }
        WebSocketUtils.sendToSessionId(template, opponent.id, NetContract.SUB_QUEUE_GAME,
                GameNetworkProtocol.createEmpty(session.getSessionKey(), principal.id, GameNetworkProtocol.PHASE_SESSION_LOST));
        WebSocketUtils.sendToSessionId(template, principal.id, NetContract.SUB_QUEUE_GAME,
                GameNetworkProtocol.createEmpty(session.getSessionKey(), opponent.id, GameNetworkProtocol.PHASE_SESSION_LOST));

        WebSocketUtils.sendToSessionId(template, opponent.id, NetContract.SUB_QUEUE_ERROR, LanguageSupport.ERR_GAME_SESSION_LOST);
        WebSocketUtils.sendToSessionId(template, principal.id, NetContract.SUB_QUEUE_ERROR, LanguageSupport.ERR_GAME_SESSION_LOST);

        lock.lock();
        try {
            opponent.setNotPlaying();
            principal.setNotPlaying();
        } finally {
            lock.unlock();
        }

        template.convertAndSend(NetContract.SUB_TOPIC_CHANGE, opponent.getInfo());
        template.convertAndSend(NetContract.SUB_TOPIC_CHANGE, principal.getInfo());

        lock.lock();
        try {
            gameManager.removeSession(session.getSessionKey());
        } finally {
            lock.unlock();
        }

    }

    @Override
    public List<UserInfo> lobby(String filterPrincipal) {
        List<UserInfo> usersInfo;
        lock.lock();
        try {
            usersInfo = lobby.getUsers()
                    .stream()
                    .filter(user -> user.username != filterPrincipal)
                    .map(user -> user.getInfo())
                    .collect(Collectors.toList());
            Logger.out.print(this, "Sending user lobby: " + usersInfo);
        } finally {
            lock.unlock();
        }
        return usersInfo;
    }

    private User[] getParticipants(String principalId, String otherName, String otherId) {
        User principal;
        User other;
        lock.lock();
        try {
            principal = lobby.getUserById(principalId);
            if (otherName != null)
                other = lobby.getUserByUsername(otherName);
            else if (otherId != null)
                other = lobby.getUserById(otherId);
            else
                throw new IllegalArgumentException("One of otherName or otherId must be initialized");
        } finally {
            lock.unlock();
        }
        return new User[]{principal, other};
    }
}
