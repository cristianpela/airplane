package org.criskey.airplane.server;

import org.apache.log4j.Logger;
import org.criskey.airplane.common.log.*;

/**
 * Created by criskey on 3/11/2016.
 */
public class SpringLogger implements ILogger {

    private Logger logger = Logger.getLogger(SpringLogger.class);

    @Override
    public void print(Object owner, String message) {
        logger.info(output(message, false, Level.INFO));
    }

    @Override
    public void print(Object owner, String message, boolean showThread) {
        logger.info(output(message, showThread, Level.INFO));
    }

    @Override
    public void print(Object owner, String message, boolean showThread, Level level) {
        logger.info(output(message, showThread, level));
    }

    @Override
    public void print(Object owner, String message, Level level) {
        logger.info(output(message, false, level));
    }

    @Override
    public void printd(Object owner, String message) {
        logger.debug(output(message, false, Level.DEBUG));
    }

    @Override
    public void printd(Object owner, String message, boolean showThread) {
        logger.debug(output(message, showThread, Level.DEBUG));
    }

    @Override
    public void printd(Object owner, String message, boolean showThread, Level level) {
        logger.debug(output(message, showThread, level));
    }

    @Override
    public void printd(Object owner, String message, Level level) {
        logger.debug(output(message, false, level));
    }

    @Override
    public void addPrintListener(PrintListener printListener) {
        throw new UnsupportedOperationException("Use a custom appender log4j to listen outputs");
    }

    @Override
    public void removePrintListener(PrintListener printListener) {
        throw new UnsupportedOperationException("Use a custom appender log4j to listen outputs");
    }

    private String output(String message, boolean showThread, Level level) {
        return "[" + level + "] " + message + " " + ((showThread) ? Thread.currentThread() : "");
    }

}
