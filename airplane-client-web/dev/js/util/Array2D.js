(function () {

    var _array = [];
    var _size, _length;
    var _cellSize, cellPadding, _resolution;
    var app = AirplaneClient;

    var public = Array2D.prototype;
    function Array2D(size, resolution, filler) {
        _resolution = resolution;
        _size = size;
        _resolution = resolution;
        _length = size * size;
        _cellSize = app.Position(resolution.row / _length, resolution.column / _length);
        if (filler)
            fill(filler);
    };

    public.fill = function (filler) {
        for (var i = 0; i < _length; i++) {
            array.push(filler);
        }
    }

    public.nativeArray = function(){
        return _array;
    }

    public.set = function (pos, value) {
        checkBounds(pos, new app.Position(_size, _size));
        _array[index(pos)] = value;
    }

    public.get = function(pos){
        checkBounds(pos, new app.Position(_size, _size));
        return array[index(pos)];
    }

    //  public final class CellRect {

    //     public final Position topLeft;

    //     public final Position topRight;

    //     public final Position bottomLeft;

    //     public final Position bottomRight;

    //     private CellRect(Position position) {
    //         topLeft = new Position(position.row * cellSize.row, position.column * cellSize.column);
    //         topRight = topLeft.move(Position.horizontal(cellSize.row));
    //         bottomRight = topRight.move(Position.vertical(cellSize.column));
    //         bottomLeft = bottomRight.move(Position.horizontal(-cellSize.row));
    //     }

    //     @Override
    //     public String toString() {
    //         return "CellRect{" +
    //                 "topLeft=" + topLeft +
    //                 ", topRight=" + topRight +
    //                 ", bottomLeft=" + bottomLeft +
    //                 ", bottomRight=" + bottomRight +
    //                 '}';
    //     }
    // }

    public.CellRect = function(position){
        return {
            topLeft : 0
        };
    }

    function index(pos) {
        return pos.row * size + pos.column;
    }

    function checkBounds(pos, size) {
        if (pos.column < 0 || pos.column >= size.column || pos.row < 0 || pos.row >= size.row)
            throw new Error("Out of bounds");
    }


   

    AirplaneClient.Array2D = Array2D;
})();
