var gulp = require('gulp');
var gutil = require('gulp-util');
var connect = require('gulp-connect');
var jade = require('gulp-jade');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');


var serverStaticRes = './../airplane-server/src/main/resources/static/';
var bootstrapPath = './bower_components/bootstrap/dist/'

gulp.task('jade', function () {
  return gulp.src('./dev/templates/*.jade')
    .pipe(jade({ pretty: true }))
    .pipe(gulp.dest(serverStaticRes))
    .pipe(connect.reload());
});

gulp.task('fonts', function () {
  return gulp.src('./bower_components/font-awesome/fonts/*')
    .pipe(gulp.dest(serverStaticRes + 'fonts'));
});

gulp.task('css', function () {
  return gulp.src([bootstrapPath + 'css/bootstrap.css', '/dev/css/*.css'])
   // .pipe(uglify().on('error', gutil.log))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(serverStaticRes + 'css'));
});

gulp.task('js', function () {
  return gulp.src([
    './bower_components/mustache.js/mustache.js',
    './bower_components/jquery/dist/jquery.js',
    './bower_components/sockjs/sockjs.js',
    bootstrapPath + 'js/bootstrap.js',
    './dev/js/**/*.js'
  ])
    .pipe(uglify().on('error', gutil.log))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(serverStaticRes + 'js'))
    .pipe(connect.reload());
});

gulp.task('images', function () {
  return gulp.src('./dev/img/*').pipe(imagemin())
    .pipe(gulp.dest(serverStaticRes + 'img'))
    .pipe(connect.reload());
});

gulp.task('connect', function () {
  connect.server({
    livereload: true,
    root: serverStaticRes
  });
});

gulp.task('watch', function () {
  gulp.watch(['./dev/templates/**/*.jade'], ['jade']);
  gulp.watch(['./dev/css/**/*.css'], ['css']);
  gulp.watch(['./dev/img/*'], ['images']);
  gulp.watch(['./dev/js/**/*.js'], ['js']);
});

gulp.task('default', ['fonts', 'images', 'css', 'js', 'jade', 'connect', 'watch']);

