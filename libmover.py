"""
Module
"""
import shutil

JAR = 'airplane-common.jar'
COMMON_PATH_JAR = './airplane-common/out/artifacts/airplane_common_jar/' + JAR

"""Copy 'airplane_common_jar' into desired destionation."""
def copy_jar(path):
    print 'copying ' + JAR + ' to ' + path
    shutil.copy2(COMMON_PATH_JAR, path)
    print 'done'

copy_jar('./airplane-android/app/libs')
copy_jar('./airplane-server/libs')
