package org.criskey.airplane.common;

/**
 * Created by criskey on 30/8/2016.
 */
public enum TargetProtocol {

    HIT(1), MISS(-1), EMPTY(0), PLANE(2), PLANE_HEAD(3), INVALID(-2), WINNER(3), LOSER(-3), ATTEMPT(7), SKIP(9);


    private int value;

    TargetProtocol(int value) {
        this.value = value;
    }

    public int get() {
        return value;
    }

    public static TargetProtocol fromValue(int value){
        TargetProtocol tp;
        switch (value){
            case 1: tp = HIT; break;
            case -1: tp = MISS; break;
            case 0: tp = EMPTY; break;
            case 2: tp = PLANE; break;
            case 3: tp = PLANE_HEAD; break;
            case -2: tp = INVALID; break;
            default: tp = INVALID;
        }
        return tp;
    }
}
