package org.criskey.airplane.common;

import javafx.geometry.Pos;
import org.criskey.airplane.common.util.Position;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by criskey on 30/8/2016.
 */
public class GameManager {

    private static GameManager instance;

    private ConcurrentMap<String, GameSession> gameSessions;


    public static synchronized GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    //use this only for testing or for DI frameworks like a Spring bean
    public GameManager() {
        gameSessions = new ConcurrentHashMap<String, GameSession>();
    }

    public String createGame(String userNameOne, String userNameTwo) throws Exception {
        GameSession gameSession = getSession(userNameOne, userNameTwo);
        String key = createSessionKey(userNameOne, userNameTwo);
        if (gameSession != null) {
            gameSession.reset();
        } else {
            gameSessions.putIfAbsent(key, new GameSession(key));
        }
        return key;
    }

    public boolean startGame(String key, String userName, PlaneConfiguration planeConfiguration) throws Exception {
        GameSession gameSession = getSession(key);
        if (gameSession == null)
            throw new Exception("You need to create a game first");

        if(gameSession.isReady()) // session is ready so we don't allow other configuration
            return false;

        Player player = gameSession.getPlayer(userName);

        if (player.configBoard(planeConfiguration)) {
            gameSession.addPlayer(player);
        } else {
            return false;
        }

        return gameSession.isReady();
    }

    public void reset(String key) {
        GameSession gameSession = getSession(key);
        if (gameSession != null)
            gameSession.reset();
    }

    public void removeSession(String key) {
        GameSession gameSession = getSession(key);
        if (gameSession != null) {
            gameSessions.remove(key);
        }
    }

    public TargetProtocol target(String sessionKey, String userName, Position position) throws Exception {
        GameSession gameSession = gameSessions.get(sessionKey);
        return gameSession != null ? gameSession.target(userName, position) : TargetProtocol.INVALID;
    }

    public GameSession getSession(String playerOne, String playerTwo) {
        String key = createSessionKey(playerOne, playerTwo);
        GameSession gameSession = gameSessions.get(key);
        if (gameSession == null) {
            gameSession = gameSessions.get(createSessionKey(playerTwo, playerOne));
        }
        return gameSession;
    }

    public GameSession getSession(String key) {
        return (key != null) ? gameSessions.get(key) : null;
    }

    private String createSessionKey(String playerOne, String playerTwo) {
        return playerOne + "-" + playerTwo;
    }

}
