package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 30/8/2016.
 */
public class GameSession {

    private Player playerOne, playerTwo, nextPlayerToMove;

    private String sessionKey;

    public GameSession(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public GameSession(String sessionKey, Player playerOne, Player playerTwo) {
        this(playerOne, playerTwo);
        this.sessionKey = sessionKey;
    }

    public GameSession(String key) {
        this.sessionKey = key;
    }

    public void addPlayer(Player player) {
        if (playerOne == null) {
            playerOne = player;
            if (playerTwo != null && playerOne.equals(playerTwo))
                playerOne = null;
        } else if (playerTwo == null) {
            playerTwo = player;
            if (playerOne != null && playerTwo.equals(playerOne))
                playerTwo = null;
        }
    }

    public TargetProtocol target(String targetUsername, Position position) throws Exception {
        Player targetPlayer = getPlayer(targetUsername);
        if (nextPlayerToMove != null && targetPlayer.equals(nextPlayerToMove)) {
            throw new Exception("Invalid turn. This is " + nextPlayerToMove + "s turn");
        }

        TargetProtocol targetProtocol = targetPlayer.target(position);
        if (targetProtocol == TargetProtocol.HIT) {
            targetPlayer.increaseScore();// count the hits
            if (targetPlayer.getScore() == GameSettings.VICTORY_CONDITION) {
                targetProtocol = TargetProtocol.WINNER;
            }
        }

        //if the add is valid switch else keep the same player to target
        nextPlayerToMove = (targetProtocol != TargetProtocol.INVALID
                || targetProtocol == TargetProtocol.SKIP) ? targetPlayer : otherPlayer(targetPlayer);
        return targetProtocol;
    }

    public Player otherPlayer(Player player) {
        return playerOne.equals(player) ? playerTwo : playerTwo.equals(player) ? playerOne : null;
    }

    public Player getNextPlayerToMove() {
        return nextPlayerToMove;
    }

    public boolean hasEnded() {
        return playerOne.getScore() == GameSettings.VICTORY_CONDITION ||
                playerTwo.getScore() == GameSettings.VICTORY_CONDITION;
    }

    public boolean isReady() {
        return playerOne != null && playerTwo != null && playerOne.isReady() && playerTwo.isReady();
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public Player getPlayer(String username) {
        if (playerOne != null && playerOne.getUsername().equals(username)) {
            return playerOne;
        } else if (playerTwo != null && playerTwo.getUsername().equals(username)) {
            return playerTwo;
        }
        return new Player(username);
    }

    public void reset() {
        playerOne.reset();
        playerTwo.reset();
        nextPlayerToMove = null;
    }

}
