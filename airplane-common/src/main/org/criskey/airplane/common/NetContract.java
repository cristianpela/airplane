package org.criskey.airplane.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by criskey on 26/9/2016.
 */
public final class NetContract {

    private static final Pattern PATH_ARG_PATTERN = Pattern.compile("\\{(.*?)\\}");

    public static final String LOCAL_HOST = "localhost:8080";

    public static final String HTTP_ROOT = createHttpRoot(LOCAL_HOST);

    public static final String WS_ROOT = createWSRoot(LOCAL_HOST, true);

    public static final String ERROR_INVITATION_ALREADY_SENT = "Invitation already sent or you have been already invited!";

    public static final String ERROR_INVITATION_SELF = "You can't invite yourself";

    private NetContract() {
    }


    public interface WebSocketBrokers {
        String TOPIC = "/topic";
        String QUEUE = "/queue";
        String PREFIX_APP = "/app";
        String PREFIX_USER_DEST = "/user";
        String ENDPOINT = "/airplane-ws";
    }

    public static final String REQ_LOGOUT = WebSocketBrokers.PREFIX_USER_DEST + "/logout";

    //subscriptions mappings
    public static final String SUB_TOPIC_JOINED = "/topic/joined";

    public static final String SUB_TOPIC_LEFT = "/topic/left";

    public static final String SUB_TOPIC_CHANGE = "/topic/changed";

    public static final String SUB_QUEUE_INVITES = "/queue/invites";

    public static final String SUB_QUEUE_ERROR = "/queue/error";

    public static final String SUB_QUEUE_GAME = "/queue/game";

    public static final String SUB_LOBBY = "/lobby";

    //message mappings
    public static final String MSG_JOIN = "/join";

    public static final String MSG_LEFT = "/left/{name}";

    public static final String MSG_CHANGED = "/changed/{name}";

    public static final String PRIVATE_MSG_INVITE = "/invite/{name}";

    public static final String PRIVATE_MSG_INVITE_STATUS = "/invite/status/{name}";

    public static final String PRIVATE_MSG_GAME_CONFIG = "/game/config";

    public static final String PRIVATE_MSG_GAME_TARGET = "/game/target";

    public static final String PRIVATE_MSG_GAME_LEAVE = "/game/leave";


    public static final String paramedRequest(String prefix, String msgQuery, String... params) {
        Matcher matcher = PATH_ARG_PATTERN.matcher(msgQuery);
        StringBuffer buffer = new StringBuffer();
        int pos = 0;
        while (matcher.find()) {
            matcher.appendReplacement(buffer, params[pos]);
            if (pos < params.length) {
                pos++;
            }
        }
        matcher.appendTail(buffer);
        String result = buffer.toString();
        result = (!result.startsWith("/")) ? "/" + result : result;
        return prefix + result;
    }

    public static final String createWSRoot(String hostName, boolean hasPostFixSegment) {
        String segment = hasPostFixSegment ? "/websocket" : "";
        return "ws://" + hostName + WebSocketBrokers.ENDPOINT + segment;
    }


    public static final String createHttpRoot(String hostName) {
        return "http://" + hostName;
    }
}
