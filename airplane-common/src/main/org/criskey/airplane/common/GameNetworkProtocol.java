package org.criskey.airplane.common;

/**
 * Created by criskey on 27/9/2016.
 */
public class GameNetworkProtocol {

    public static final int PHASE_PREPARE = 0;

    public static final int PHASE_CONFIG = 1;

    public static final int PHASE_OTHER_READY = 2;

    public static final int PHASE_PRINCIPAL_READY = 9 ;

    public static final int PHASE_START = 3;

    public static final int PHASE_GAME = 4;

    public static final int PHASE_END = 5;

    public static final int PHASE_TAKE_TURN = 6;

    public static final int PHASE_WAIT_TURN = 7;

    public static final int PHASE_SESSION_LOST = 8;

    private int phase;

    private String gameKey;

    private String otherPlayerId;

    private int data;

    public GameNetworkProtocol() {
    }

    public static GameNetworkProtocol createEmpty(String gameKey, String otherPlayerId, int phase) {
        return new GameNetworkProtocol(gameKey, otherPlayerId, phase, 0);
    }

    public GameNetworkProtocol(String gameKey, String otherPlayerId, int phase, int data) {
        this.gameKey = gameKey;
        this.otherPlayerId = otherPlayerId;
        this.phase = phase;
        this.data = data;
    }

    public String getOtherPlayerId() {
        return otherPlayerId;
    }

    public void setOtherPlayerId(String otherPlayerId) {
        this.otherPlayerId = otherPlayerId;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getGameKey() {
        return gameKey;
    }

    public void setGameKey(String gameKey) {
        this.gameKey = gameKey;
    }

    @Override
    public String toString() {
        return "GameNetworkProtocol{" +
                "phase=" + phase +
                ", gameKey='" + gameKey + '\'' +
                ", data=" + data +
                '}';
    }
}
