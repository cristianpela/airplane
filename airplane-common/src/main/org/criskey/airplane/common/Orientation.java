package org.criskey.airplane.common;

/**
 * Created by criskey on 10/9/2016.
 */
public enum Orientation {
    NORTH, EST, SOUTH, WEST;

    public static int maxDirections() {
        return Orientation.values().length;
    }

    public static Orientation getOrientation(int ordinal) {
        int size = Orientation.maxDirections();
        return Orientation.values()[ordinal % size];
    }
}