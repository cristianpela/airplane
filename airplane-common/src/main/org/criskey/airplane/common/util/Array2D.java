package org.criskey.airplane.common.util;

import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by criskey on 31/8/2016.
 */
public class Array2D<T> implements Iterable<Position> {

    private final T[] array;

    private final int size;
    private final int length;


    @SuppressWarnings("unchecked")
    public Array2D(int size, T filler) {
        this.size = size;
        length = size * size;
        array = (T[]) new Object[length];
        if (filler != null) {
            fill(filler);
        }
    }

    public Array2D(int size) {
        this(size, null);
    }

    public void fill(T filler) {
        Arrays.fill(array, filler);
    }

    /**
     * fill a subArraySize * subArraySize region starting at the startPosition offset.
     *
     * @param startPosition the top-left starting offset
     * @param subArraySize  region dimension. For ex a subArraySize = 4 would give 16 length sub-array
     * @param values        Array of values to fill. The length must be subArraySize * subArraySize
     * @return true if position is set
     */
    public boolean set(Position startPosition, int subArraySize, T... values) {
        if (values.length != subArraySize * subArraySize) {
            Logger.out.print(this,
                    "Values length" + values.length + " should match subArraySize * subArraySize : " + subArraySize * subArraySize,
                    ILogger.Level.ERROR);
            return false;
        }
        Position start = startPosition.copy();
        int nextValue = 0;
        for (int i = 0; i < subArraySize; i++) {
            for (int j = 0; j < subArraySize; j++) {
                if (!set(start, values[nextValue++]))
                    return false;
                start = start.add(Position.horizontal(1));
            }
            start = start.add(Position.vertical(1)).add(Position.horizontal(-subArraySize));
        }
        return true;
    }

    public boolean set(Position pos, T value) {
        boolean ck = checkBounds(pos, size);
        if (ck)
            array[index(pos)] = value;
        return ck;
    }


    public T get(Position pos) {
        return checkBounds(pos, size) ? array[index(pos)] : null;
    }

    /**
     * @return a copy of the underneath 1d array with a stride of {@link #size}
     */
    T[] nativeArray() {
        return Arrays.copyOf(array, length);
    }

    public boolean rotate(int subArraySize, Position offset) {
        if (!checkBounds(offset, size))
            return false;
        int level = 0;
        int last = subArraySize - 1;
        int levels = subArraySize / 2;
        int xOffset = offset.column;
        int yOffset = offset.row;
        while (level < levels) {
            for (int i = level; i < last; i++) {
                swap(index(level + yOffset, i + xOffset), index(i + yOffset, last + xOffset));
                swap(index(level + yOffset, i + xOffset), index(last + yOffset, last - i + level + xOffset));
                swap(index(level + yOffset, i + xOffset), index(last - i + level + yOffset, level + xOffset));
            }
            level++;
            last--;
        }
        return true;
    }

    public boolean translate(Position subArrayOrigin, int subArraySize,
                             Position translationStart,
                             Position translationEnd, T filler) {
        //validity check if top left corner (origin) and bottom right corner after translation are on the grid
        Position possibleOrigin = subArrayOrigin.add(translationEnd.diff(translationStart));
        Position bottomRight = possibleOrigin.add(Position.unit(subArraySize - 1));
        if (!checkBounds(possibleOrigin, size) ||
                !checkBounds(bottomRight, size)) {
            Logger.out.printd(this, "Bound size" + subArraySize + " Bad translate start: " + possibleOrigin + " end: "
                    + bottomRight, ILogger.Level.ERROR);
            return false;
        }

        //possible origin is valid and now fill a temporary sub-array with the on board data
        Array2D<T> subArray2D = new Array2D<T>(subArraySize);
        Position subStart = subArrayOrigin.copy();
        for (int i = 0; i < subArraySize; i++) {
            for (int j = 0; j < subArraySize; j++) {
                subArray2D.array[subArray2D.index(i, j)] = get(subStart);
                subStart = subStart.add(Position.horizontal(1));
            }
            subStart = subStart.add(Position.vertical(1)).add(Position.horizontal(-subArraySize));
        }

        fill(filler);// reset the main array

        //refill the main array the temporary array values starting at the possibleOrigin offset (translated offset)
        for (int i = 0; i < subArraySize; i++) {
            for (int j = 0; j < subArraySize; j++) {
                set(possibleOrigin, subArray2D.array[subArray2D.index(i, j)]);
                possibleOrigin = possibleOrigin.add(Position.horizontal(1));
            }
            possibleOrigin = possibleOrigin.add(Position.vertical(1)).add(Position.horizontal(-subArraySize));
        }

        return true;
    }

    private boolean checkBounds(Position pos, int size) {
        return pos.column >= 0 && pos.column < size && pos.row >= 0 && pos.row < size;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Array2D<?> array2D = (Array2D<?>) o;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(array, array2D.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }

    private int index(Position pos) {
        return pos.row * size + pos.column;
    }

    private int index(int row, int column) {
        return row * size + column;
    }

    private void swap(int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    @Override
    public Iterator<Position> iterator() {
        return new Iterator<Position>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < array.length;
            }

            @Override
            public Position next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                int row = index / size;
                int column = index - row * size;
                index++;
                return new Position(row, column);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

}
