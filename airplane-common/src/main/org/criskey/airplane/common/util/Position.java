package org.criskey.airplane.common.util;


/**
 * Wraps a position on a battlefield board.<br>
 * row: represents y, height<br>
 * column: represents x, width<br>
 * Created by criskey on 30/8/2016.
 */
public final class Position {

    /**
     * Not a position
     */
    public static final Position NaP = new Position(254, 255);

    /**
     * represents y-axis, height
     */
    public final int row;

    /**
     * represents x-axis, width
     */
    public final int column;

    /**
     * Returns a position based on the chess position paradigm (algebraic notation) for example A1 should return [1,1]
     *
     * @param position
     * @return
     */
    public static Position fromAlgebraicNotation(String position) {
        position = position.toUpperCase();
        int column = position.charAt(0) - 17 - '0';
        int row = Integer.parseInt(position.substring(1)) - 1;
        return new Position(row, column);
    }

    public static String toAlgebraicNotation(Position position) {
        char column = (char) (position.column + 65);
        String row = (position.row + 1) + "";
        return column + row;
    }

    public static final Position unit(int unit) {
        return new Position(unit, unit);
    }

    public static final Position asDimension(int width, int height) {
        return new Position(height, width);
    }

    public static final Position asXY(int x, int y) {
        return new Position(y, x);
    }

    public static final Position horizontal(int column) {
        return new Position(0, column);
    }

    public static final Position vertical(int row) {
        return new Position(row, 0);
    }

    public static final int pack(Position position) {
        return (position.row << 8) + position.column;
    }

    public static Position unpack(int data) {
        int rowMask = 0xFF00, colMask = 0xFF;
        return new Position((data & rowMask) >> 8, (data & colMask));
    }

    /**
     * Wraps a position on a battlefield board.<br>
     * row: represents y, height<br>
     * column: represents x, width<br>
     */
    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }


    public Position add(Position offset) {
        return new Position(row + offset.row, column + offset.column);
    }

    public Position add(int offset) {
        return new Position(row + offset, column + offset);
    }

    public Position diff(Position offset) {
        return new Position(row - offset.row, column - offset.column);
    }

    public Position diff(int offset) {
        return new Position(row - offset, column - offset);
    }

    public Position negate() {
        return new Position(-this.row, -this.column);
    }

    public Position div(Position by) {
        return new Position(row / by.row, column / by.column);
    }

    public Position div(int by) {
        return new Position(row / by, column / by);
    }

    public Position mult(Position by) {
        return new Position(row * by.row, column * by.column);
    }

    public Position scale(float by) {
        return new Position((int) (row * by), (int) (column * by));
    }

    public Position copy() {
        return new Position(this.row, this.column);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (row != position.row) return false;
        return column == position.column;

    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }

    @Override
    public String toString() {
        return "Position{" +
                "row[y,h]=" + row +
                ", col[x,w]=" + column +
                '}';
    }
}
