package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 10/9/2016.
 */
public final class PlaneConfiguration {

    /**
     * Represents the top-left corner of the airplane bound area (area of 5 x 5)
     */
    public final Position origin;

    /**
     * Plane orientation
     */
    public final Orientation orientation;

    /**
     * Airplane typeId. See {@link AirplaneModels}
     */
    public final int typeId;

    public PlaneConfiguration(int typeId, Position origin, Orientation orientation) {
        this.origin = origin;
        this.orientation = orientation;
        this.typeId = typeId;
    }

    public static int pack(PlaneConfiguration configuration) {
        int typeId = configuration.typeId;
        Position position = configuration.origin;
        int orientation = configuration.orientation.ordinal();
        return (typeId << 24) + (position.row << 16) + (position.column << 8) + orientation;
    }

    public static PlaneConfiguration unpack(int data) {
        int typeMask = 0xFF000000, rowMask = 0xFF0000, colMask = 0xFF00, orientMask = 0xFF;
        int typeId = (data & typeMask) >> 24;
        Position position = new Position((data & rowMask) >> 16, (data & colMask) >> 8);
        Orientation orientation = Orientation.values()[data & orientMask];
        return new PlaneConfiguration(typeId, position, orientation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlaneConfiguration that = (PlaneConfiguration) o;

        if (typeId != that.typeId) return false;
        if (origin != null ? !origin.equals(that.origin) : that.origin != null) return false;
        return orientation == that.orientation;

    }

    @Override
    public int hashCode() {
        int result = origin != null ? origin.hashCode() : 0;
        result = 31 * result + (orientation != null ? orientation.hashCode() : 0);
        result = 31 * result + typeId;
        return result;
    }

    @Override
    public String toString() {
        return "PlaneConfiguration{" +
                "typeId=" + typeId +
                ", origin=" + origin +
                ", orientation=" + orientation +
                '}';
    }
}
