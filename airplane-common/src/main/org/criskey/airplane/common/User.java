package org.criskey.airplane.common;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by criskey on 1/9/2016.
 */
public class User {

    public final String username;

    public boolean isPlaying;

    public User playingWith;

    private Set<String> invitesUserName;

    public String id;

    public User(String username) {
        this(username, null, false);
    }

    public User(String username, String id) {
        this(username, id, false);
    }

    public User(String username, String id, boolean isPlaying) {
        invitesUserName = new HashSet<String>();
        this.username = username;
        this.id = (id == null) ? username : id;
        this.isPlaying = isPlaying;
    }

    public Set<String> getInvites() {
        return invitesUserName;
    }

    public UserInfo getInfo() {
        return new UserInfo(username, id, isPlaying);
    }

    public void setPlayingWith(User user){
        isPlaying = true;
        playingWith = user;
    }

    public void setNotPlaying(){
        isPlaying = false;
        playingWith = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username);

    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", isPlaying=" + isPlaying +
                '}';
    }
}
