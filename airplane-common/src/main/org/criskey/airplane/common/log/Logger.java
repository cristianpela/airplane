package org.criskey.airplane.common.log;

/**
 * Created by criskey on 19/10/2016.
 */
public class Logger implements ILogger {

    public static Logger out = new Logger(new SystemLogger());

    private ILogger logger;

    public static synchronized void change(ILogger newLogger) {
        out = new Logger(newLogger);
    }

    public Logger(ILogger newLogger) {
        this.logger = newLogger;
    }

    @Override
    public void print(Object messageHolder, String message) {
        logger.print(messageHolder, message);
    }

    @Override
    public void print(Object messageHolder, String message, boolean showExecThread) {
        logger.print(messageHolder, message, showExecThread);
    }

    @Override
    public void print(Object messageHolder, String message, boolean showExecThread, Level level) {
        logger.print(messageHolder, message, showExecThread, level);
    }

    @Override
    public void print(Object messageHolder, String message, Level level) {
        logger.print(messageHolder, message, level);
    }

    @Override
    public void printd(Object messageHolder, String message) {
        logger.printd(messageHolder, message);
    }

    @Override
    public void printd(Object messageHolder, String message, boolean showExecThread) {
        logger.printd(messageHolder, message, showExecThread);
    }

    @Override
    public void printd(Object messageHolder, String message, boolean showExecThread, Level level) {
        logger.printd(messageHolder, message, showExecThread, level);
    }

    @Override
    public void printd(Object messageHolder, String message, Level level) {
        logger.printd(messageHolder, message, level);
    }

    @Override
    public void addPrintListener(PrintListener printListener) {
        logger.addPrintListener(printListener);
    }

    @Override
    public void removePrintListener(PrintListener printListener) {
        logger.removePrintListener(printListener);
    }
}
