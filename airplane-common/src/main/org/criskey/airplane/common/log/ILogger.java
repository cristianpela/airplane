package org.criskey.airplane.common.log;

/**
 * Created by criskey on 19/10/2016.
 */
public interface ILogger {

    enum Level {
        INFO, ERROR, DEBUG;
    }

    void print(Object messageHolder, String message);

    void print(Object messageHolder, String message, boolean showExecThread);

    void print(Object messageHolder, String message, boolean showExecThread, Level level);

    void print(Object messageHolder, String message, Level level);

    void printd(Object messageHolder, String message);

    void printd(Object messageHolder, String message, boolean showExecThread);

    void printd(Object messageHolder, String message, boolean showExecThread, Level level);

    void printd(Object messageHolder, String message, Level level);

    void addPrintListener(PrintListener printListener);

    void removePrintListener(PrintListener printListener);
}
