package org.criskey.airplane.common.log;

import java.lang.management.ManagementFactory;

/**
 * Created by criskey on 19/10/2016.
 */
public abstract class DebugLogger extends AbstractLogger {


    @Override
    public void printd(Object messageHolder, String message) {
        if (isInDebugMode())
            print(messageHolder, message, Level.DEBUG);
    }

    @Override
    public void printd(Object messageHolder, String message, boolean showExecThread) {
        if (isInDebugMode())
            print(messageHolder, message, showExecThread, Level.DEBUG);
    }

    @Override
    public void printd(Object messageHolder, String message, boolean showExecThread, Level level) {
        if (isInDebugMode())
            print(messageHolder, message, showExecThread, overwriteLevel(level));
    }

    @Override
    public void printd(Object messageHolder, String message, Level level) {
        if (isInDebugMode())
            print(messageHolder, message, overwriteLevel(level));
    }

    protected abstract boolean isInDebugMode();

    private Level overwriteLevel(Level level){
        return (level != Level.ERROR) ? Level.DEBUG : Level.ERROR;
    }

}
