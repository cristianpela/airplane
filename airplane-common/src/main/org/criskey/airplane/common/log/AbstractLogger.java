package org.criskey.airplane.common.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by criskey on 27/10/2016.
 */
public abstract class AbstractLogger implements ILogger {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private List<PrintListener> listeners;

    public AbstractLogger() {
        listeners = new CopyOnWriteArrayList<PrintListener>();
    }

    @Override
    public void print(Object messageHolder, String message) {
        print(messageHolder, message, false, Level.INFO);
    }

    @Override
    public void print(Object messageHolder, String message, boolean showExecThread) {
        print(messageHolder, message, showExecThread, Level.INFO);
    }

    @Override
    public void print(Object messageHolder, String message, Level level) {
        print(messageHolder, message, false, level);
    }

    @Override
    public void addPrintListener(PrintListener printListener) {
        listeners.add(printListener);
    }

    @Override
    public void removePrintListener(PrintListener printListener) {
        listeners.remove(printListener);
    }

    protected void notifyListeners(String log, Level level) {
        for (PrintListener l : listeners) {
            l.onPrint(log, level);
        }
    }

    protected String getDateFormat() {
        return dateFormat.format(new Date());
    }

    protected String getOutputFormat(Object messageHolder, String message, boolean showExecThread) {
        return String.format("[%s %s]: %s%s\n",
                getDateFormat(),
                messageHolder.getClass().getSimpleName(),
                message,
                (showExecThread) ? ", " + Thread.currentThread() : "");
    }

}
