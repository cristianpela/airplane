package org.criskey.airplane.common.log;

/**
 * Created by criskey on 31/10/2016.
 */
public interface PrintListener {

    void onPrint(String log, ILogger.Level level);

}
