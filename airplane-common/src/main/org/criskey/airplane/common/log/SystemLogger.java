package org.criskey.airplane.common.log;

import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by criskey on 19/10/2016.
 */
public class SystemLogger extends DebugLogger {

    @Override
    public void print(Object messageHolder, String message, boolean showExecThread, Level level) {
        if (level == null) level = Level.INFO;
        PrintStream out = (level == Level.INFO || level == Level.DEBUG) ? System.out : System.err;
        String log = getOutputFormat(messageHolder, message, showExecThread);
        out.printf(log);
        notifyListeners(log, level);
    }

    @Override
    protected boolean isInDebugMode() {
        return ManagementFactory.getRuntimeMXBean().
                getInputArguments().toString().indexOf("jdwp") >= 0;
    }
}
