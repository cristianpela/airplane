package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 28/9/2016.
 */
public final class Shot {

    public final TargetProtocol targetProtocol;

    public final Position position;

    public Shot(Position position, TargetProtocol targetProtocol) {
        this.targetProtocol = targetProtocol;
        this.position = position;
    }

    public static int pack(Shot shot) {
        Position position = shot.position;
        int tp = shot.targetProtocol.ordinal();
        return (position.row << 16) + (position.column << 8) + tp;
    }

    public static Shot unpack(int data) {
        int rowMask = 0xFF0000, colMask = 0xFF00, tpMask = 0xFF;
        Position position = new Position((data & rowMask) >> 16, (data & colMask) >> 8);
        TargetProtocol tp = TargetProtocol.values()[data & tpMask];
        return new Shot(position, tp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shot shot = (Shot) o;

        if (targetProtocol != shot.targetProtocol) return false;
        return position != null ? position.equals(shot.position) : shot.position == null;

    }

    @Override
    public int hashCode() {
        int result = targetProtocol != null ? targetProtocol.hashCode() : 0;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%1s  -  %2$s", Position.toAlgebraicNotation(position), this.targetProtocol);
    }
}
