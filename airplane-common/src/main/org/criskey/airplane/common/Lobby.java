package org.criskey.airplane.common;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by criskey on 1/9/2016.
 */
public class Lobby {

    private List<User> users, leftUsers;

    public Lobby() {
        users = new ArrayList<User>();
        leftUsers = new ArrayList<User>();
    }

    public  boolean join(String username, String id) {
        User newUser = getUserByUsername(username);
        if (newUser == null) {
            users.add(new User(username, id));
            return true;
        }
        return false;
    }

    public  boolean leave(String username) {
        for (int i = users.size() - 1; i >= 0; i--) {
            User user = users.get(i);
            if (user.username.equals(username)) {
                users.remove(i);
                leftUsers.add(user);
                return true;
            }
        }
        return false;
    }

    public  boolean invite(String fromUsername, String destUsername) {
        User destUser = getUserByUsername(destUsername);
        User fromUser = getUserByUsername(fromUsername);
        if (fromUser != null && destUser != null
                && !destUser.getInvites().contains(fromUsername)
                && !fromUser.getInvites().contains(destUsername)) {
            destUser.getInvites().add(fromUsername);
            return true;
        }
        return false;
    }

    public  boolean clearInvitation(String fromUsername, String destUsername) {
        User destUser = getUserByUsername(destUsername);
        User fromUser = getUserByUsername(fromUsername);
        if (fromUser != null && destUser != null) {
            destUser.getInvites().remove(fromUsername);
            return true;
        }
        return false;
    }

    public  List<User> getUsers() {
        return users;
    }

    public User getUserById(String id) {
        for (User user : users) {
            if (user.id.equals(id)) {
                return user;
            }
        }
        return null;
    }

    public  User getUserByUsername(String username) {
        for (User user : users) {
            if (user.username.equals(username)) {
                return user;
            }
        }
        return null;
    }

    public List<User> getLeftUsers() {
        return leftUsers;
    }
}
