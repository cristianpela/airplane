package org.criskey.airplane.common;


import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 30/8/2016.
 */
public class Player {

    protected Array2D<TargetProtocol> board;

    protected Position origin;

    protected Orientation orientation = Orientation.NORTH;
    protected  int airplaneTypeId;

    private String username;

    private int score;

    private boolean ready;

    public Player(String username) {
        this(Position.unit(1), 0);
        this.username = username;
    }

    protected Player(Position resolution, int margin) {
        board = newBoard(resolution, margin);
    }


    protected Array2D<TargetProtocol> newBoard(Position resolution, int margin) {
        return new Array2D<TargetProtocol>(GameSettings.BOARD_SIZE, TargetProtocol.EMPTY);
    }

    public Player() {
        this("Player Unknown");
    }


    /**
     * incoming blows from another player
     *
     * @param position
     * @return
     */
    public TargetProtocol target(Position position) {

        if (position.equals(Position.NaP)) {
            return TargetProtocol.SKIP;
        }

        TargetProtocol value = TargetProtocol.INVALID;
        try {
            value = board.get(position);
        } catch (ArrayIndexOutOfBoundsException ex) {
            return value;
        }
        TargetProtocol result = TargetProtocol.INVALID;
        switch (value) {
            case PLANE:
                result = TargetProtocol.HIT;
                break;
            case PLANE_HEAD:
                result = TargetProtocol.WINNER;
                break;
            case HIT: // already used position
                result = TargetProtocol.INVALID;
                break;
            case MISS: // already used position
                result = TargetProtocol.INVALID;
                break;
            case EMPTY:
                result = TargetProtocol.MISS;
                break;
        }
        if (result != TargetProtocol.INVALID) {
            board.set(position, result);
        }
        return result;
    }

    public boolean configBoard(PlaneConfiguration planeConfiguration) {
        AirplaneModels models = AirplaneModels.get(null);
        airplaneTypeId = planeConfiguration.typeId;
        origin = planeConfiguration.origin;
        boolean done = board.set(origin, AirplaneModels.BOX_BOUND, models.select(planeConfiguration.typeId));
        if(done && orientation != planeConfiguration.orientation){
            int cycle = 0;
            while(cycle++ < planeConfiguration.orientation.ordinal()){
                board.rotate(AirplaneModels.BOX_BOUND, origin);
            }
            orientation = planeConfiguration.orientation;
        }
        ready = done;
        return done;
    }

    public void reset() {
        ready = false;
        origin = null;
        board.fill(TargetProtocol.EMPTY);
    }

    public boolean isReady() {
        return ready;
    }


    public TargetProtocol getValue(Position position) {
        return board.get(position);
    }

    public void increaseScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public String getUsername() {
        return username;
    }

    public PlaneConfiguration getConfiguration() {
        return (origin == null) ? null : new PlaneConfiguration(airplaneTypeId, origin, orientation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return username.equals(player.username);

    }


    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return username;
    }


}
