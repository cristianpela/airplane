package org.criskey.airplane.common;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by criskey on 8/11/2016.
 */
public final class AirplaneModels {

    public static final int BOX_BOUND = 5;

    private static AirplaneModels INSTANCE;

    private List<String[]> models = Arrays.asList(
            new String[]
                    {
                            "0", "0", "3", "0", "0",
                            "2", "2", "2", "2", "2",
                            "0", "0", "2", "0", "0",
                            "0", "0", "2", "0", "0",
                            "0", "2", "2", "2", "0"
                    },
            new String[]
                    {
                            "0", "0", "3", "0", "0",
                            "2", "0", "2", "0", "2",
                            "0", "2", "2", "2", "0",
                            "0", "2", "2", "2", "0",
                            "0", "0", "2", "0", "0"
                    }
    );

    private Random random;

    private AirplaneModels() {
        random = new Random();
    }

    public static AirplaneModels get(String modelsFile) {
        if (INSTANCE == null)
            INSTANCE = new AirplaneModels();
        if (modelsFile != null)
            try {
                INSTANCE.loadFromFile(modelsFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

        return INSTANCE;
    }

    private List<String[]> loadFromFile(String modelFile) throws IOException {
        throw new UnsupportedOperationException("To be implemented");
    }

    public TargetProtocol[] select(int id) {
        return toTargetProtocolModel(models.get(id));
    }

    public TargetProtocol[] randomSelect() {
        int id = getRandomTypeId();
        return select(id);
    }

    public int getRandomTypeId() {
        return random.nextInt(models.size());
    }

    private TargetProtocol[] toTargetProtocolModel(String... modelData) {
        TargetProtocol[] tps = new TargetProtocol[modelData.length];
        for (int i = 0; i < modelData.length; i++) {
            tps[i] = TargetProtocol.fromValue(Integer.parseInt(modelData[i]));
        }
        return tps;
    }

    /**
     * Set the seed for the random selection. Use this method only in tests.
     *
     * @param seed
     */
    public void setSelectionSeed(long seed) {
        random.setSeed(seed);
    }

}
