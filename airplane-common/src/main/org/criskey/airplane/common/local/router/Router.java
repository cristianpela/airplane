package org.criskey.airplane.common.local.router;

/**
 * Created by criskey on 20/9/2016.
 */
public interface Router<C> {

    Screen show(Screen toScreen);

    void hide(Screen screen);

    void addScreen(Screen screen, C screenContainer);
}
