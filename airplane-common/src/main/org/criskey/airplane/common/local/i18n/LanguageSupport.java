package org.criskey.airplane.common.local.i18n;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by criskey on 21/11/2016.
 */
public abstract class LanguageSupport {

    public static final String ERR_NO_CONNECTION = "err_no_connection";

    public static final String ERR_SERVER_DOWN = "err_server_down";

    public static final String ERR_CONNECTION_TIMEOUT = "err_connection_timeout";

    public static final String ERR_GAME_SESSION_LOST = "err_game_session_lost";

    public static final String ERR_APP_DISCONNECTED = "info_app_disconnected";


    public static final String TITLE = "title";

    public static final String TITLE_LOGGED = "title_logged";

    public static final String TITLE_DIALOG_INFO = "title_dialog_info";

    public static final String TITLE_DIALOG_ERROR = "title_dialog_error";

    public static final String TITLE_DIALOG_WARNING = "title_dialog_warning";


    public static final String MENU_AUTH = "menu_auth";

    public static final String MENU_AUTH_OUT = "menu_auth_out";

    public static final String MENU_LOBBY = "menu_lobby";

    public static final String MENU_INVITES = "menu_invites";

    public static final String MENU_GAME_SETUP = "menu_game_setup";

    public static final String MENU_GAME_SESSION = "menu_game_session";

    public static final String MENU_GAME_HIT_HISTORY = "menu_game_hit_history";

    public static final String MENU_SETTINGS = "menu_settings";


    public static final String SCREEN_JOIN_BTN_JOIN = "scr_join_btn_join";

    public static final String SCREEN_JOIN_LBL_WAIT = "scr_join_lbl_wait";

    public static final String SCREEN_JOIN_LBL_USERNAME = "scr_join_lbl_username";

    public static final String SCREEN_JOIN_LBL_PASSWORD = "scr_join_lbl_password";

    public static final String SCREEN_JOIN_EDIT_USERNAME = "scr_join_edit_username";

    public static final String SCREEN_JOIN_ERR_BLANK_NAME = "scr_join_err_blank_name";

    public static final String SCREEN_JOIN_ERR_INVALID_NAME = "scr_join_err_invalid_name";

    public static final String SCREEN_JOIN_ERR_ALREADY_NAME = "scr_join_err_already_name";


    public static final String SCREEN_GAME_SESSION_TAKE_TURN = "scr_game_session_take_turn";

    public static final String SCREEN_GAME_SESSION_WAIT_TURN = "scr_game_session_wait_turn";

    public static final String SCREEN_GAME_SESSION_WINNER = "scr_game_session_winner";

    public static final String SCREEN_GAME_SESSION_LOSER = "scr_game_session_loser";


    public static final String SCREEN_GAME_SETUP_ERR_POSITION = "scr_game_setup_err_position";

    public static final String SCREEN_GAME_SETUP_BTN_SUBMIT = "scr_game_setup_btn_submit";

    public static final String SCREEN_GAME_SETUP_LBL_ME_READY = "scr_game_setup_lbl_me_ready";

    public static final String SCREEN_GAME_SETUP_LBL_OTHER_READY = "scr_game_setup_lbl_other_ready";

    public static final String SCREEN_GAME_SETUP_ERR_CONFIGURATION = "scr_game_setup_err_configuration";


    public static final String SCREEN_GAME_BTN_LEAVE = "scr_game_leave";

    public static final String SCREEN_GAME_BOARD_ERR_POSITION_FILLED = "scr_game_board_position_filled";


    public static final String SCREEN_SETTINGS_LBL_SERVER = "scr_settings_lbl_server";

    public static final String SCREEN_SETTINGS_LBL_SERVER_DESC = "scr_settings_lbl_server_desc";

    public static final String SCREEN_SETTINGS_LBL_LANGUAGE = "scr_settings_lbl_language";


    public static final String SCREEN_LOBBY_BTN_INVITE = "scr_lobby_btn_invite";

    public static final String SCREEN_LOBBY_BTN_PLAYING = "scr_lobby_btn_playing";

    public static final String SCREEN_LOBBY_ERR_INVITE = "scr_lobby_err_invite";


    public static final String BTN_ACCEPT = "btn_accept";

    public static final String BTN_REJECT = "btn_reject";

    public static final String BTN_OK = "btn_ok";

    public static final String BTN_CANCEL = "btn_cancel";

    public static final String BTN_CLOSE = "btn_close";


    public static final String LANG_EN = "lang_en";

    public static final String LANG_RO = "lang_ro";


    //shot translations
    public static final String SHOT_HIT = "shot_hit";

    public static final String SHOT_MISS = "shot_miss";

    public static final String SHOT_WINNER = "shot_winner";

    public static final String SHOT_LOSER = "shot_loser";

    public static final String SHOT_SKIP = "shot_skip" ;


    protected Locale locale;

    private List<LanguageChangeListener> listeners;

    public LanguageSupport(Locale locale) {
        initLocale(locale);
        listeners = new LinkedList<LanguageChangeListener>();
    }

    public LanguageSupport() {
        this(Locale.US);
    }

    public void setLocale(Locale locale) {
        initLocale(locale);
        notifyListeners();
    }

    private void initLocale(Locale locale) {
        this.locale = locale;
        onLocaleChanged();
    }

    public void addLanguageChangeListener(LanguageChangeListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners() {
        for (LanguageChangeListener l : listeners) {
            l.onLanguageChanged();
        }
    }

    public abstract String i18n(String resourceKey, Object... params);

    public abstract void onLocaleChanged();


    private static Pattern LOCALE_PATTERN = Pattern.compile("^[a-z]{2}_[A-Z]{2}$");

    public static Locale getLocaleFromFormat(String format) {
        if (LOCALE_PATTERN.matcher(format).matches()) {
            String[] localeTokens = format.split("_");
            return new Locale(localeTokens[0], localeTokens[1]);
        } else {
            throw new IllegalArgumentException(format + " format incorrect. Must be like xx_XX" +
                    " where xx is two letter code for language and XX is two letter capitalized for country");
        }
    }

}
