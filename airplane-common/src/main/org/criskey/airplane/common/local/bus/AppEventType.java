package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 14/10/2016.
 */
public enum AppEventType {

    LOGGED,

    NETWORK_CONNECTION,

    GAME_SESSION,
    GAME_SESSION_SAVE,

    LOBBY,
    USER_INVITATION,

    /**
     * This a special event that must incapsulate other custom events.<br/>For example if we have a specific event only for android, we
     * should call it maybe like this: <code>new AppEvent(OTHER, new AndroidAppEvent(ANDROID_EVENT_TYPE, data));</code><br/>
     * This decision was taken because enums can't be inherited. If we were using the interface mixin workaround, there's is too much overhead for getting the <code>values()</code>
     * on {@link DataBus} logic.
     */
    OTHER,
    NONE,
    ERROR, ALL
}
