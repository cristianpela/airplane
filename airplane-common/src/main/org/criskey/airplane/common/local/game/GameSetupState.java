package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.bus.SimpleState;

/**
 * Created by criskey on 30/1/2017.
 */
public class GameSetupState extends SimpleState {

    public final boolean isConfigurationSubmitted;

    public final PlaneConfiguration planeConfiguration;

    public GameSetupState(boolean isConfigurationSubmitted, PlaneConfiguration planeConfiguration) {
        this.isConfigurationSubmitted = isConfigurationSubmitted;
        this.planeConfiguration = planeConfiguration;
    }

}
