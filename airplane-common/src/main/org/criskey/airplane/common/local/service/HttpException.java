package org.criskey.airplane.common.local.service;

import java.io.IOException;

/**
 * Created by criskey on 26/1/2017.
 */
public class HttpException extends IOException {
    public HttpException() {
        super();
    }

    public HttpException(String message) {
        super(message);
    }

    public HttpException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpException(Throwable cause) {
        super(cause);
    }
}
