package org.criskey.airplane.common.local.bus;

import org.criskey.airplane.common.local.service.websocket.ListenerWSNetwork;
import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.log.Logger;

import java.util.*;

/**
 * Created by criskey on 21/9/2016.
 */
public class DataBus {

    //TODO: threat ALL and NONE AppEventType's?

    private Map<AppEventType, Set<DataBusListener>> listeners;

    private OnUIRunCallback onUIRunCallback;

    /**
     * Listen about connection status and broadcast further to interested Databus listeners;
     * This must be hooked to a {@link org.criskey.airplane.common.local.service.IAirplaneStompService}
     */
  //  private ListenerWSNetwork listenerWSNetwork = new DataBusListenerWSNetwork(this);

    public DataBus(OnUIRunCallback onUIRunCallback) {
        this.onUIRunCallback = onUIRunCallback;
        listeners = new HashMap<AppEventType, Set<DataBusListener>>();
//        listeners.put(AppEventType.NETWORK_CONNECTION, new LinkedHashSet<DataBusListener>());
    }

    public void addListener(DataBusListener listener) {
        AppEventType[] eventTypesToListen = listener.getAppEventsToListen();
        if (eventTypesToListen == null) {
            for (AppEventType e : AppEventType.values()) {
                addListenerToGroup(listener, e);
            }
        } else {
            for (AppEventType e : eventTypesToListen) {
                addListenerToGroup(listener, e);
            }
        }
    }

    private void addListenerToGroup(DataBusListener listener, AppEventType e) {
        Set<DataBusListener> group = listeners.get(e);
        if (group == null)
            listeners.put(e, group = new LinkedHashSet<DataBusListener>());
        group.add(listener);
    }

    public void removeListener(DataBusListener listener) {
        AppEventType[] eventsToListen = listener.getAppEventsToListen();
        if (eventsToListen == null) {
            for (AppEventType e : listeners.keySet()) {
                removePresenterFromGroup(listener, e);
            }
        } else {
            for (AppEventType e : eventsToListen) {
                removePresenterFromGroup(listener, e);
            }
        }
        listeners.remove(listener);
    }

    private void removePresenterFromGroup(DataBusListener listener, AppEventType e) {
        Set<DataBusListener> group = listeners.get(e);
        group.remove(listener);
    }

    /**
     * Broadcast events to interested listeners. It's always running on ui thread
     *
     * @param event
     */
    public synchronized void broadcast(final AppEvent event) {
        //TODO: making a copy is the right way?
        final Set<DataBusListener> filteredListeners = listeners.get(event.type);
        if(filteredListeners== null) return;
        final Set<DataBusListener> set = new LinkedHashSet<DataBusListener>(filteredListeners);
        //normally in order to avoid ConcurrentModificationException, we traverse the set in reverse,
        // but by doing this, the event dispatching order is broken. So making a copy is necessary for now
        if (onUIRunCallback.isUIThread()) {
            notifyListeners(set, event);
        } else {
            onUIRunCallback.runOnUI(new OnUICodeInjector() {
                @Override
                public void inject() {
                    notifyListeners(set, event);
                }
            });
        }
    }

    private void notifyListeners(Set<DataBusListener> set, AppEvent event) {
        Logger.out.print(this, event.toString(), true);
        for (Iterator<DataBusListener> it = set.iterator(); it.hasNext(); ) {
            DataBusListener listener = it.next();
            listener.onEvent(event);
        }
    }


    public Map<AppEventType, Set<DataBusListener>> getListeners() {
        return Collections.unmodifiableMap(listeners);
    }

//   // public ListenerWSNetwork getListenerWSNetwork() {
//        return listenerWSNetwork;
//    }

    @Override
    public String toString() {
        return "DataBus{" +
                "listeners=" + listeners +
                '}';
    }

}
