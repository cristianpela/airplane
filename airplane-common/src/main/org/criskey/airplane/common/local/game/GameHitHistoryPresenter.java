package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.log.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 20/10/2016.
 */
public class GameHitHistoryPresenter extends GamePresenter {

    public static final String KEY_PLANE_CONFIGURATION = "KEY_PLANE_CONFIGURATION";
    public static final String KEY_LIST_HIT_HISTORY = "KEY_LIST_HIT_HISTORY";

    public GameHitHistoryPresenter(ArgsBatch batch) {
        super(batch);
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    private void restore() {
        GameState gameState = getGameState();
        if (gameState != null) {
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(KEY_PLANE_CONFIGURATION, gameState.gameSetupState.planeConfiguration);
            dataMap.put(KEY_LIST_HIT_HISTORY, gameState.gameSessionState.opponentAttempts);
            this.getView().restore(dataMap);
        }
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    @Override
    public void onEvent(AppEvent event) {
        GameState gameState = getGameState();
        GameSessionState gameSessionState = gameState.gameSessionState;
        int phase = gameState.phase;
        switch (phase) {
            case GameNetworkProtocol.PHASE_TAKE_TURN: {
                int lastShotIndex = gameSessionState.opponentAttempts.size() - 1;
                Shot opponentShot = gameSessionState.opponentAttempts.get(lastShotIndex);
                getView().incomingBlow(opponentShot);
                Logger.out.printd(this, "Opponent Shot: " + opponentShot);
                break;
            }
        }
    }


    private GameHitHistoryView getView() {
        return (GameHitHistoryView) mView;
    }

}
