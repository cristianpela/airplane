package org.criskey.airplane.common.local.viewpresenter;

import com.google.gson.Gson;
import org.criskey.airplane.common.local.bus.StateContainer;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.bus.DataBus;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.IAirplaneStompService;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 21/9/2016.
 */
public class PresenterManager {

    private static PresenterManager instance = null;

    private IAirplaneStompService service;
    private Router router;
    private OnUIRunCallback uiRunCallback;
    private Gson gson;
    private StateContainer stateContainer;
    private Storage storage;
    private LanguageSupport lang;

    //TODO: make it weak hash map?
    private static Map<Class<? extends Presentable>, Presentable> presenters =
            new HashMap<Class<? extends Presentable>, Presentable>();

    public static synchronized PresenterManager createInstance(IAirplaneStompService service,
                                                               Router router,
                                                               OnUIRunCallback uiRunCallback,
                                                               Gson gson,
                                                               StateContainer stateContainer,
                                                               Storage storage,
                                                               LanguageSupport lang) {
        if (instance == null) {
            instance = new PresenterManager();
            instance.router = router;
            instance.service = service;
            instance.uiRunCallback = uiRunCallback;
            instance.gson = gson;
            instance.stateContainer = stateContainer;
            instance.storage = storage;
            instance.lang = lang;
        }
        return instance;
    }

    //only use this constructor for testing purposes, otherwise this should be a singleton
    public PresenterManager() {
    }

    public static synchronized <P extends Presentable> P plugReflect(Class<P> presenterClass) {
        assert (instance != null);

        Presentable cachedPresenter = presenters.get(presenterClass);
        if (cachedPresenter != null)
            return (P) cachedPresenter;

        try {
            Constructor<P> constructor = presenterClass.getConstructor(Presentable.ArgsBatch.class);
            Presentable.ArgsBatch batch = new Presentable.ArgsBatch(
                    instance.service,
                    instance.stateContainer,
                    instance.router,
                    instance.gson,
                    instance.storage,
                    instance.lang,
                    instance.uiRunCallback
            );
            P presenter = constructor.newInstance(batch);
            if (batch.lang != null)    batch.lang.addLanguageChangeListener(presenter);
            if (batch.stateContainer != null) batch.stateContainer.addDataBusListener(presenter);
            presenters.put(presenterClass, presenter);
            return presenter;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static synchronized <P extends Presentable> void unPlug(P presenter) {
        assert (instance != null);
        instance.stateContainer.removeDataBusListener(presenter);
    }

}
