package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.SimpleState;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.local.util.FluentMap;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 21/9/2016.
 */
public class JoinPresenter extends Presentable {

    public static class JoinState extends SimpleState {

        public static final JoinState EMPTY = new JoinPresenter.JoinState(null, null, true, false, false);

        public final String username;
        public final String password;
        public final boolean rememberMe;
        public final boolean loggedIn;
        public final boolean waiting;

        public JoinState(String username, String password, boolean rememberMe, boolean loggedIn, boolean waiting) {
            this.username = username;
            this.password = password;
            this.rememberMe = rememberMe;
            this.loggedIn = loggedIn;
            this.waiting = waiting;
        }

        @Override
        public boolean isRetained() {
            return true;
        }
    }

    public static final String KEY_BOOLEAN_WAITING = "KEY_BOOLEAN_WAITING";

    public static final String KEY_BOOLEAN_LOGGED_IN = "KEY_BOOLEAN_LOGGED_IN";

    public static final String KEY_STRING_USERNAME = "KEY_STRING_USERNAME";

    public static final String KEY_STRING_PASSWORD = "KEY_STRING_PASSWORD";

    public JoinPresenter(ArgsBatch batch) {
        super(batch);
        addAppEventTypeToListen(AppEventType.NETWORK_CONNECTION);
    }

    public void join(final String username, final String password) {
        JoinState joinState = getJoinState();
        if (joinState.loggedIn) return; // just an extra fail-safe
        if (username.length() == 0) {
            getView().showError(i18n(LanguageSupport.SCREEN_JOIN_ERR_BLANK_NAME), true);
        } else {
            getView().onWait();
            mService.connect(username, password, new AsyncCallback<String>() {
                @Override
                public void onReturn(String data, Error error) {
                    if (error != null) {
                        mStateContainer.announce(new AppEvent(AppEventType.ERROR, error.getMessage()));
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOGGED,
                                new JoinState(username, password, true, false, false)));
                    } else {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOGGED,
                                new JoinState(username, password, true, false, true)));
                    }
                }
            });
        }
    }

    //for androids
    public void discardView() {
        mView = null;
    }

    @Override
    public void onEvent(AppEvent event) {
        restore();
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    private void restore() {
        JoinState joinState = getJoinState();
        getView().restore(new FluentMap<String, Object>()
                .put(KEY_BOOLEAN_WAITING, joinState.waiting)
                .put(KEY_BOOLEAN_LOGGED_IN, joinState.loggedIn)
                .put(KEY_STRING_PASSWORD, joinState.password)
                .put(KEY_STRING_USERNAME, joinState.username)
                .get());
        if (!joinState.waiting)
            getView().onJoined();
    }

    public void disconnect() {
        mService.disconnect();
    }

    private JoinView getView() {
        return (JoinView) mView;
    }

    private JoinState getJoinState() {
        JoinState joinState = (JoinState) mStateContainer.getState().get(AppEventType.LOGGED);
        return joinState;
    }

}
