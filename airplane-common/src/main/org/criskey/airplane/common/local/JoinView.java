package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 21/9/2016.
 */
public interface JoinView extends IView {

    void onWait();

    void onJoined();
}
