package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 2/2/2017.
 */
public interface State {

    /**
     * Called right before {@link StateContainer#reset()}
     */
    void onClear();

    /**
     * Flag that tells {@link StateContainer} that this state should not be removed, when {@link StateContainer#reset()}
     * is called
     *
     * @return
     */
    boolean isRetained();

}
