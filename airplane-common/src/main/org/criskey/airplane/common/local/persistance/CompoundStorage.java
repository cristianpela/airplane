package org.criskey.airplane.common.local.persistance;

import org.criskey.airplane.common.local.util.AsyncCallback;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by criskey on 25/11/2016.
 */
public class CompoundStorage implements Storage {

    private List<Storage> storages;

    public CompoundStorage() {
        storages = new LinkedList<Storage>();
    }

    public CompoundStorage(Storage... storages){
        this();
        addStorage(storages);
    }

    @Override
    public <F> void save(Prefix prefix, String fieldKey, F field) {
        for (Storage storage : storages) {
            storage.save(prefix, fieldKey, field);
        }
    }

    @Override
    public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
        for (Storage storage : storages) {
            storage.load(prefix, fieldKey, asyncCallback);
        }
    }

    @Override
    public void wipe(Prefix prefix) {
        for (Storage storage : storages) {
            storage.wipe(prefix);
        }
    }

    @Override
    public void commit(Prefix prefix) {
        for (Storage storage : storages) {
            storage.commit(prefix);
        }
    }

    public void addStorage(Storage... storages) {
        for (Storage storage : storages) {
            this.storages.add(storage);
        }
    }

    public void addStorage(Prefix prefix, Storage storage){
        storages.add(new PrefixedStorageProxy(prefix, storage));
    }
}
