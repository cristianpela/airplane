package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 25/11/2016.
 */
public interface SettingsView extends IView {

    void onCurrentLanguageChanged(String currentLanguage);

    void onServerAddressChanged(String serverAddress);

}
