package org.criskey.airplane.common.local.service;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.util.Position;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.criskey.airplane.common.NetContract.*;

/**
 * Created by criskey on 19/9/2016.
 */
public class DummyAirplaneStompService implements IAirplaneStompService {

    private static final int MILLIS = 1;

    private ExecutorService executorService = Executors.newCachedThreadPool();

    private Map<String, ListenerSubscription> subscriptionMap = new HashMap<String, ListenerSubscription>();

    private List<String> names = new ArrayList<String>();

    public DummyAirplaneStompService() throws Exception {
        names.add("Cristi");
        names.add("John");
    }

    @Override
    public void changeHost(String host) {

    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public String addSubscription(String destination, ListenerSubscription listener) {
        subscriptionMap.put(destination, listener);
        return null;
    }

    @Override
    public void removeSubscription(String subscriptionId) {

    }

    @Override
    public void connect(String username, String password, AsyncCallback<String> asyncCallback) {

    }

    @Override
    public void joinLobby() {

    }

    public void disconnect() {

    }


    @Override
    public void sendInvitation(String username) {

    }

    @Override
    public void acceptInvite(String username, boolean b) {

    }

    @Override
    public void attemptHit(String gameKey, String otherPlayerId, Position position) {

    }

    @Override
    public String submitConfiguration(String gameKey, String otherPlayerId, PlaneConfiguration configuration) {
        return null;
    }

    @Override
    public void leaveSession(String gameKey, String otherPlayerId) {

    }

    public void startToGenerateData(){
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(7000);
                    names.add("Frank");
                    subscriptionMap.get(SUB_TOPIC_JOINED).onMessage(null, DummyAirplaneStompService.this.getUzr("Frank", false));
                    Thread.sleep(5000);
                    subscriptionMap.get(SUB_TOPIC_CHANGE).onMessage(null, DummyAirplaneStompService.this.getUzr("Frank", true));
                    Thread.sleep(5000);
                    subscriptionMap.get(SUB_TOPIC_LEFT).onMessage(null, DummyAirplaneStompService.this.getUzr("Frank", true));

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String uzersToJson() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < names.size(); i++) {
            String delim = (i == names.size() - 1) ? "" : ",";
            builder.append(getUzr(names.get(i), false));
            builder.append(delim);
        }
        builder.append("]");
        return builder.toString();
    }

    private String getUzr(String name, boolean playing) {
        return "{\"username\":\"" + name + "\",\"isPlaying\":" + playing + ",\"id\":\"/app/join/user2\",\"invites\":[]}";
    }


}
