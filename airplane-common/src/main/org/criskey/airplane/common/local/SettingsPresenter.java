package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by criskey on 25/11/2016.
 */
public class SettingsPresenter extends Presentable {

    public static final String KEY_STRING_ARR_AVAILABLE_LANGUAGES = "KEY_STRING_ARR_AVAILABLE_LANGUAGES";

    public static final String KEY_STRING_CURR_LANGUAGE = "KEY_STRING_CURR_LANGUAGE";

    public static final String KEY_STRING_CURR_LANGUAGE_IDX = "KEY_STRING_CURR_LANGUAGE_IDX";

    public static final String KEY_STRING_SERVER_ADDRESS = "KEY_STRING_SERVER_ADDRESS";

    private String[] langCodes;

    private String[] langNameKeys;

    private String currLangKey;

    private String serverAddress;

    public SettingsPresenter(ArgsBatch batch) {
        super(batch);
        mStorage.load(Prefix.FILES, Storage.PRF_AVAILABLE_LANGUAGE_CODES, new AsyncCallback<String[]>() {
            @Override
            public void onReturn(String[] data, Error error) {
                langCodes = error == null ? data : new String[]{"en_US"};
            }
        });
        mStorage.load(Prefix.FILES, Storage.PRF_AVAILABLE_LANGUAGE_NAME_KEYS, new AsyncCallback<String[]>() {
            @Override
            public void onReturn(String[] data, Error error) {
                langNameKeys = error == null ? data : new String[]{LanguageSupport.LANG_EN};
            }
        });
        mStorage.load(Prefix.PREF, Storage.PRF_CURR_LANGUAGE_NAME_KEY, new AsyncCallback<String>() {
            @Override
            public void onReturn(String data, Error error) {
                currLangKey = error == null ? data : LanguageSupport.LANG_EN;
                ;
            }
        });
        mStorage.load(Prefix.PREF, Storage.PRF_SERVER_ADDRESS, new AsyncCallback<String>() {
            @Override
            public void onReturn(String data, Error error) {
                serverAddress = error == null ? data : "";
            }
        });
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    @Override
    public void onEvent(AppEvent event) {
    }

    public void setCurrentLanguageCodeIndex(int index) {
        String key = langNameKeys[index];
        currLangKey = key;
        mLang.setLocale(new Locale(langCodes[index]));
        mStorage.save(Prefix.PREF, Storage.PRF_CURR_LANGUAGE_NAME_KEY, key);
        mStorage.save(Prefix.PREF, Storage.PRF_CURR_LANGUAGE_CODE, langCodes[index]);
        getView().onCurrentLanguageChanged(i18n(currLangKey));
    }

    public void setServerAddress(String newAddress) {
        newAddress = newAddress.trim();
        serverAddress = newAddress;
        mService.changeHost(serverAddress);
        getView().onServerAddressChanged(serverAddress);
        mStorage.save(Prefix.PREF, Storage.PRF_SERVER_ADDRESS, serverAddress);
    }

    private void restore() {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(KEY_STRING_CURR_LANGUAGE, i18n(currLangKey));
        dataMap.put(KEY_STRING_CURR_LANGUAGE_IDX, getCurrLangKeyIndex());
        dataMap.put(KEY_STRING_ARR_AVAILABLE_LANGUAGES, translatedLangNameKeys());
        dataMap.put(KEY_STRING_SERVER_ADDRESS, serverAddress);
        mView.restore(dataMap);
    }

    private String[] translatedLangNameKeys() {
        String[] translated = new String[langNameKeys.length];
        for (int i = 0; i < langNameKeys.length; i++) {
            translated[i] = i18n(langNameKeys[i]);
        }
        return translated;
    }

    private int getCurrLangKeyIndex() {
        for (int i = 0; i < langNameKeys.length; i++) {
            if (langNameKeys[i].equals(currLangKey))
                return i;
        }
        return -1;
    }

    public void commit(int index, String serverAddress) {
        setCurrentLanguageCodeIndex(index);
        setServerAddress(serverAddress);
        mRouter.hide(Screen.SETTINGS);
        mStorage.commit(Prefix.PREF);
    }

    private SettingsView getView() {
        return (SettingsView) mView;
    }

}
