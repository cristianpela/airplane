package org.criskey.airplane.common.local.platform;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 17/11/2016.
 */
public interface PlayerUIOps {

    PlaneConfiguration getConfiguration();

    void block(boolean block);

    boolean isBlocked();

    /**
     * While marking, the client can choose: </br>
     * <ul>
     *     <li>to repaint. Choose not to repaint while marking when traversing a loop (more efficient).
     *     Then after the loop is done call the native player-ui to repaint  </li>
     *     <li>to notify the {@link BoardChangeCallback}. During the board reconstruction, for example, one might not want to notify the callback</li>
     * </ul>
     * @param shot
     * @param repaint
     * @param notifyCallback
     */
    void mark(Shot shot, boolean repaint, boolean notifyCallback);

    void configureBoard(PlaneConfiguration configuration);

    void addBoardChangeCallback(BoardChangeCallback boardChangeCallback);

    void reset();

    interface BoardChangeCallback {
        void onBoardChanged(Position position);
        void onError(String message);
    }
}