package org.criskey.airplane.common.local.service;

import com.google.gson.Gson;
import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;

import java.util.Map;

/**
 * Created by criskey on 20/9/2016.
 */
public abstract class OnUIListenerSubscription<T> implements ListenerSubscription {

    private Gson gson;

    private Class<T> clazz;

    private OnUIRunCallback onUIRunCallback;

    protected OnUIListenerSubscription(Class<T> clazz, OnUIRunCallback onUIRunCallback) {
        this(clazz, onUIRunCallback, new Gson());
    }

    protected OnUIListenerSubscription(Class<T> clazz, OnUIRunCallback onUIRunCallback, Gson gson) {
        this.clazz = clazz;
        this.onUIRunCallback = onUIRunCallback;
        this.gson = gson;
    }

    @Override
    public void onMessage(final Map<String, String> headers, final String body) {
        onUIRunCallback.runOnUI(new OnUICodeInjector() {
            @Override
            public void inject() {
                if (!clazz.equals(String.class) && !clazz.equals(Void.class)) {
                    T object = gson.fromJson(body, clazz);
                    OnUIListenerSubscription.this.onUIMessage(headers, object);
                } else {
                    OnUIListenerSubscription.this.onUIMessage(headers, (T) body);
                }
            }
        });
    }


    public abstract void onUIMessage(Map<String, String> headers, T object);


}
