package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.List;

/**
 * Created by criskey on 20/10/2016.
 */
public interface GameHitHistoryView extends IView {

    /**
     * Records the incoming shot from the adversary
     * @param opponentShot
     */
    void incomingBlow(Shot opponentShot);

    void setConfiguration(PlaneConfiguration configuration);

}
