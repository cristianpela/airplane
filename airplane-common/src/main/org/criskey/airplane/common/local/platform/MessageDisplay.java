package org.criskey.airplane.common.local.platform;

/**
 * Created by criskey on 13/9/2016.
 */
public interface MessageDisplay {

    enum Kind {
        ERROR("#F08080"), INFO("#FFFFFF"), WARNING("#F0E68C");
        private String color;
        Kind(String color){
            this.color = color;
        }
        public String getColor(){
            return color;
        }
    }

    void show(Kind kind, String title, String message);

}
