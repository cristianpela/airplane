package org.criskey.airplane.common.local.router;

/**
 * Created by criskey on 31/1/2017.
 */
public abstract class AbstractRouter<C> implements Router<C> {

    private RouterMiddleware mMiddleware;

    private class InternalMiddleware implements RouterMiddleware {

        //start of the middleware chain
        //just show the redirect screen, without going into the middleware chain
        //and close requested screens
        @Override
        public void onPropagateUp(Screen redirectScreen, Screen... screensToClose) {
            show(redirectScreen);
            for (Screen screen : screensToClose) {
                hide(screen);
            }
        }

        @Override
        public void setPreviousInChain(RouterMiddleware routerMiddleware) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setNextInChain(RouterMiddleware routerMiddleware) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Screen next(Screen to) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Constructor for testing purposes. Should not be used in production
     *
     * @param internalMiddleware
     * @param middleware
     */
    protected AbstractRouter(RouterMiddleware internalMiddleware, RouterMiddleware middleware) {
        init(internalMiddleware, middleware);
    }

    public AbstractRouter(RouterMiddleware middleware) {
        init(new InternalMiddleware(), middleware);
    }

    private void init(RouterMiddleware internalMiddleware, RouterMiddleware middleware) {
        this.mMiddleware = middleware;
        if (middleware != null)
            middleware.setPreviousInChain(internalMiddleware);
    }

    /**
     * should call this method in the {@link Router#show(Screen)} concrete implementation
     *
     * @param to
     */
    protected final Screen showInternal(Screen to) {
        if (to.getScreenGroup().bypass || mMiddleware == null) {
            return to;
        } else if (mMiddleware != null) {
            return mMiddleware.next(to);
        }
        throw new IllegalStateException("Can't touch this");
    }
}
