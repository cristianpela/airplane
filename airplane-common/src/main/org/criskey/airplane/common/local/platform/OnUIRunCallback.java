package org.criskey.airplane.common.local.platform;

/**
 * Created by criskey on 20/9/2016.
 */
public interface OnUIRunCallback {
    void runOnUI(OnUICodeInjector injector);
    boolean isUIThread();
}
