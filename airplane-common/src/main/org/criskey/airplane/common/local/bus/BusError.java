package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 1/2/2017.
 */
public class BusError {

    enum Kind{

    }

    public final Kind kind;

    public final String message;

    public BusError(Kind kind, String message) {
        this.kind = kind;
        this.message = message;
    }
}
