package org.criskey.airplane.common.local.bus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 30/1/2017.
 */
public abstract class CompositeReducer implements Reducer<Map<AppEventType, Reducer<?>>> {
    @Override
    public abstract Map<AppEventType, Reducer<?>> onReduce(AppEventType eventType, Map<AppEventType, ?> state);

    public static Builder chain() {
        return new Builder();
    }

    public static class Builder {

        Map<AppEventType, Reducer<?>> mReducers;

        public Builder() {
            mReducers = new HashMap<AppEventType, Reducer<?>>();
        }

        public Builder put(AppEventType eventType, Reducer<?> reducer) {
            mReducers.put(eventType, reducer);
            return this;
        }

        public CompositeReducer build() {
            return new CompositeReducer() {
                @Override
                public Map<AppEventType, Reducer<?>> onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                    return mReducers;
                }
            };
        }
    }
}
