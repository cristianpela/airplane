package org.criskey.airplane.common.local.platform;

/**
 * Created by criskey on 20/9/2016.
 */
public interface OnUICodeInjector {
    void inject();
}
