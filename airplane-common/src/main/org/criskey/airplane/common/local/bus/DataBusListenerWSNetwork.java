package org.criskey.airplane.common.local.bus;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.local.JoinPresenter.JoinState;
import org.criskey.airplane.common.local.game.GameState;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.service.IAirplaneStompService.Status;
import org.criskey.airplane.common.local.service.websocket.ListenerWSNetwork;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;

/**
 * Created by criskey on 27/1/2017.
 */
public class DataBusListenerWSNetwork implements ListenerWSNetwork, DataBusListener {

    private StateContainer mStateContainer;

    public DataBusListenerWSNetwork(StateContainer stateContainer) {
        mStateContainer = stateContainer;
        mStateContainer.addDataBusListener(this);
    }

    @Override
    public void onState(final int state) {
        Status status = Status.fromStompState(state);
        if (status == Status.ONGOING_CONNECTION)
            return;// don't bother if there'sn't any type of response
        if (status == Status.DISCONNECTED_FROM_OTHER) {
            Logger.out.print(this, "Server down", ILogger.Level.ERROR);
            mStateContainer.announce(new AppEvent(AppEventType.ERROR, LanguageSupport.ERR_SERVER_DOWN));
        } else if (status == Status.DISCONNECTED_FROM_APP) {
            Logger.out.print(this, "Disconnected", ILogger.Level.ERROR);
            mStateContainer.announce(new AppEvent(AppEventType.ERROR, LanguageSupport.ERR_APP_DISCONNECTED));
        }
        mStateContainer.announce(new AppEvent(AppEventType.NETWORK_CONNECTION, status));
    }

    @Override
    public void onEvent(AppEvent event) {
        Status networkStatus = (Status) event.data;
        if (networkStatus != Status.CONNECTED) {
            GameState gameState = (GameState) mStateContainer.getState().get(AppEventType.GAME_SESSION);
            if (gameState != null) {
                GameNetworkProtocol protocol = GameNetworkProtocol.createEmpty(
                        gameState.gameKey,
                        gameState.otherPlayerId,
                        GameNetworkProtocol.PHASE_SESSION_LOST
                );
                mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                        Reducers.gameSession(protocol)));
            }
            JoinState joinState = getJoinState();
            mStateContainer.dispatchState(new AppEvent(AppEventType.LOGGED,
                    Reducers.just(new JoinState(
                            joinState.username,
                            joinState.password,
                            joinState.rememberMe,
                            false,
                            false
                    ))));
            mStateContainer.reset();
        } else if (networkStatus == Status.CONNECTED) {
            JoinState joinState = getJoinState();
            mStateContainer.dispatchState(new AppEvent(AppEventType.LOGGED,
                    Reducers.just(new JoinState(
                            joinState.username,
                            joinState.password,
                            joinState.rememberMe,
                            true,
                            false)
                    )));
        }
    }

    private JoinState getJoinState() {
        return (JoinState) mStateContainer.getState().get(AppEventType.LOGGED);
    }

    @Override
    public AppEventType[] getAppEventsToListen() {
        return new AppEventType[]{AppEventType.NETWORK_CONNECTION};
    }
}
