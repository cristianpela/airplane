package org.criskey.airplane.common.local.platform;

import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;

/**
 * An interface between library and system graphics
 * Created by criskey on 13/9/2016.
 */
public interface Painter {

    public void drawRect(int colLeft, int rowTop, int colRight, int rowBottom);

    public void drawLine(int colFrom, int rowFrom, int colTo, int rowTo);

    public void setColor(String colorCode);

    public void drawString(String string, Position position);

}
