package org.criskey.airplane.common.local.router;

import static org.criskey.airplane.common.local.i18n.LanguageSupport.*;
import static org.criskey.airplane.common.local.router.ScreenGroup.*;

/**
 * Created by criskey on 1/2/2017.
 */
public enum Screen {

    MAIN(TITLE, GENERAL),
    JOIN(MENU_AUTH, GENERAL),
    SETTINGS(MENU_SETTINGS, GENERAL),

    LOBBY(MENU_LOBBY, LOBBY_GROUP),
    INVITES(MENU_INVITES, LOBBY_GROUP),

    GAME_SETUP(MENU_GAME_SETUP, GAME_GROUP),
    GAME_SESSION_(MENU_GAME_SESSION, GAME_GROUP),
    GAME_HIT_HISTORY(MENU_GAME_HIT_HISTORY, GAME_GROUP);

    private String id;
    private ScreenGroup screenGroup;

    Screen(String id, ScreenGroup screenGroup) {
        this.id = id;
        this.screenGroup = screenGroup;
    }

    public ScreenGroup getScreenGroup() {
        return screenGroup;
    }

    public String id() {
        return id;
    }
}
