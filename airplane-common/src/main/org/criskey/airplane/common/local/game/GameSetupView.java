package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 20/10/2016.
 */
public interface GameSetupView extends IView {
    void reset();
}
