package org.criskey.airplane.common.local;

import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.List;

/**
 * Created by criskey on 21/9/2016.
 */
public interface LobbyListView extends IView {

    void onUsersFetched(List<UserInfo> users);

    void onUserJoined(UserInfo user);

    void onUserLeft(UserInfo user);

    void onUserChanged(UserInfo user);

    void setEntriesEnabled(boolean enabled);

}
