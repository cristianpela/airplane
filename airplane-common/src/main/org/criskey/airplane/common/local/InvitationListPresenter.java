package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.Reducers;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.NetContract.ERROR_INVITATION_ALREADY_SENT;
import static org.criskey.airplane.common.NetContract.ERROR_INVITATION_SELF;
import static org.criskey.airplane.common.local.bus.AppEventType.USER_INVITATION;

/**
 * Created by criskey on 26/9/2016.
 */
public class InvitationListPresenter extends Presentable {

    public static final String KEY_LIST_INVITATIONS = "KEY_LIST_INVITATIONS";

    public static final String KEY_BOOLEAN_ENTRIES_DISABLED = "KEY_BOOLEAN_ENTRIES_DISABLED";


    public static class InvitationListState {

        public final boolean disabled;

        public final List<String> invitations;

        public InvitationListState(boolean disabled, List<String> invitations) {
            this.disabled = disabled;
            this.invitations = invitations;
        }
    }

    public InvitationListPresenter(ArgsBatch args) {
        super(args);
        addAppEventTypeToListen(USER_INVITATION);
    }

    @Override
    public void onEvent(AppEvent event) {
        restore();
//        AppEventType type = event.type;
//        switch (type) {
//            case GAME_SESSION: {
//                GameNetworkProtocol protocol = (GameNetworkProtocol) event.data;
//                switch (protocol.getPhase()) {
//                    case GameNetworkProtocol.PHASE_START:
//                    case GameNetworkProtocol.PHASE_SESSION_LOST:
//                    case GameNetworkProtocol.PHASE_END: {
//                        getView().setEntriesEnabled(enabled = true);
//                        break;
//                    }
//                }
//                break;
//            }
//            case USER_INVITATION: {
//                gotInvitation(event.data.toString());
//                break;
//            }
//            case NETWORK_CONNECTION:
//                int conn = (Integer) event.data;
//                if (conn == Stomp.DISCONNECTED_FROM_APP ||
//                        conn == Stomp.DISCONNECTED_FROM_OTHER) {
//                    reset();
//                }
//                break;
        //     }
    }


    private void gotInvitation(String invitation) {
        if (invitation.equals(ERROR_INVITATION_ALREADY_SENT) ||
                invitation.equals(ERROR_INVITATION_SELF)) {
            //TODO: let this be processed on lobby presenter?
            //getView().showError(invitation, true);
        } else {
            getView().onNewInvitation(invitation);
        }
    }

    public void acceptInvite(String username) {
        mService.acceptInvite(username, true);
        mStateContainer.dispatchState(new AppEvent(USER_INVITATION,
                Reducers.invitation(username, false)
        ));

    }

    public void rejectInvite(String username) {
        mService.acceptInvite(username, false);
        mStateContainer.dispatchState(new AppEvent(USER_INVITATION,
                Reducers.invitation(username, false)
        ));
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    private void restore() {
        InvitationListState state = (InvitationListState) mStateContainer.getState().get(USER_INVITATION);
        if (state != null) {
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(KEY_LIST_INVITATIONS, state.invitations);
            dataMap.put(KEY_BOOLEAN_ENTRIES_DISABLED, state.disabled);
            mView.restore(dataMap);
        }
    }

    @Override
    public void onDetach() {
        this.mView = null;
    }

    private InvitationListView getView() {
        return (InvitationListView) this.mView;
    }
}
