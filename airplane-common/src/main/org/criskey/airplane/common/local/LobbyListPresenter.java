package org.criskey.airplane.common.local;

import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.SimpleState;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.local.bus.AppEventType.LOBBY;

/**
 * Created by criskey on 21/9/2016.
 */
public class LobbyListPresenter extends Presentable {

    public static final String KEY_LIST_LOBBY = "KEY_LIST_LOBBY";

    public static final String KEY_BOOLEAN_ENTRIES_DISABLED = "KEY_BOOLEAN_ENTRIES_DISABLED";

    public static class LobbyState extends SimpleState {

        public static final LobbyState EMPTY = new LobbyState(false, new ArrayList<UserInfo>());

        public final boolean disabled;
        public final List<UserInfo> users;

        public LobbyState(boolean disabled, List<UserInfo> users) {
            this.disabled = disabled;
            this.users = users;
        }

        @Override
        public boolean isRetained() {
            return true;
        }
    }

    public LobbyListPresenter(ArgsBatch batch) {
        super(batch);
        addAppEventTypeToListen(LOBBY);
    }

    @Override
    public void onEvent(AppEvent event) {
        restore();
    }

    public void sendInvitation(UserInfo user) {
        mService.sendInvitation(user.username);
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    private void restore() {
        LobbyState lobbyState = getLobbyState();
        if (lobbyState != null) {
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(KEY_LIST_LOBBY, lobbyState.users);
            dataMap.put(KEY_BOOLEAN_ENTRIES_DISABLED, lobbyState.disabled);
            this.getView().restore(dataMap);
        }
    }

    private LobbyState getLobbyState() {
        return (LobbyState) mStateContainer.getState().get(LOBBY);
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    private LobbyListView getView() {
        return (LobbyListView) mView;
    }
}
