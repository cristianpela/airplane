package org.criskey.airplane.common.local.util;

/**
 * Created by criskey on 17/10/2016.
 */
public interface AsyncCallback<T> {
    void onReturn(T data, Error error);
}
