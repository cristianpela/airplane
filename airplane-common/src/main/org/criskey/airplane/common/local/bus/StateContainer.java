package org.criskey.airplane.common.local.bus;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by criskey on 24/1/2017.
 */
public class StateContainer {

    private Map<AppEventType, Object> mState;

    private DataBus mDataBus;

    private StateObserver mStateObserver;

    public StateContainer(DataBus dataBus, Map<AppEventType, State> initState, StateObserver stateObserver) {
        mState = new ConcurrentHashMap<AppEventType, Object>();
        if (initState != null)
            initState(initState);
        mDataBus = dataBus;
        mStateObserver = stateObserver;
    }

    public StateContainer(DataBus dataBus, StateObserver stateObserver) {
        this(dataBus, null, stateObserver);
    }

    public StateContainer(DataBus dataBus, Map<AppEventType, State> initState){
        this(dataBus, initState, null);
    }

    public StateContainer(DataBus dataBus) {
        this(dataBus, null, null);
    }

    public void addDataBusListener(DataBusListener listener) {
        mDataBus.addListener(listener);
    }

    public void removeDataBusListener(DataBusListener listener) {
        mDataBus.removeListener(listener);
    }

    public void dispatchState(AppEvent event) {

        Object newData = event.data;

        Map<AppEventType, ?> oldState = Collections.unmodifiableMap(mState);

        Set<AppEventType> compositeEvToBroadcast = null;
        if (newData == null) {
            mState.remove(event.type);
        } else if (newData instanceof CompositeReducer) {
            CompositeReducer compositeReducer = (CompositeReducer) newData;
            Map<AppEventType, Reducer<?>> reducers = compositeReducer.onReduce(event.type, oldState);
            compositeEvToBroadcast = reducers.keySet();
            for (AppEventType eventType : compositeEvToBroadcast) {
                Reducer<?> reducer = reducers.get(eventType);
                if (reducer instanceof CompositeReducer) // don't let going deep into the composition tree
                    throw new IllegalStateException("Only one level of composition is permitted");
                putDataFromReducer(eventType, reducer, oldState);
            }
        } else if (newData instanceof Reducer) {
            putDataFromReducer(event.type, (Reducer) newData, oldState);
        } else {
            mState.put(event.type, newData);
        }

        Map<AppEventType, Object> newState = Collections.unmodifiableMap(mState);
        if (mStateObserver != null) {
            mStateObserver.observe(oldState, newState);
        }

        if (compositeEvToBroadcast != null) {
            for (AppEventType eventType : compositeEvToBroadcast) {
                mDataBus.broadcast(new AppEvent(eventType, newState));
            }
        } else {
            mDataBus.broadcast(new AppEvent(event.type, newState));
        }
    }

    private Object putDataFromReducer(AppEventType eventType, Reducer reducer, Map<AppEventType, ?> oldState) {
        Object data = reducer.onReduce(eventType, oldState);
        mState.put(eventType, data);
        return data;
    }

    public void announce(AppEvent event) {
        mDataBus.broadcast(event);
    }

    public Map<AppEventType, Object> getState() {
        return Collections.unmodifiableMap(mState);
    }

    public void reset() {
        Set<AppEventType> appEventTypes = mState.keySet();
        for (AppEventType key : appEventTypes) {
            Object stateEntry = mState.get(key);
            if (stateEntry instanceof State) {
                State state = ((State) stateEntry);
                state.onClear();
                if (!state.isRetained())
                    mState.remove(key);
            } else {
                mState.remove(key);
            }
        }
        //notify all that the state was reset
        for (AppEventType key : appEventTypes) {
            announce(new AppEvent(key, mState));
        }
    }

    private void initState(Map<AppEventType, State> state) {
        for (AppEventType eventType : state.keySet()) {
            mState.put(eventType, state.get(eventType));
        }
    }
}
