package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.bus.SimpleState;

import java.util.List;

/**
 * Created by criskey on 30/1/2017.
 */
public class GameSessionState extends SimpleState {

    public final List<Shot> attempts;

    public final List<Shot> opponentAttempts;

    public final boolean hasTurn;

    public final boolean isWinner;

    public GameSessionState(List<Shot> attempts, List<Shot> otherPlayerAttempts, boolean hasTurn, boolean isWinner) {
        this.attempts = attempts;
        this.opponentAttempts = otherPlayerAttempts;
        this.hasTurn = hasTurn;
        this.isWinner = isWinner;
    }

}
