package org.criskey.airplane.common.local.platform;

import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.InteractivePlayer;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.util.Position;

public class PlayerUI implements PlayerUIOps {

    public enum Mode {
        SETUP, GAME, DISABLED
    }

    private final InteractivePlayer player;
    private int margin;

    private Position resolution;
    private Position cellSize;

    private RePainter repainter;
    private MessageDisplay messageDisplay;
    private PlayerUIOps.BoardChangeCallback boardChangeCb;

    private boolean isInTranslation;
    private boolean isCursorInPlane;
    private boolean isFirstTimeCreated = true;
    private boolean isFreeToMark = true;
    private boolean isInSetupMode;
    private boolean isInteractable;

    private final String defaultColor = "#000000";

    private Position currentTranslation;

    public PlayerUI(Position resolution, Mode mode, int margin) {
        this.player = new InteractivePlayer();
        setMode(mode);
        this.margin = margin;
        setResolution(resolution);
    }

    public PlayerUI(Position resolution, Mode mode) {
        this(resolution, mode, 0);
    }

    public void setMode(Mode mode) {
        switch (mode) {
            case SETUP: {
                isInSetupMode = isInteractable = true;
                break;
            }
            case GAME: {
                isInSetupMode = false;
                isInteractable = true;
                break;
            }
            default: {
                isInSetupMode = isInteractable = false;
            }
        }
        isFreeToMark = !isInSetupMode & isInteractable;
    }

    public Position getSize() {
        //board size + margin
        return resolution.add(margin);
    }

    // ############platform coupling######
    public void draw(Painter painter) {
        if (resolution == null)
            return;

        int halfMargin = margin / 2;
        Position halfCellSize = cellSize.div(2);

        //fill the occupied squares and draw headers
        int xH = 0, yH = 0; // header titles
        for (int row = 0; row < GameSettings.BOARD_SIZE; row++) {
            for (int col = 0; col < GameSettings.BOARD_SIZE; col++) {
                TargetProtocol tp = player.getValue(new Position(row, col));
                painter.setColor(getTargetColor(tp));
                if (tp != TargetProtocol.EMPTY) {
                    painter.drawRect(
                            col * cellSize.column + margin,
                            row * cellSize.row + margin,
                            (col + 1) * cellSize.column + margin,
                            (row + 1) * cellSize.row + margin
                    );
                }
                painter.setColor(defaultColor);
                if (margin > 0) {
                    if (row == 0)
                        painter.drawString("" + xH++,
                                new Position(halfMargin, col * cellSize.column + halfCellSize.column + margin));
                    if (col == 0)
                        painter.drawString("" + yH++,
                                new Position(row * cellSize.row + halfCellSize.row + margin, halfMargin));
                }
            }
        }


        //draw board grid
        int w = resolution.column;
        int h = resolution.row;
        int cw = w / GameSettings.BOARD_SIZE;
        int ch = h / GameSettings.BOARD_SIZE;
        for (int col = 1; col < GameSettings.BOARD_SIZE; col++) {
            painter.drawLine(col * cw + margin, margin, col * cw + margin, h + margin);
        }
        for (int row = 1; row < GameSettings.BOARD_SIZE; row++) {
            painter.drawLine(margin, row * ch + margin, w + margin, row * ch + margin);
        }

        //draw the board bounds
        painter.drawLine(margin, margin, resolution.column + margin, margin);
        painter.drawLine(resolution.column + margin, margin, resolution.column + margin, resolution.row + margin);
        painter.drawLine(margin, margin, margin, resolution.row + margin);
        painter.drawLine(margin, resolution.row + margin, resolution.column + margin, resolution.row + margin);
    }

    private String getTargetColor(TargetProtocol targetProtocol) {
        String c;
        switch (targetProtocol) {
            case PLANE:
                c = "#FDFF00"; //yellow
                break;
            case PLANE_HEAD:
                c = "#FFA500"; //orange
                break;
            case HIT:
                c = "#00FF11"; //green
                break;
            case MISS:
                c = "#FF0000"; // red
                break;
            case ATTEMPT:
                c = "#999966"; // light brown
                break;
            default:
                c = defaultColor; // black
        }
        return c;
    }

    public void onInteraction(InteractionEvent event) {

        InteractionEvent.Type type = event.type;
        Position index = getIndexOfResolution(event.position);

        switch (type) {
            case ROTATION: {
                if (isInSetupMode)
                    rotate();
                break;
            }
            case CHECK: {
                if (isInSetupMode) {
                    if (isFirstTimeCreated) {
                        setup(index);
                    } else {
                        isInTranslation = isCursorInPlane = false;
                    }
                } else {
                    if (isFreeToMark) { // only allow to click if the target is confirmed by server
                        if (player.getValue(index) != TargetProtocol.EMPTY) {
                            if (boardChangeCb != null)
                                boardChangeCb.onError(LanguageSupport.SCREEN_GAME_BOARD_ERR_POSITION_FILLED);
                        } else {
                            mark(new Shot(index, TargetProtocol.ATTEMPT), true, true);
                            isFreeToMark = false;
                        }
                    } else {
                        if (boardChangeCb != null)
                            boardChangeCb.onError(LanguageSupport.SCREEN_GAME_SESSION_WAIT_TURN);
                    }
                }
                break;
            }
            case TRANSLATE: {
                if (isInSetupMode) {
                    isInTranslation = isCursorInPlane;
                    if (isInTranslation) {
                        translate(index);
                    }
                }
                break;
            }
            case TRANSLATE_START: {
                if (isInSetupMode) {
                    isCursorInPlane = isInPlane(index);
                    if (isCursorInPlane) {
                        currentTranslation = index;
                    }
                }
                break;
            }
            case TRANSLATE_END: {
                if (isInSetupMode) {
                    isInTranslation = isCursorInPlane = false;
                    currentTranslation = null;
                }
                break;
            }
        }
    }

    private boolean isInPlane(Position index) {
        TargetProtocol tp = player.getValue(index);
        return tp == TargetProtocol.PLANE || tp == TargetProtocol.PLANE_HEAD;
    }

    public boolean isInPlaneResolution(Position resolutionPoint) {
        return isInPlane(getIndexOfResolution(resolutionPoint));
    }

    private Position getIndexOfResolution(Position position) {
        return position.diff(margin).div(cellSize);
    }

    public void onResize(Position newDimension) {
        setResolution(newDimension);
        repaint();
    }

    public void setResolution(Position newDimension) {
        resolution = newDimension.diff(margin);
        cellSize = resolution.div(GameSettings.BOARD_SIZE);
    }

    public void configureBoard(PlaneConfiguration planeConfiguration) {
        player.reset();
        isFirstTimeCreated = true;
        if (player.configBoard(planeConfiguration)) {
            repaint();
            isFirstTimeCreated = false;
        } else if (boardChangeCb != null)
            boardChangeCb.onError(LanguageSupport.SCREEN_GAME_SETUP_ERR_POSITION);
    }

    private void setup(Position index) {
        AirplaneModels models = AirplaneModels.get(null);
        configureBoard(new PlaneConfiguration(models.getRandomTypeId(), player.createOriginFrom(index), Orientation.NORTH));
        if (boardChangeCb != null)
            boardChangeCb.onBoardChanged(index);
    }


    public void rotate() {
        if (player.rotate()) {
            if (boardChangeCb != null)
                boardChangeCb.onBoardChanged(player.getConfiguration().origin);
            repaint();
        }
    }

    public void translate(Position translationEnd) {
        if (currentTranslation != null && player.translate(currentTranslation, translationEnd)) {
            if (boardChangeCb != null)
                boardChangeCb.onBoardChanged(translationEnd);
            repaint();
        }
        currentTranslation = translationEnd;
    }

    @Override
    public void mark(Shot shot, boolean repaint, boolean notifyCallback) {
        player.set(shot.position, shot.targetProtocol);
        if (boardChangeCb != null && notifyCallback)
            boardChangeCb.onBoardChanged(shot.position);
        if (repaint)
            repaint();
    }

    // ############platform coupling######
    public void registerRepainter(RePainter repainter) {
        this.repainter = repainter;
    }

    public void registerMessageDisplay(MessageDisplay messageDisplay) {
        this.messageDisplay = messageDisplay;
    }
    // ###################################

    private void repaint() {
        if (repainter != null) {
            repainter.paintAgain();
        }
    }
//
//    public void showMessage(String message) {
////        if (messageDisplay != null)
////            messageDisplay.show(message);
//    }


    @Override
    public void block(boolean blockMarking) {
        isFreeToMark = !blockMarking;
    }

    @Override
    public boolean isBlocked() {
        return !isFreeToMark;
    }

    public void clearSystemReferences() {
        repainter = null;
        messageDisplay = null;
    }

    @Override
    public void addBoardChangeCallback(PlayerUIOps.BoardChangeCallback cb) {
        this.boardChangeCb = cb;
    }

    @Override
    public PlaneConfiguration getConfiguration() {
        return player.getConfiguration();
    }

    public void reset() {
        isFirstTimeCreated = true;
        isFreeToMark = true;
        player.reset();
        repaint();
    }

}
