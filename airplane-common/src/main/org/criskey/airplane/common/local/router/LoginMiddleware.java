package org.criskey.airplane.common.local.router;

import org.criskey.airplane.common.local.JoinPresenter.JoinState;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.DataBusListener;
import org.criskey.airplane.common.local.bus.StateContainer;

import static org.criskey.airplane.common.local.router.Screen.JOIN;
import static org.criskey.airplane.common.local.router.Screen.LOBBY;

/**
 * Created by criskey on 31/1/2017.
 */
public class LoginMiddleware implements RouterMiddleware, DataBusListener {

    private RouterMiddleware mPrevMiddleware;

    private StateContainer mStateContainer;

    public LoginMiddleware(StateContainer stateContainer, RouterMiddleware prevMiddleware) {
        this.mPrevMiddleware = prevMiddleware;
        this.mStateContainer = stateContainer;
    }

    public LoginMiddleware(StateContainer stateContainer) {
        this(stateContainer, null);
    }

    @Override
    public void onEvent(AppEvent event) {
        JoinState joinState = getJoinState();
        if (joinState.loggedIn) {
            onPropagateUp(LOBBY, JOIN);
        } else {
            onPropagateUp(JOIN, ScreenGroup.getNonBypassScreens());
        }
    }

    @Override
    public AppEventType[] getAppEventsToListen() {
        return new AppEventType[]{AppEventType.LOGGED};
    }

    @Override
    public Screen next(Screen to) {
        //this is the last link in the chain... no forwarding
        JoinState joinState = getJoinState();
        if (!joinState.loggedIn) {
            return JOIN;
        } else {
            return to;
        }
    }

    private JoinState getJoinState() {
        return (JoinState) mStateContainer.getState().get(AppEventType.LOGGED);
    }

    @Override
    public void onPropagateUp(Screen redirectScreen, Screen... screensToClose) {
        if (mPrevMiddleware != null)
            mPrevMiddleware.onPropagateUp(redirectScreen, screensToClose);
    }

    @Override
    public void setPreviousInChain(RouterMiddleware routerMiddleware) {
        mPrevMiddleware = routerMiddleware;
    }

    @Override
    public void setNextInChain(RouterMiddleware routerMiddleware) {
        throw new UnsupportedOperationException();
    }
}
