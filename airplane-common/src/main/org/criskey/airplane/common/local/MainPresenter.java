package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.JoinPresenter.JoinState;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.criskey.airplane.common.log.Logger;

import java.util.HashMap;
import java.util.Map;

import static org.criskey.airplane.common.local.bus.AppEventType.ERROR;
import static org.criskey.airplane.common.local.bus.AppEventType.LOGGED;
import static org.criskey.airplane.common.local.i18n.LanguageSupport.MENU_AUTH;
import static org.criskey.airplane.common.local.i18n.LanguageSupport.MENU_AUTH_OUT;
import static org.criskey.airplane.common.local.router.Screen.JOIN;

/**
 * Created by criskey on 21/9/2016.
 */

public class MainPresenter extends Presentable {

    public static final String KEY_STRING_USERNAME = "KEY_STRING_USERNAME";
    public static final String KEY_STRING_LANG_AUTH_STATE = "KEY_STRING_LANG_AUTH_STATE";

    public MainPresenter(ArgsBatch batch) {
        super(batch);
        setAppEventsToListen(LOGGED, ERROR);
    }

    @Override
    public void onEvent(AppEvent event) {
        if (event.type == LOGGED)
            restore();
        else
            getView().showError(i18n(String.valueOf(event.data)), true);
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    private void restore() {
        JoinState joinState = getAuthState();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(KEY_STRING_USERNAME,
                !joinState.loggedIn ? "" : joinState.username);
        dataMap.put(KEY_STRING_LANG_AUTH_STATE,
                !joinState.loggedIn ? MENU_AUTH : MENU_AUTH_OUT);
        mView.restore(dataMap);
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    public void close() {
        Logger.out.printd(this, "Closing application!");
        mService.disconnect();
    }

    public void auth() {
        if (isLoggedIn()) {
            mService.disconnect();
        } else {
            showScreen(JOIN);
        }
    }

    private boolean isLoggedIn() {
        return getAuthState().loggedIn;
    }

    public void showScreen(Screen screen) {
        mRouter.show(screen);
    }

    public MainView getView() {
        return (MainView) mView;
    }

    private JoinState getAuthState() {
        return (JoinState) mStateContainer.getState().get(LOGGED);
    }

}
