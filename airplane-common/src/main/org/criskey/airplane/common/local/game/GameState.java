package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.local.bus.SimpleState;
import org.criskey.airplane.common.local.bus.State;

/**
 * Created by criskey on 30/1/2017.
 */
public class GameState extends SimpleState{

    public final int phase;

    public final String gameKey;

    public final String statusMessage;

    public final String otherPlayerId;

    public final GameSetupState gameSetupState;

    public final GameSessionState gameSessionState;

    public GameState(int phase, String gameKey, String otherPlayerId, String statusMessage, GameSetupState gameSetupState,
                     GameSessionState gameSessionState) {
        this.phase = phase;
        this.gameSetupState = gameSetupState;
        this.gameSessionState = gameSessionState;
        this.gameKey = gameKey;
        this.otherPlayerId = otherPlayerId;
        this.statusMessage = statusMessage;
    }

}
