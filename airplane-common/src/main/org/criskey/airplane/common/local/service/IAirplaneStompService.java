package org.criskey.airplane.common.local.service;

import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 22/9/2016.
 */
public interface IAirplaneStompService {

    //adapt the stomp codes
    enum Status {
        CONNECTED, //Connection completely established
        ONGOING_CONNECTION, //Connection process is ongoing
        DISCONNECTED_FROM_OTHER, //Error, no more internet connection, etc.
        DISCONNECTED_FROM_APP, //application explicitly ask for shut down the connection
        NO_CONNECTION;//no connection, server is down before connecting;

        public static Status fromStompState(int state) {
            return Status.values()[state - 1];
        }
    }

    void changeHost(String host);

    String getHost();

    String addSubscription(String destination, ListenerSubscription listener);

    public void removeSubscription(String subscriptionId);

    void connect(String username, String password, AsyncCallback<String> asyncCallback);

    void joinLobby();

    void disconnect();

    void sendInvitation(String username);

    void acceptInvite(String username, boolean b);

    void attemptHit(String gameKey, String otherPlayerId, Position position);

    String submitConfiguration(String gameKey, String otherPlayerId, PlaneConfiguration configuration);

    void leaveSession(String gameKey, String otherPlayerId);

}
