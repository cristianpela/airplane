package org.criskey.airplane.common.local.viewpresenter;

import java.util.Map;

/**
 * Created by criskey on 19/10/2016.
 */
public interface IView {

    /**
     * Any implementation must attach the presenter in this method.<br>
     * onResume name was chosen to be aligned with android activity/fragment lifecycle method name, thus making the code less verbose
     */
    public void onResume();

    /**
     * Implementations must detach the presenter in this method.<br>
     * onPause name was chosen to be aligned with android activity/fragment lifecycle method name, thus making the code less verbose
     */
    public void onPause();

    /**
     *
     * @param message
     * @param pop
     */
    public void showMessage(String message, boolean pop);

    /**
     *
     * @param error
     * @param pop
     */
    public void showError(String error, boolean pop);

    /**
     * Called by presenter when a mView is attached. Also this may be called when the language has been changed.</br>
     * This is means that language dependent fields like labels, buttons etc... should set their text here.
     * @param dataMap pass the logic data model from presenter. Attention: It may be NULL
     */
    void restore(Map<String, ?> dataMap);


}
