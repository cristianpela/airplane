package org.criskey.airplane.common.local.i18n;

/**
 * Created by criskey on 24/11/2016.
 */
public interface LanguageChangeListener {

    public void onLanguageChanged();

}
