package org.criskey.airplane.common.local.viewpresenter;

import com.google.gson.Gson;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.DataBusListener;
import org.criskey.airplane.common.local.bus.StateContainer;
import org.criskey.airplane.common.local.i18n.LanguageChangeListener;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.IAirplaneStompService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Just a flag interface for PresenterManager
 * Created by criskey on 21/9/2016.
 */
public abstract class Presentable implements DataBusListener, LanguageChangeListener {

    protected IAirplaneStompService mService;
    protected StateContainer mStateContainer;
    protected Router mRouter;
    protected Gson mGson;
    protected Storage mStorage;
    protected OnUIRunCallback uiRunCallback;
    protected LanguageSupport mLang;

    private List<AppEventType> mAppEventsToListen;

    private String id;

    protected IView mView;

    public Presentable() {
        this.id = UUID.randomUUID().toString();
    }

    public Presentable(IAirplaneStompService service,
                       StateContainer stateContainer,
                       Router router,
                       Gson gson,
                       Storage storage,
                       LanguageSupport lang,
                       OnUIRunCallback uiRunCallback) {
        this.id = UUID.randomUUID().toString();
        this.mService = service;
        this.mStateContainer = stateContainer;
        this.mRouter = router;
        this.mGson = gson;
        this.mStorage = storage;
        this.mLang = lang;
        this.uiRunCallback = uiRunCallback;
        this.mAppEventsToListen = new ArrayList<AppEventType>();
    }

    public Presentable(ArgsBatch batch) {
        this(batch.service,
                batch.stateContainer,
                batch.router,
                batch.gson,
                batch.storage,
                batch.lang,
                batch.uiRunCallback);
    }

    @Override
    public void onLanguageChanged() {
        if (mView != null)
            mView.restore(null);
    }

    public abstract void onAttach(IView view);

    public abstract void onDetach();

    public void broadcast(AppEvent event) {
        mStateContainer.dispatchState(event);
    }

    public void forward(AppEvent event) {
        mStateContainer.announce(event);
    }

    public void setAppEventsToListen(AppEventType... eventType) {
        for (AppEventType et : eventType)
            mAppEventsToListen.add(et);
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Presentable that = (Presentable) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public AppEventType[] getAppEventsToListen() {
        return (mAppEventsToListen.isEmpty()) ? null : mAppEventsToListen.toArray(new AppEventType[mAppEventsToListen.size()]);
    }

    @Override
    public String toString() {
        return "Presentable{" + this.getClass().getSimpleName() +
                " id='" + id + '\'' +
                '}';
    }

    public String i18n(String resourceKey, Object... params) {
        return mLang.i18n(resourceKey, params);
    }

    protected void addAppEventTypeToListen(AppEventType eventType) {
        mAppEventsToListen.add(eventType);
    }

    protected void addAppEventTypeToListen(AppEventType... eventType) {
        for (AppEventType et : eventType)
            mAppEventsToListen.add(et);
    }

    /**
     * convenient "struct" to hold all the presentables constructor parameters
     */
    public static final class ArgsBatch {
        final IAirplaneStompService service;
        final StateContainer stateContainer;
        final Router router;
        final Gson gson;
        final Storage storage;
        final LanguageSupport lang;
        final OnUIRunCallback uiRunCallback;

        public ArgsBatch(IAirplaneStompService service,
                         StateContainer stateContainer,
                         Router router,
                         Gson gson,
                         Storage storage,
                         LanguageSupport lang,
                         OnUIRunCallback uiRunCallback) {
            this.service = service;
            this.stateContainer = stateContainer;
            this.router = router;
            this.gson = gson;
            this.storage = storage;
            this.lang = lang;
            this.uiRunCallback = uiRunCallback;
        }
    }
}
