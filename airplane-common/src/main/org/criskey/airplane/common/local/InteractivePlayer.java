package org.criskey.airplane.common.local;

import com.sun.istack.internal.NotNull;
import org.criskey.airplane.common.*;
import org.criskey.airplane.common.log.Logger;
import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;

import java.util.Iterator;

/**
 * Created by criskey on 19/9/2016.
 */
public class InteractivePlayer extends Player implements Iterable<Position> {

    public InteractivePlayer() {
        board = new Array2D<TargetProtocol>(GameSettings.BOARD_SIZE, TargetProtocol.EMPTY);
    }

    public boolean rotate() {
        orientation = Orientation.getOrientation(orientation.ordinal() + 1);
        return board.rotate(AirplaneModels.BOX_BOUND, origin);
    }

    public boolean translate(Position translationStart, Position translationEnd) {
        boolean translated = false;
        if (!translationStart.equals(translationEnd)) {
            translated = board.translate(origin, AirplaneModels.BOX_BOUND, translationStart.copy(), translationEnd.copy(), TargetProtocol.EMPTY);
            if (translated) {
                origin = origin.add(translationEnd.diff(translationStart));
            }
        }
        return translated;
    }

    public void set(Position index, TargetProtocol tp) {
        board.set(index, tp);
    }

    public Position createOriginFrom(Position position) {
        return position.diff(AirplaneModels.BOX_BOUND / 2);
    }

    @Override
    public Iterator<Position> iterator() {
        return board.iterator();
    }

}
