package org.criskey.airplane.common.local.i18n;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 13/12/2016.
 */
public final class TranslatorUtil {

    private static TranslatorUtil INSTANCE;

    private LanguageSupport languageSupport;

    public static void init(LanguageSupport languageSupport){
        if(INSTANCE != null) new IllegalStateException("Instance already created");
        INSTANCE = new TranslatorUtil(languageSupport);
    }

    public static TranslatorUtil getInstance(){
        assert INSTANCE !=null;
        return INSTANCE;
    }


    private TranslatorUtil(LanguageSupport languageSupport){
        this.languageSupport = languageSupport;
    }

    /**
     * Translate a shot in the current language
     * @param shot
     * @return translation
     */
    public String translateShot(Shot shot){
        TargetProtocol tp = shot.targetProtocol;
        String key = null;
        switch (tp){
            case HIT:    key = LanguageSupport.SHOT_HIT; break;
            case MISS:   key = LanguageSupport.SHOT_MISS; break;
            case WINNER: key = LanguageSupport.SHOT_WINNER; break;
            case LOSER:  key = LanguageSupport.SHOT_LOSER; break;
            case SKIP:   key = LanguageSupport.SHOT_SKIP; break;
        }
        return languageSupport.i18n(key, Position.toAlgebraicNotation(shot.position));
    }

}
