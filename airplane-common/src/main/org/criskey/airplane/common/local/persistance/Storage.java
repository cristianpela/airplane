package org.criskey.airplane.common.local.persistance;

import org.criskey.airplane.common.local.util.AsyncCallback;

/**
 * Created by criskey on 13/10/2016.
 */
public interface Storage {

    String PRF_CURR_LANGUAGE_NAME_KEY = "prf_language_key";

    String PRF_CURR_LANGUAGE_CODE = "prf_language_code";

    String PRF_AVAILABLE_LANGUAGE_CODES = "prf_available_language_codes";

    String PRF_AVAILABLE_LANGUAGE_NAME_KEYS = "prf_available_language_name_keys";

    String PRF_SERVER_ADDRESS = "prf_server_address";

    <F> void save(Prefix prefix, String fieldKey, F field);

    <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback);

    void wipe(Prefix prefix);

    void commit(Prefix prefix);

}
