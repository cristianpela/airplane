package org.criskey.airplane.common.local.router;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by criskey on 1/2/2017.
 */
public enum ScreenGroup {

    GENERAL(true), LOBBY_GROUP(false), GAME_GROUP(false);

    public final boolean bypass;

    ScreenGroup(boolean bypass) {
        this.bypass = bypass;
    }

    public Screen[] getScreens() {
        List<Screen> screens = new LinkedList<Screen>();
        for (Screen screen : Screen.values()) {
            if (screen.getScreenGroup() == this) {
                screens.add(screen);
            }
        }
        return screens.toArray(new Screen[screens.size()]);
    }

    public static ScreenGroup[] getNonBypassGroups() {
        List<ScreenGroup> screenGroups = new ArrayList<ScreenGroup>();
        for (ScreenGroup screenGroup : ScreenGroup.values()) {
            if (!screenGroup.bypass)
                screenGroups.add(screenGroup);
        }
        return screenGroups.toArray(new ScreenGroup[screenGroups.size()]);
    }

    public static Screen[] getNonBypassScreens() {
        List<Screen> nonBypassScreens = new LinkedList<Screen>();
        ScreenGroup[] nonBypassGroups = getNonBypassGroups();
        for(ScreenGroup group: nonBypassGroups){
            for(Screen screen: group.getScreens()){
                nonBypassScreens.add(screen);
            }
        }
        return nonBypassScreens.toArray(new Screen[nonBypassScreens.size()]);
    }
}
