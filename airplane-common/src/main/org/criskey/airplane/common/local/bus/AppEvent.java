package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 14/10/2016.
 */
public class AppEvent {

    public final AppEventType type;
    public final Object data;

    public AppEvent(AppEventType type, Object data) {
        this.type = type;
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppEvent appEvent = (AppEvent) o;

        if (type != appEvent.type) return false;
        return data.equals(appEvent.data);

    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AppEvent{" +
                "type=" + type +
                ", data=" + data +
                '}';
    }
}
