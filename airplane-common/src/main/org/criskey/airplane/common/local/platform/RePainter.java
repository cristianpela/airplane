package org.criskey.airplane.common.local.platform;

/**
 * An interface between library and system graphics repainting mechanism
 * Created by criskey on 13/9/2016.
 */
public interface RePainter {

    public void paintAgain();

}
