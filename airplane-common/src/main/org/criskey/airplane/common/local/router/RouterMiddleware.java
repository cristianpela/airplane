package org.criskey.airplane.common.local.router;

/**
 * Created by criskey on 31/1/2017.
 */
public interface RouterMiddleware {

    Screen next(Screen to);

    void onPropagateUp(Screen redirectScreen, Screen... screensToClose);

    void setPreviousInChain(RouterMiddleware routerMiddleware);

    void setNextInChain(RouterMiddleware routerMiddleware);
}
