package org.criskey.airplane.common.local.router;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.DataBusListener;
import org.criskey.airplane.common.local.bus.StateContainer;
import org.criskey.airplane.common.local.game.GameState;

import static org.criskey.airplane.common.GameNetworkProtocol.*;
import static org.criskey.airplane.common.local.bus.AppEventType.GAME_SESSION;
import static org.criskey.airplane.common.local.router.Screen.*;

/**
 * Created by criskey on 31/1/2017.
 */
public class GameMiddleware implements RouterMiddleware, DataBusListener {

    private RouterMiddleware mPrevMiddleware;

    private RouterMiddleware mNextMiddleware;

    private StateContainer mStateContainer;

    public GameMiddleware(StateContainer stateContainer, RouterMiddleware prevMiddleware, RouterMiddleware nextMiddleware) {
        this.mPrevMiddleware = prevMiddleware;
        this.mNextMiddleware = nextMiddleware;
        this.mStateContainer = stateContainer;
    }

    public GameMiddleware(StateContainer stateContainer, RouterMiddleware nextMiddleware) {
        this(stateContainer, null, nextMiddleware);
    }

    public GameMiddleware(StateContainer stateContainer) {
        this(stateContainer, null, null);
    }

    @Override
    public void onEvent(AppEvent event) {
        GameState gameState = (GameState) mStateContainer.getState().get(GAME_SESSION);
        if (mPrevMiddleware != null) {
            if (gameState == null) {
                // the game session has lost or is over, then redirect to lobby and close any screen game
                mPrevMiddleware.onPropagateUp(LOBBY, ScreenGroup.GAME_GROUP.getScreens());
            } else {
                int phase = gameState.phase;
                switch (phase) {
                    case PHASE_PREPARE:
                        mPrevMiddleware.onPropagateUp(GAME_SETUP);
                        break;
                    case PHASE_START:
                        mPrevMiddleware.onPropagateUp(GAME_SESSION_, GAME_SETUP);
                        break;
                    case PHASE_END:
                    case PHASE_SESSION_LOST:
                        mPrevMiddleware.onPropagateUp(LOBBY, ScreenGroup.GAME_GROUP.getScreens());
                        break;
                }
            }
        }

    }

    @Override
    public AppEventType[] getAppEventsToListen() {
        return new AppEventType[]{GAME_SESSION};
    }

    @Override
    public Screen next(Screen to) {
        Screen _to = to;
        if (mNextMiddleware != null)
            _to = mNextMiddleware.next(to);
        //we're good to filter here
        if (_to == to) {
            boolean isGameScreen = (_to == Screen.GAME_SESSION_)
                    || (_to == GAME_SETUP)
                    || (_to == GAME_HIT_HISTORY);
            if (isGameScreen) {//make sure the requested screen is a game screen
                GameState gameState = (GameState) mStateContainer.getState().get(GAME_SESSION);
                if (gameState != null) {// we have a game in progress
                    // prevent to access any other game screens if we're are in setup mode
                    if (!gameState.gameSetupState.isConfigurationSubmitted) {
                        _to = GAME_SETUP;
                    } else if (_to == GAME_SETUP) { // prevent to show game setup when in game session
                        _to = GAME_SESSION_;
                    }
                } else {
                    _to = LOBBY; // there's no game, we send the user to lobby
                }
            }
        }
        return _to;
    }

    @Override
    public void onPropagateUp(Screen redirectScreen, Screen... screensToClose) {
        if (mPrevMiddleware != null)
            mPrevMiddleware.onPropagateUp(redirectScreen, screensToClose);
    }

    @Override
    public void setPreviousInChain(RouterMiddleware routerMiddleware) {
        mPrevMiddleware = routerMiddleware;
    }

    @Override
    public void setNextInChain(RouterMiddleware routerMiddleware) {
        mNextMiddleware = routerMiddleware;
    }
}
