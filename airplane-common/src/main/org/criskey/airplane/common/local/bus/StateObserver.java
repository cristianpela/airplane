package org.criskey.airplane.common.local.bus;

import java.awt.event.ActionEvent;
import java.util.Map;

/**
 * Created by criskey on 24/1/2017.
 */
public interface StateObserver {

    void observe(Map<AppEventType, ?> oldState, Map<AppEventType, ?> newState);

}
