package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 20/10/2016.
 */
public class GameSetupPresenter extends GamePresenter {

    public static final String KEY_PLANE_CONFIGURATION = "KEY_PLANE_CONFIGURATION";

    public static final String KEY_STRING_STATUS_MESSAGE = "KEY_STRING_STATUS_MESSAGE";

    public static final String KEY_BOOLEAN_IS_CONFIG_SUBMIT = "KEY_BOOLEAN_IS_CONFIG_SUBMIT";

    //    private boolean isConfigurationSubmitted;
//
    private PlaneConfiguration mPlaneConfiguration;
//
//    private String statusMessage;

    public GameSetupPresenter(ArgsBatch batch) {
        super(batch);
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    private void restore() {
        GameState gameState = getGameState();
        if (gameState != null) {
            GameSetupState gameSetupState = gameState.gameSetupState;
            String statusMessage = gameState.statusMessage;
            PlaneConfiguration planeConfiguration = gameSetupState.planeConfiguration;
            boolean isConfigurationSubmitted = gameSetupState.isConfigurationSubmitted;
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(KEY_PLANE_CONFIGURATION, planeConfiguration);
            dataMap.put(KEY_STRING_STATUS_MESSAGE, statusMessage == null ? "" : i18n(statusMessage));
            dataMap.put(KEY_BOOLEAN_IS_CONFIG_SUBMIT, isConfigurationSubmitted);
            this.getView().restore(dataMap);
        }
    }

    @Override
    public void onDetach() {
        saveConfiguration();
        mView = null;
    }


    @Override
    public void onEvent(AppEvent event) {
        GameState gameState = getGameState();
        String statusMessage = gameState.statusMessage;
        int phase = gameState.phase;
        switch (phase) {
            case GameNetworkProtocol.PHASE_PREPARE:{
                restore();
                break;
            }
            case GameNetworkProtocol.PHASE_OTHER_READY: {
                getView().showMessage(i18n(statusMessage), false);
                break;
            }
            case GameNetworkProtocol.PHASE_PRINCIPAL_READY: {
                getView().showMessage(i18n(statusMessage), false);
                break;
            }
        }
    }

    private void saveConfiguration() {
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION_SAVE,
                Reducers.gameSessionSetPlaneConfiguration(mPlaneConfiguration)));
    }

    public void setConfiguration(PlaneConfiguration configuration) {
        mPlaneConfiguration = configuration;
    }

    public String submitConfiguration() {
        GameState gameState = getGameState();
        PlaneConfiguration planeConfiguration = (mPlaneConfiguration == null)
                ? gameState.gameSetupState.planeConfiguration
                : mPlaneConfiguration;
        String gameKey = gameState.gameKey;
        String otherPlayerId = gameState.otherPlayerId;
        if (planeConfiguration == null) {
            getView().showError(i18n(LanguageSupport.SCREEN_GAME_SETUP_ERR_CONFIGURATION), true);
            return null;
        }
        saveConfiguration();
        return mService.submitConfiguration(gameKey, otherPlayerId, planeConfiguration);
    }

    private GameSetupView getView() {
        return (GameSetupView) mView;
    }
}
