package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.viewpresenter.IView;

/**
 * Created by criskey on 21/9/2016.
 */
public interface MainView extends IView{

    void onLoggedIn(String username);

    void onSignOut();

}
