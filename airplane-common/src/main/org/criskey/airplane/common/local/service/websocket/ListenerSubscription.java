package org.criskey.airplane.common.local.service.websocket;

import java.util.Map;

/**
 * Al credits goes to user <a href= "http://stackoverflow.com/users/3706184/eperrin95">Eperrin95</a>
 * <br>
 * <br>
 *  <a href="http://stackoverflow.com/questions/24346068/set-up-a-stomp-client-in-android-with-spring-framework-in-server-side">
 *  See this thread on stackoverflow
 *  </a>
 */
public interface ListenerSubscription {
    public void onMessage(Map<String, String> headers, String body);
}