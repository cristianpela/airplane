/**
 * Al credits goes to user <a href= "http://stackoverflow.com/users/3706184/eperrin95">Eperrin95</a>
 * <br>
 * <br>
 * <a href="http://stackoverflow.com/questions/24346068/set-up-a-stomp-client-in-android-with-spring-framework-in-server-side">
 * See this thread on stackoverflow
 * </a>
 */

package org.criskey.airplane.common.local.service.websocket;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Stomp {

    private static final String TAG = Stomp.class.getSimpleName();

    public static final int CONNECTED = 1;//Connection completely established
    public static final int NOT_CONNECTED = 2;//Connection process is ongoing
    public static final int DISCONNECTED_FROM_OTHER = 3;//Error, no more internet mConnStatus, etc.
    public static final int DISCONNECTED_FROM_APP = 4;//application explicitly ask for shut down the mConnStatus
    public static final int NO_CONNECTION = 5;//no mConnStatus, server is down before connecting;

    private static final String PREFIX_ID_SUBSCIPTION = "sub-";
    private static final String ACCEPT_VERSION_NAME = "accept-version";
    private static final String ACCEPT_VERSION = "1.1,1.0";
    private static final String COMMAND_CONNECT = "CONNECT";
    private static final String COMMAND_CONNECTED = "CONNECTED";
    private static final String COMMAND_MESSAGE = "MESSAGE";
    private static final String COMMAND_RECEIPT = "RECEIPT";
    private static final String COMMAND_ERROR = "ERROR";
    private static final String COMMAND_DISCONNECT = "DISCONNECT";
    private static final String COMMAND_SEND = "SEND";
    private static final String COMMAND_SUBSCRIBE = "SUBSCRIBE";
    private static final String COMMAND_UNSUBSCRIBE = "UNSUBSCRIBE";
    private static final String SUBSCRIPTION_ID = "id";
    private static final String SUBSCRIPTION_DESTINATION = "destination";
    private static final String SUBSCRIPTION_SUBSCRIPTION = "subscription";

    private static final Set<String> VERSIONS = new HashSet<String>();

    private static final int MAX_FRAME_SIZE = 16 * 1024;

    static {
        VERSIONS.add("V1.0");
        VERSIONS.add("V1.1");
        VERSIONS.add("V1.2");
    }

    private WebSocket mWebsocket;

    private AtomicInteger mSubCounter;

    private AtomicInteger mConnStatus;

    private Map<String, String> mHeaders;

    private Map<String, Subscription> mSubscriptions;

    private ListenerWSNetwork mNetworkListener;

    /**
     * Constructor of a stomp object. Only url used to set up a mConnStatus with a server can be instantiate
     *
     * @param url the url of the server to connect with
     */
    public Stomp(String url, Map<String, String> headersSetup, ListenerWSNetwork listenerWSNetwork) {
        try {
            this.mWebsocket = new WebSocket(new URI(url), null, headersSetup);
            this.mSubCounter = new AtomicInteger();
            this.mHeaders = new HashMap<String, String>();
            this.mConnStatus = new AtomicInteger(NOT_CONNECTED);
            this.mNetworkListener = listenerWSNetwork;
            this.mNetworkListener.onState(NOT_CONNECTED);
            mSubscriptions = new ConcurrentHashMap<String, Subscription>();

            this.mWebsocket.setEventHandler(new WebSocketEventHandler() {
                @Override
                public void onOpen() {
                    if (Stomp.this.mHeaders != null) {
                        Stomp.this.mHeaders.put(ACCEPT_VERSION_NAME, ACCEPT_VERSION);

                        transmit(COMMAND_CONNECT, Stomp.this.mHeaders, null);

                        System.out.println("...Web Socket Opened");
                    }
                }

                @Override
                public void onMessage(WebSocketMessage message) {
                    System.out.println("<<< " + message.getText());
                    Frame frame = Frame.fromString(message.getText());

                    boolean isMessageConnected = false;

                    if (frame.getCommand().equals(COMMAND_CONNECTED)) {
                        mConnStatus.compareAndSet(NOT_CONNECTED, CONNECTED);
                        mNetworkListener.onState(CONNECTED);

                        System.out.println("connected to server : " + frame.getHeaders().get("server"));
                        isMessageConnected = true;

                    } else if (frame.getCommand().equals(COMMAND_MESSAGE)) {
                        String subscriptionId = frame.getHeaders().get(SUBSCRIPTION_SUBSCRIPTION);
                        ListenerSubscription onReceive = mSubscriptions.get(subscriptionId).getCallback();

                        if (onReceive != null) {
                            onReceive.onMessage(frame.getHeaders(), frame.getBody());
                        } else {
                            System.err.println("Error : Subscription with id = " + subscriptionId + " had not been subscribed");
                            //ACTION TO DETERMINE TO MANAGE SUBCRIPTION ERROR
                        }

                    } else if (frame.getCommand().equals(COMMAND_RECEIPT)) {
                        //I DON'T KNOW WHAT A RECEIPT STOMP MESSAGE IS

                    } else if (frame.getCommand().equals(COMMAND_ERROR)) {
                        System.err.println("Error : Headers = " + frame.getHeaders() + ", Body = " + frame.getBody());
                        //ACTION TO DETERMINE TO MANAGE ERROR MESSAGE

                    } else {

                    }

                    if (isMessageConnected)
                        Stomp.this.subscribe();
                }

                @Override
                public void onClose() {
                    if (mConnStatus.get() == DISCONNECTED_FROM_APP) {
                        System.out.println("Web Socket disconnected");
                        disconnectFromAppResponse();
                    } else {
                        System.out.println("Problem : Web Socket disconnected whereas Stomp disconnect method has never "
                                + "been called.");
                        disconnectFromServerResponse();
                    }
                }

                @Override
                public void onPing() {

                }

                @Override
                public void onPong() {

                }

                @Override
                public void onError(IOException e) {
                    System.err.println("Error: " + e.getMessage());
                }

            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a message to server thanks to mWebsocket
     *
     * @param command one of a frame property, see {@link Frame} for more details
     * @param headers one of a frame property, see {@link Frame} for more details
     * @param body    one of a frame property, see {@link Frame} for more details
     */
    private void transmit(String command, Map<String, String> headers, String body) {
        String out = Frame.marshall(command, headers, body);
        System.out.println(">>> " + out);
        while (true) {
            if (out.length() > MAX_FRAME_SIZE) {
                this.mWebsocket.send(out.substring(0, MAX_FRAME_SIZE));
                out = out.substring(MAX_FRAME_SIZE);
            } else {
                this.mWebsocket.send(out);
                break;
            }
        }
    }

    /**
     * Set up a web socket mConnStatus with a server
     */
    public void connect() throws Exception {
        if (mConnStatus.get() != CONNECTED) {
            System.out.println("Opening Web Socket...");
            try {
                this.mWebsocket.connect();
            } catch (Exception e) {
                mNetworkListener.onState(NO_CONNECTION);
                System.out.println("Impossible to establish a mConnStatus : " + e.getClass() + ":" + e.getMessage());
                throw new Exception("Impossible to establish a mConnStatus : " + e.getClass() + ":" + e.getMessage());
            }
        }
    }

    /**
     * disconnection come from the server, without any intervention of client side. Operations order is very important
     */
    private void disconnectFromServerResponse() {
        if (mConnStatus.get() == CONNECTED) {
            this.mWebsocket.close();
            this.mNetworkListener.onState(DISCONNECTED_FROM_OTHER);
            mConnStatus.compareAndSet(CONNECTED, DISCONNECTED_FROM_OTHER);
        }
    }

    /**
     * disconnection come from the app, because the public method disconnect was called
     */
    private void disconnectFromAppResponse() {
        if (mConnStatus.get() == DISCONNECTED_FROM_APP) {
            this.mWebsocket.close();
            this.mNetworkListener.onState(mConnStatus.get());
            mConnStatus.compareAndSet(DISCONNECTED_FROM_APP, NOT_CONNECTED);
        }
    }

    /**
     * Close the web socket mConnStatus with the server. Operations order is very important
     */
    public void disconnect() {
        if (mConnStatus.get() == CONNECTED) {
            mConnStatus.compareAndSet(CONNECTED, DISCONNECTED_FROM_APP);
            transmit(COMMAND_DISCONNECT, null, null);
            this.mWebsocket.close();
        }
    }

    /**
     * Send a simple message to the server thanks to the body parameter
     *
     * @param destination The destination through a Stomp message will be send to the server
     * @param headers     mHeaders of the message
     * @param body        body of a message
     */
    public void send(String destination, Map<String, String> headers, String body) {
        if (mConnStatus.get() == CONNECTED) {
            if (headers == null)
                headers = new HashMap<String, String>();

            if (body == null)
                body = "";

            headers.put(SUBSCRIPTION_DESTINATION, destination);

            transmit(COMMAND_SEND, headers, body);
        }
    }

    /**
     * Allow a client to send a subscription message to the server independently of the initialization of the web socket.
     * If mConnStatus have not been already done, just save the subscription
     *
     * @param subscription a subscription object
     */
    public String subscribe(Subscription subscription) {
        subscription.setId(PREFIX_ID_SUBSCIPTION + this.mSubCounter.incrementAndGet());
        mSubscriptions.put(subscription.getId(), subscription);

        if (mConnStatus.get() == CONNECTED) {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put(SUBSCRIPTION_ID, subscription.getId());
            headers.put(SUBSCRIPTION_DESTINATION, subscription.getDestination());
            subscribe(headers);
        }
        return subscription.getId();
    }

    /**
     * Subscribe to a Stomp channel, through messages will be send and received. A message send from a determine channel
     * can not be receive in an another.
     */
    private void subscribe() {
        if (mConnStatus.get() == CONNECTED) {
            for (Subscription subscription : mSubscriptions.values()) {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put(SUBSCRIPTION_ID, subscription.getId());
                headers.put(SUBSCRIPTION_DESTINATION, subscription.getDestination());
                subscribe(headers);
            }
        }
    }

    /**
     * Send the subscribe to the server with an header
     *
     * @param headers header of a subscribe STOMP message
     */
    private void subscribe(Map<String, String> headers) {
        transmit(COMMAND_SUBSCRIBE, headers, null);
    }

    /**
     * Destroy a subscription with its id
     *
     * @param id the id of the subscription. This id is automatically setting up in the subscribe method
     */
    public void unsubscribe(String id) {
        if (mConnStatus.get() == CONNECTED) {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put(SUBSCRIPTION_ID, id);
            mSubscriptions.remove(id);
            this.transmit(COMMAND_UNSUBSCRIBE, headers, null);
        }
    }

    public void unsubscribeAll() {
        if (mConnStatus.get() == CONNECTED) {
            for (Iterator<Map.Entry<String, Subscription>> it = mSubscriptions.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Subscription> next = it.next();
                Map<String, String> headers = new HashMap<String, String>();
                headers.put(SUBSCRIPTION_ID, next.getKey());
                it.remove();
                this.transmit(COMMAND_UNSUBSCRIBE, headers, null);
            }
        }
    }
}