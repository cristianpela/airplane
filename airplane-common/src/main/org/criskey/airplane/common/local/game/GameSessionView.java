package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.List;

/**
 * Created by criskey on 20/10/2016.
 */
public interface GameSessionView extends IView{

    void responseAttempt(Shot attempt);

    void takeTurn(boolean hasTurn);

    void onTurnCountDown(int secondsToEnd);
}
