package org.criskey.airplane.common.local.persistance;

import org.criskey.airplane.common.local.util.AsyncCallback;

/**
 * Created by criskey on 25/11/2016.
 */
public class PrefixedStorageProxy implements Storage {

    private Enum prefix;

    private Storage settingsStorageImpl;

    public PrefixedStorageProxy(Prefix prefix, Storage settingsStorageImpl) {
        this.settingsStorageImpl = settingsStorageImpl;
        this.prefix = prefix;
    }

    @Override
    public <F> void save(Prefix prefix, String fieldKey, F field) {
        if (hasPrefix(prefix))
            settingsStorageImpl.save(prefix, fieldKey, field);
    }

    @Override
    public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
        if (hasPrefix(prefix))
            settingsStorageImpl.load(prefix, fieldKey, asyncCallback);
    }

    @Override
    public void wipe(Prefix prefix) {
        if (hasPrefix(prefix))
            settingsStorageImpl.wipe(prefix);
    }

    @Override
    public void commit(Prefix prefix) {
        if (hasPrefix(prefix))
            settingsStorageImpl.commit(prefix);
    }

    private boolean hasPrefix(Prefix prefix) {
        return this.prefix.equals(prefix);
    }
}
