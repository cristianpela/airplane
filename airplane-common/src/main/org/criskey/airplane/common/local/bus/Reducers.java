package org.criskey.airplane.common.local.bus;

import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.InvitationListPresenter.InvitationListState;
import org.criskey.airplane.common.local.JoinPresenter.JoinState;
import org.criskey.airplane.common.local.LobbyListPresenter.LobbyState;
import org.criskey.airplane.common.local.game.GameSessionState;
import org.criskey.airplane.common.local.game.GameSetupState;
import org.criskey.airplane.common.local.game.GameState;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.util.FluentMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.GameNetworkProtocol.*;

/**
 * Created by criskey on 30/1/2017.
 */
public final class Reducers {

    private Reducers() {
    }

    public static <T> Reducer<T> just(final T newState) {
        return new Reducer<T>() {
            @Override
            public T onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                return newState;
            }
        };
    }

    public static Reducer<LobbyState> lobbyNewUser(final UserInfo userInfo) {
        return new Reducer<LobbyState>() {
            @Override
            public LobbyState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                JoinState joinState = getJoinState(state);
                LobbyState oldLobbyState = getLobbyState(state);

                List<UserInfo> newUsers = copyList(oldLobbyState.users);
                boolean isPrincipal = userInfo.username.equals(joinState.username);
                if (!isPrincipal && !newUsers.contains(userInfo))
                    newUsers.add(userInfo);
                LobbyState newLobbyState = new LobbyState(oldLobbyState.disabled, newUsers);

                return newLobbyState;
            }
        };
    }

    public static Reducer<LobbyState> lobbyChangeUser(final UserInfo userInfo) {
        return new Reducer<LobbyState>() {
            @Override
            public LobbyState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                JoinState joinState = getJoinState(state);
                LobbyState oldLobbyState = getLobbyState(state);

                List<UserInfo> newUsers = copyList(oldLobbyState.users);
                boolean isPrincipal = userInfo.username.equals(joinState.username);
                int index = newUsers.indexOf(userInfo);
                if (!isPrincipal && index > -1) {
                    newUsers.set(index, userInfo);
                }
                LobbyState newLobbyState = new LobbyState(oldLobbyState.disabled, newUsers);

                return newLobbyState;
            }
        };
    }


    public static CompositeReducer lobbyRemoveUser(final UserInfo userInfo) {
        return new CompositeReducer() {
            @Override
            public Map<AppEventType, Reducer<?>> onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                getJoinState(state);
                LobbyState oldLobbyState = getLobbyState(state);
                List<UserInfo> newUsers = copyList(oldLobbyState.users);
                newUsers.remove(userInfo);
                LobbyState newLobbyState = new LobbyState(oldLobbyState.disabled, newUsers);
                return new FluentMap<AppEventType, Reducer<?>>()
                        .put(AppEventType.USER_INVITATION, Reducers.invitation(userInfo.username, false))
                        .put(AppEventType.LOBBY, Reducers.just(newLobbyState))
                        .get();
            }
        };
    }

    public static Reducer<LobbyState> lobbyDisable(final boolean disable) {
        return new Reducer<LobbyState>() {
            @Override
            public LobbyState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                getJoinState(state);
                LobbyState oldLobbyState = getLobbyState(state);
                return new LobbyState(
                        disable,
                        oldLobbyState.users
                );
            }
        };
    }

    public static Reducer<InvitationListState> invitation(final String invitation, final boolean isNewOne) {
        return new Reducer<InvitationListState>() {
            @Override
            public InvitationListState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                getJoinState(state);
                LobbyState lobbyState = getLobbyState(state);

                InvitationListState oldState = (InvitationListState) state.get(AppEventType.USER_INVITATION);
                List<String> newInvitations = copyList((oldState == null) ? null : oldState.invitations);
                if (isNewOne)
                    newInvitations.add(invitation);
                else {
                    newInvitations.remove(invitation);
                }
                return new InvitationListState(oldState == null ? false : oldState.disabled, newInvitations);
            }
        };
    }

    public static Reducer<InvitationListState> invitationDisabled(final boolean disable) {
        return new Reducer<InvitationListState>() {
            @Override
            public InvitationListState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                getJoinState(state);
                InvitationListState oldState = (InvitationListState) state.get(AppEventType.USER_INVITATION);
                return new InvitationListState(disable, copyList(oldState == null ? null : oldState.invitations));
            }
        };
    }

    public static Reducer<GameState> gameSessionSetPlaneConfiguration(final PlaneConfiguration planeConfiguration) {
        return new Reducer<GameState>() {
            @Override
            public GameState onReduce(AppEventType eventType, Map<AppEventType, ?> state) {
                GameState oldState = (GameState) state.get(AppEventType.GAME_SESSION);
                if (oldState == null)
                    throw new IllegalStateException("Game state is not initialized");

                GameSetupState gameSetupState = oldState.gameSetupState;
                gameSetupState = new GameSetupState(gameSetupState.isConfigurationSubmitted, planeConfiguration);

                return new GameState(
                        oldState.phase,
                        oldState.gameKey,
                        oldState.otherPlayerId,
                        oldState.statusMessage,
                        gameSetupState,
                        oldState.gameSessionState);
            }
        };
    }

    public static CompositeReducer gameSession(final GameNetworkProtocol protocol) {

        return new CompositeReducer() {
            @Override
            public Map<AppEventType, Reducer<?>> onReduce(AppEventType eventType, Map<AppEventType, ?> state) {

                Map<AppEventType, Reducer<?>> reducers = new HashMap<AppEventType, Reducer<?>>();

                final int phase = protocol.getPhase();

                GameState oldState = (GameState) state.get(AppEventType.GAME_SESSION);
                GameSessionState gameSessionState = (oldState == null) ? null : oldState.gameSessionState;
                GameSetupState gameSetupState = (oldState == null) ? null : oldState.gameSetupState;
                String statusMessage = (oldState == null) ? null : oldState.statusMessage;

                switch (phase) {
                    case PHASE_PREPARE: {
                        reducers.put(AppEventType.LOBBY, lobbyDisable(true));
                        reducers.put(AppEventType.USER_INVITATION, invitationDisabled(true));
                        gameSessionState = new GameSessionState(
                                new ArrayList<Shot>(),
                                new ArrayList<Shot>(),
                                false,
                                false);
                        gameSetupState = new GameSetupState(false, null);
                        break;
                    }
                    case PHASE_OTHER_READY: {
                        statusMessage = LanguageSupport.SCREEN_GAME_SETUP_LBL_OTHER_READY;
                        break;
                    }
                    case GameNetworkProtocol.PHASE_PRINCIPAL_READY: {
                        statusMessage = LanguageSupport.SCREEN_GAME_SETUP_LBL_ME_READY;
                        PlaneConfiguration planeConfiguration = PlaneConfiguration.unpack(protocol.getData());
                        gameSetupState = new GameSetupState(true, planeConfiguration);
                        break;
                    }
                    case GameNetworkProtocol.PHASE_TAKE_TURN: {
                        Shot opponentShot = Shot.unpack(protocol.getData());
                        List<Shot> opponentAttempts = copyList(gameSessionState.opponentAttempts);
                        opponentAttempts.add(opponentShot);
                        gameSessionState = new GameSessionState(
                                copyList(gameSessionState.attempts),
                                opponentAttempts,
                                true,
                                gameSessionState.isWinner);
                        statusMessage = LanguageSupport.SCREEN_GAME_SESSION_TAKE_TURN;
                        break;
                    }
                    case GameNetworkProtocol.PHASE_WAIT_TURN: {
                        Shot myShot = Shot.unpack(protocol.getData());
                        List<Shot> myAttempts = copyList(gameSessionState.attempts);
                        myAttempts.add(myShot);
                        gameSessionState = new GameSessionState(
                                myAttempts,
                                copyList(gameSessionState.opponentAttempts),
                                false,
                                gameSessionState.isWinner);
                        statusMessage = LanguageSupport.SCREEN_GAME_SESSION_WAIT_TURN;
                        break;
                    }
                    case PHASE_START: {
                        boolean hasTurn = protocol.getData() == GameNetworkProtocol.PHASE_TAKE_TURN;
                        gameSessionState = new GameSessionState(
                                copyList(gameSessionState.attempts),
                                copyList(gameSessionState.opponentAttempts),
                                hasTurn,
                                false);
                        statusMessage = (hasTurn) ? LanguageSupport.SCREEN_GAME_SESSION_TAKE_TURN
                                : LanguageSupport.SCREEN_GAME_SESSION_WAIT_TURN;
                        break;
                    }
                    case PHASE_END: {
                        TargetProtocol status = TargetProtocol.values()[protocol.getData()];
                        gameSessionState = new GameSessionState(
                                copyList(gameSessionState.attempts),
                                copyList(gameSessionState.opponentAttempts),
                                status == TargetProtocol.WINNER,
                                gameSessionState.isWinner);
                        statusMessage = (status == TargetProtocol.WINNER)
                                ? LanguageSupport.SCREEN_GAME_SESSION_WINNER
                                : LanguageSupport.SCREEN_GAME_SESSION_LOSER;
                        reducers.put(AppEventType.LOBBY, lobbyDisable(false));
                        reducers.put(AppEventType.USER_INVITATION, invitationDisabled(false));
                        break;
                    }
                    case PHASE_SESSION_LOST: {
                        reducers.put(AppEventType.LOBBY, lobbyDisable(false));
                        reducers.put(AppEventType.USER_INVITATION, invitationDisabled(false));
                        break;
                    }
                }
                reducers.put(AppEventType.GAME_SESSION, Reducers.just(new GameState(
                        phase,
                        protocol.getGameKey(),
                        protocol.getOtherPlayerId(),
                        statusMessage,
                        gameSetupState,
                        gameSessionState
                )));
                return reducers;
            }
        };
    }

    private static JoinState getJoinState(Map<AppEventType, ?> state) {
        JoinState joinState = (JoinState) state.get(AppEventType.LOGGED);
        if (joinState == null)
            throw new IllegalStateException("You are are not logged in");
        return joinState;
    }

    private static LobbyState getLobbyState(Map<AppEventType, ?> state) {
        LobbyState lobbyState = (LobbyState) state.get(AppEventType.LOBBY);
        if (lobbyState == null)
            lobbyState = new LobbyState(false, new ArrayList<UserInfo>());
//            throw new IllegalStateException("Lobby is not created. Make sure you access the lobby state after login and" +
//                    " lobby creation - for example here is an example of login + lobby creation:\n " +
//                    "\tdispatchState(new AppEvent(AppEventType.LOGGED, Reducers.just(JoinState(\"foo\", true))));\n" +
//                    "\tdispatchState(new AppEvent(AppEventType.LOBBY,  Reducers.just(new LobbyListPresenter.LobbyState(false, users))));");
        return lobbyState;
    }

    private static <T> List<T> copyList(List<T> from) {
        List<T> list = new ArrayList<T>();
        if (from != null) {
            list.addAll(from);
        }
        return list;
    }

}
