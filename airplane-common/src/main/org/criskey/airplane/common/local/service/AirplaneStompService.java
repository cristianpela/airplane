package org.criskey.airplane.common.local.service;

import com.google.gson.Gson;
import net.iharder.Base64;
import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.local.service.websocket.ListenerWSNetwork;
import org.criskey.airplane.common.local.service.websocket.Stomp;
import org.criskey.airplane.common.local.service.websocket.Subscription;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.util.Position;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.criskey.airplane.common.NetContract.*;

/**
 * Created by criskey on 22/9/2016.
 */
public class AirplaneStompService implements IAirplaneStompService {

    private ListenerWSNetwork listenerWSNetWork;

    private Stomp stomp;

    private ExecutorService wsExecutor = Executors.newSingleThreadExecutor();

    private Gson gson;

    private String hostName;

    private String username;

    private String password;

    public AirplaneStompService(ListenerWSNetwork listenerWSNetwork, Gson gson) throws URISyntaxException {
        this(LOCAL_HOST, listenerWSNetwork, gson);
    }

    public AirplaneStompService(String hostName, ListenerWSNetwork listenerWSNetwork, Gson gson) throws URISyntaxException {
        this.hostName = hostName;
        this.gson = gson;
        this.listenerWSNetWork = listenerWSNetwork;
    }

    private void initStomp(String hostName, ListenerWSNetwork listenerWSNetwork) {
        Map<String, String> httpHeader = new HashMap<String, String>();
        httpHeader.put("Authorization", "Basic " + new String(Base64.encodeBytes((username + ":" + password).getBytes())));
        stomp = new Stomp(createWSRoot(hostName, true), httpHeader, listenerWSNetwork);
    }


    @Override
    public void changeHost(String host) {
        this.hostName = host;

    }

    @Override
    public String getHost() {
        return hostName;
    }

    @Override
    public String addSubscription(String destination, ListenerSubscription listener) {
        Subscription sub = new Subscription(destination, listener);
        return stomp.subscribe(sub);
    }

    @Override
    public void removeSubscription(String subscriptionId) {
        stomp.unsubscribe(subscriptionId);
    }

    @Override
    public void connect(final String username, final String password, final AsyncCallback<String> asyncCallback) {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //String url = paramedRequest(createHttpRoot(hostName), REQ_USERS);
                   // String response = httpRequest(url, username, password);
                    AirplaneStompService.this.username = username;
                    AirplaneStompService.this.password = password;
                    initStomp(hostName, listenerWSNetWork);
                    stomp.connect();
                    asyncCallback.onReturn("OK", null);
                } catch (Exception e) {
                    asyncCallback.onReturn(null, new Error(e.getClass().toString() + ": " + e.getMessage()));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void disconnect() {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                stomp.unsubscribeAll();
                stomp.disconnect();
//                try {
//                    httpRequest(paramedRequest(createHttpRoot(hostName), REQ_LOGOUT),username, password);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                username = password = null;
            }
        });
    }

    @Override
    public void joinLobby() {
//        wsExecutor.submit(new Runnable() {
//            @Override
//            public void run() {
//                stomp.send(paramedRequest(WebSocketBrokers.PREFIX_APP, MSG_JOIN), null, "");
//            }
//        });
    }

    @Override
    public void sendInvitation(final String username) {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                stomp.send(paramedRequest(WebSocketBrokers.PREFIX_APP, NetContract.PRIVATE_MSG_INVITE, username), null, "");
            }
        });
    }

    @Override
    public void acceptInvite(final String username, final boolean accept) {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                stomp.send(paramedRequest(NetContract.WebSocketBrokers.PREFIX_APP, PRIVATE_MSG_INVITE_STATUS, username), null,
                        Boolean.toString(accept));
            }
        });
    }

    @Override
    public void attemptHit(final String gameKey, final String otherPlayerId, final Position position) {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                GameNetworkProtocol protocol = new GameNetworkProtocol(gameKey, otherPlayerId,
                        GameNetworkProtocol.PHASE_CONFIG, Shot.pack(new Shot(position, TargetProtocol.ATTEMPT)));
                stomp.send(WebSocketBrokers.PREFIX_APP + PRIVATE_MSG_GAME_TARGET, null, gson.toJson(protocol));
            }
        });
    }

    @Override
    public String submitConfiguration(String gameKey, String otherPlayerId, PlaneConfiguration configuration) {
        GameNetworkProtocol protocol = new GameNetworkProtocol(gameKey, otherPlayerId,
                GameNetworkProtocol.PHASE_CONFIG, PlaneConfiguration.pack(configuration));
        final String jsonProtocol = gson.toJson(protocol);
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                stomp.send(WebSocketBrokers.PREFIX_APP + PRIVATE_MSG_GAME_CONFIG, null, jsonProtocol);
            }
        });
        return jsonProtocol;
    }

    @Override
    public void leaveSession(final String gameKey, final String otherPlayerId) {
        wsExecutor.submit(new Runnable() {
            @Override
            public void run() {
                GameNetworkProtocol protocol = GameNetworkProtocol
                        .createEmpty(gameKey, otherPlayerId, GameNetworkProtocol.PHASE_SESSION_LOST);
                stomp.send(WebSocketBrokers.PREFIX_APP + PRIVATE_MSG_GAME_LEAVE, null, gson.toJson(protocol));
            }
        });
    }

    private String httpRequest(String url, String username, String password) throws IOException {

        String basicAuth = "Basic " + new String(Base64.encodeBytes((username + ":" + password).getBytes()));
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestProperty("Authorization", basicAuth);
        con.setConnectTimeout((int) TimeUnit.SECONDS.toMillis(30));
        con.setReadTimeout((int) TimeUnit.SECONDS.toMillis(30));

        //check for http error
        String inputLine;
        StringBuilder response = new StringBuilder();
        InputStream errIn = con.getErrorStream();
        if (errIn != null) {
            BufferedReader buffErrIn = new BufferedReader(
                    new InputStreamReader(con.getErrorStream()));
            while ((inputLine = buffErrIn.readLine()) != null) {
                response.append(inputLine);
            }
            buffErrIn.close();
            throw new HttpException(response.toString());
        }

        //if we've reached here. wir gud
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private Map<String, String> getAuthStompHeader() {
        if (password == null || username == null) {
            throw new IllegalStateException("Password and Username were not set. Use login method first");
        }
        Map<String, String> stompHeaders = new HashMap<String, String>();
        stompHeaders.put("login", username);
        stompHeaders.put("passcode", password);
        return stompHeaders;
    }


}
