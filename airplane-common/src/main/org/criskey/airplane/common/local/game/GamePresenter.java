package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.criskey.airplane.common.log.ILogger;
import org.criskey.airplane.common.log.Logger;

/**
 * Created by criskey on 2/11/2016.
 */
public abstract class GamePresenter extends Presentable {

//    protected String otherPlayerId;
//
//    protected String gameKey;

    public GamePresenter(ArgsBatch batch) {
        super(batch);
        addAppEventTypeToListen(AppEventType.GAME_SESSION);
    }

    public void leaveSession() {
        GameState gameState = getGameState();
        String gameKey = gameState.gameKey;
        String otherPlayerId = gameState.otherPlayerId;
        if (gameKey != null && otherPlayerId != null) {
            Logger.out.print(this, "Leaving session ID:{" + gameKey + "}", ILogger.Level.ERROR);
            mService.leaveSession(gameKey, otherPlayerId);
        }
    }

    protected GameState getGameState() {
        GameState gameState =  (GameState) mStateContainer.getState().get(AppEventType.GAME_SESSION);
        return gameState;
    }

}
