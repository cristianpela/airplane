package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.platform.MessageDisplay;

import java.util.Map;

/**
 * Created by criskey on 28/10/2016.
 */
public class ViewHelper implements IView {

    //TODO: maybe put the foreground mView in a WeakReference?
    private IView view, backView;

    private Presentable presentable;

    private MessageDisplay messageDisplay;

    public ViewHelper(IView view, IView backView, Presentable presentable, MessageDisplay messageDisplay) {
        this.messageDisplay = messageDisplay;
        this.presentable = presentable;
        this.view = view;
        this.backView = backView;
    }

    public ViewHelper(IView view, IView backView, Presentable presentable) {
        this(view, backView, presentable, null);
    }

    public ViewHelper(IView view, Presentable presentable, MessageDisplay messageDisplay) {
        this(view, null, presentable, messageDisplay);
    }

    public ViewHelper(IView view, Presentable presentable) {
        this(view, null, presentable, null);
    }

    @Override
    public void onResume() {
        presentable.onAttach(view);
    }

    @Override
    public void onPause() {
        presentable.onDetach();
        if (backView != null)
            presentable.onAttach(backView);
    }

    @Override
    public void showMessage(String message, boolean pop) {
        if (messageDisplay != null)
            messageDisplay.show(
                    MessageDisplay.Kind.INFO,
                    presentable.i18n(LanguageSupport.TITLE_DIALOG_INFO),
                    message);
    }

    @Override
    public void showError(String error, boolean pop) {
        if (messageDisplay != null)
            messageDisplay.show(
                    MessageDisplay.Kind.ERROR,
                    presentable.i18n(LanguageSupport.TITLE_DIALOG_ERROR),
                    error);
    }

    @Override
    public void restore(Map<String, ?> dataMap) {
        throw new UnsupportedOperationException("This method must be implemented by concrete classes");
    }

}
