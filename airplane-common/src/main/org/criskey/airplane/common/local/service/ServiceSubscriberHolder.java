package org.criskey.airplane.common.local.service;

import com.google.gson.Gson;
import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.bus.*;
import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.log.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.NetContract.*;
import static org.criskey.airplane.common.NetContract.WebSocketBrokers.PREFIX_USER_DEST;

/**
 * Created by criskey on 17/10/2016.
 */
public class ServiceSubscriberHolder implements DataBusListener {

    private IAirplaneStompService mService;

    private StateContainer mStateContainer;

    private OnUIRunCallback uiRunCallback;

    private Gson mGson;

    private boolean subscribed;

    public ServiceSubscriberHolder(IAirplaneStompService service, StateContainer stateContainer, Gson gson, OnUIRunCallback uiRunCallback) {
        this.mService = service;
        this.mStateContainer = stateContainer;
        this.uiRunCallback = uiRunCallback;
        this.mGson = gson;
    }

    @Override
    public void onEvent(AppEvent event) {
        IAirplaneStompService.Status status = (IAirplaneStompService.Status) event.data;
        if (status == IAirplaneStompService.Status.CONNECTED) {
            Logger.out.printd(this, "Subscribing to websocket messages");
            subscribe();
        }
    }

    private void subscribe() {
        mService.addSubscription(SUB_TOPIC_JOINED, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, String body) {
                final UserInfo user = mGson.fromJson(body, UserInfo.class);
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY, Reducers.lobbyNewUser(user)));
                    }
                });
            }
        });

        mService.addSubscription(SUB_TOPIC_LEFT, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, String body) {
                final UserInfo user = mGson.fromJson(body, UserInfo.class);
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY, Reducers.lobbyRemoveUser(user)));
                    }
                });
            }
        });

        mService.addSubscription(SUB_TOPIC_CHANGE, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, String body) {
                final UserInfo user = mGson.fromJson(body, UserInfo.class);
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY, Reducers.lobbyChangeUser(user)));
                    }
                });
            }
        });

        mService.addSubscription(PREFIX_USER_DEST + SUB_QUEUE_INVITES, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, final String invitation) {
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.USER_INVITATION,
                                Reducers.invitation(invitation, true)
                        ));
                    }
                });
            }
        });

        mService.addSubscription(PREFIX_USER_DEST + SUB_QUEUE_GAME, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, String body) {
                final GameNetworkProtocol protocol = mGson.fromJson(body, GameNetworkProtocol.class);
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                                Reducers.gameSession(protocol)
                        ));
                    }
                });

            }
        });

        mService.addSubscription(PREFIX_USER_DEST + SUB_QUEUE_ERROR, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, final String body) {
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.announce(new AppEvent(AppEventType.ERROR, body));
                    }
                });
            }
        });

        mService.addSubscription(PREFIX_USER_DEST + SUB_LOBBY, new ListenerSubscription() {
            @Override
            public void onMessage(Map<String, String> headers, String body) {
                final List<UserInfo> users = Arrays.asList(mGson.fromJson(body, UserInfo[].class));
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                                Reducers.just(new LobbyListPresenter.LobbyState(false, users))));
                    }
                });
                // mService.joinLobby();
            }
        });
    }

    @Override
    public AppEventType[] getAppEventsToListen() {
        return new AppEventType[]{AppEventType.NETWORK_CONNECTION};
    }
}
