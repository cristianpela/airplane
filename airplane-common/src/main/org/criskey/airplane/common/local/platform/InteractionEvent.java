package org.criskey.airplane.common.local.platform;

import org.criskey.airplane.common.util.Position;

/**
 * Created by criskey on 13/9/2016.
 */
public class InteractionEvent {

    public enum Type {
        ROTATION, CHECK, TRANSLATE_START, TRANSLATE, TRANSLATE_END;
    }

    public final Position position;

    public final Type type;

    public InteractionEvent(Position position, Type type) {
        this.position = position;
        this.type = type;
    }

    @Override
    public String toString() {
        return "InteractionEvent{" +
                "position=" + position +
                ", type=" + type +
                '}';
    }
}
