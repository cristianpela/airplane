package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 14/10/2016.
 */
public interface DataBusListener {

    public void onEvent(AppEvent event);

    public AppEventType[] getAppEventsToListen();
}
