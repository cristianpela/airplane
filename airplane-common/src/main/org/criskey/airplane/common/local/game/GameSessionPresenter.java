package org.criskey.airplane.common.local.game;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.util.Position;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by criskey on 20/10/2016.
 */
public class GameSessionPresenter extends GamePresenter {

    public static final String KEY_LIST_ATTEMPTS = "KEY_LIST_ATTEMPTS";
    public static final String KEY_STRING_STATUS_MESSAGE = "KEY_STRING_STATUS_MESSAGE";
    public static final String KEY_BOOLEAN_HAS_TURN = "KEY_BOOLEAN_HAS_TURN";

    public static final int TURN_SECONDS = 60;

    private Timer mTurnTimer;

    private boolean mEnableCountDown;

    private AtomicInteger mCurrentTurnCountDown = new AtomicInteger(TURN_SECONDS);

    public GameSessionPresenter(ArgsBatch batch) {
        super(batch);
    }

    public void enableTurnCountDownTimer(boolean enableCountDown) {
        this.mEnableCountDown = enableCountDown;
    }

    @Override
    public void onLanguageChanged() {
        restore();
    }

    @Override
    public void onAttach(IView view) {
        this.mView = view;
        restore();
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    @Override
    public void onEvent(AppEvent event) {

        GameState gameState = getGameState();
        GameSessionState gameSessionState = gameState.gameSessionState;
        int phase = gameState.phase;

        switch (phase) {
            case GameNetworkProtocol.PHASE_START:
            case GameNetworkProtocol.PHASE_TAKE_TURN: {
                getView().takeTurn(gameSessionState.hasTurn);
                getView().showMessage(i18n(gameState.statusMessage), false);
                if (gameSessionState.hasTurn) startTurnCountDown();
                break;
            }
            case GameNetworkProtocol.PHASE_WAIT_TURN: {
                int lastShotIndex = gameSessionState.attempts.size() - 1;
                Shot myShot = gameState.gameSessionState.attempts.get(lastShotIndex); // shot feedback
                getView().takeTurn(gameState.gameSessionState.hasTurn);
                getView().responseAttempt(myShot);
                getView().showMessage(i18n(gameState.statusMessage), false);
                break;
            }
            case GameNetworkProtocol.PHASE_END: {
                getView().showMessage(i18n(gameState.statusMessage), true);
                mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, null));
                endTurnCountDown();
                break;
            }
            case GameNetworkProtocol.PHASE_SESSION_LOST: {
                mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, null));
                endTurnCountDown();
                break;
            }
        }
    }

    private void endTurnCountDown() {
        if (mTurnTimer != null)
            mTurnTimer.cancel();
        mCurrentTurnCountDown.set(TURN_SECONDS);
    }

    private void startTurnCountDown() {
        if (!mEnableCountDown) return;

        mTurnTimer = new Timer();
        mTurnTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                uiRunCallback.runOnUI(new OnUICodeInjector() {
                    @Override
                    public void inject() {
                        if (mView != null)
                            getView().onTurnCountDown(mCurrentTurnCountDown.get());
                        if (mCurrentTurnCountDown.decrementAndGet() <= 0) {
                            attemptHit(Position.NaP);
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    public void attemptHit(Position position) {
        GameState gameState = getGameState();
        GameSessionState gameSessionState = gameState.gameSessionState;
        if (gameSessionState.hasTurn) {
            endTurnCountDown();
            mService.attemptHit(gameState.gameKey, gameState.otherPlayerId, position);
        }
    }

    private GameSessionView getView() {
        return (GameSessionView) mView;
    }

    private void restore() {
        GameState gameState = getGameState();
        if (gameState != null) {
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(KEY_LIST_ATTEMPTS, gameState.gameSessionState.attempts);
            dataMap.put(KEY_STRING_STATUS_MESSAGE, gameState.statusMessage == null ? "" : i18n(gameState.statusMessage));
            dataMap.put(KEY_BOOLEAN_HAS_TURN, gameState.gameSessionState.hasTurn);
            this.getView().restore(dataMap);
        }
    }
}
