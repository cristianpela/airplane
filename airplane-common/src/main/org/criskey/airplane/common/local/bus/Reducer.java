package org.criskey.airplane.common.local.bus;

import java.util.Map;

/**
 * Created by criskey on 30/1/2017.
 */
public interface Reducer<T> {

    /**
     * @param eventType
     * @param state is mutable
     * @return
     */
    T onReduce(AppEventType eventType, Map<AppEventType, ?> state);
}
