package org.criskey.airplane.common.local.bus;

/**
 * Created by criskey on 2/2/2017.
 */
public abstract class SimpleState implements State {

    @Override
    public boolean isRetained() {
        return false;
    }

    @Override
    public void onClear() {
    }
}
