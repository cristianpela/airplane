package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.viewpresenter.IView;

import java.util.List;

/**
 * Created by criskey on 26/9/2016.
 */
public interface InvitationListView extends IView{

    public void onNewInvitation(String invitation);

    public void onInvitationAccepted(String invitation);

    public void setEntriesEnabled(boolean enabled);
}
