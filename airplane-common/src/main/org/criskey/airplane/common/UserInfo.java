package org.criskey.airplane.common;

/**
 * Created by criskey on 27/9/2016.
 */
public class UserInfo {

    public final String username;

    public final String id;

    public final boolean isPlaying;

    public UserInfo(String username, String id, boolean isPlaying) {
        this.username = username;
        this.id = id;
        this.isPlaying = isPlaying;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInfo userInfo = (UserInfo) o;

        return username.equals(userInfo.username);

    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return "UserInfo{" + username + " }";
    }
}
