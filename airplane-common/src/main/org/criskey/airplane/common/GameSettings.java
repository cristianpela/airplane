package org.criskey.airplane.common;

/**
 * Created by criskey on 30/8/2016.
 */
public class GameSettings {

    public static final int BOARD_SIZE = 10;

    /**
     * ten hits assure a winner
     */
    public static final int VICTORY_CONDITION = 10;

}
