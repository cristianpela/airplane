package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Position;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 30/8/2016.
 */
public class PositionTest {


    @Test
    public void shouldProperReturnRowAndColumn() {
        Position pos = Position.fromAlgebraicNotation("A1");
        assertNotNull(pos);
        //indexing starts from 0.. So for Cols [A: 0, B 1], For Rows Put + 1
        assertEquals("Should be col 0, row 0", new Position(0, 0), pos);
        assertEquals("Should be col 1, Row 3", new Position(3, 1), Position.fromAlgebraicNotation("B4"));
        assertEquals("Should be col 9, Row 9", new Position(9, 9), Position.fromAlgebraicNotation("J10"));
    }

    @Test
    public void shouldProperConvertToAlgebraicNotation(){
        Position pos = new Position(0,0);
        assertEquals("Should be A1", "A1", Position.toAlgebraicNotation(pos));
        pos = new Position(0, 4);
        assertEquals("Should be E1", "E1", Position.toAlgebraicNotation(pos));
    }

    @Test
    public void movePosition() {
        Position pos = new Position(0, 0);
        Position p = pos.add(Position.horizontal(3));
        assertEquals("Should be col 3, row 0", new Position(0, 3), p);
        p = p.add(Position.vertical(1));
        assertEquals("Should be col 3, row 1", new Position(1, 3), p);
        p = p.add(Position.vertical(-2));
        assertEquals("Should be col 3, row 1", new Position(-1, 3), p);
    }

    @Test(expected = Exception.class)
    public void badInstance() {
        Position.fromAlgebraicNotation("dfjsdfk");
    }


    @Test
    public void packTest() {
        Position p = new Position(14, 56);
        Position up = Position.unpack(Position.pack(p));
        assertEquals(p, up);
    }
}