package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.JoinPresenter;
import org.criskey.airplane.common.local.JoinView;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;

/**
 * Created by criskey on 27/1/2017.
 */
public class JoinPresenterTest extends BasePresenterTest {

    JoinPresenter mPresenter;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mPresenter = new JoinPresenter(newArgsBatch());
        mPresenter.onAttach(mock(JoinView.class));
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                String data = mGson.toJson(mUsers);
                AsyncCallback<String> cb = invocationOnMock.getArgument(2);
                cb.onReturn(data, null);
                return null;
            }
        }).when(mService).connect(anyString(), anyString(), any(AsyncCallback.class));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }


    @Test
    public void connect_and_update_state(){
        mPresenter.join("foo","bar");
        Map<AppEventType, Object> state = mStateContainer.getState();
        assertEquals("foo", ((JoinPresenter.JoinState) state.get(AppEventType.LOGGED)).username);
        List<UserInfo> users = (List<UserInfo>) state.get(AppEventType.LOBBY);
        assertEquals(2, users.size());
        assertEquals("abc", users.get(0).username);
        assertEquals("xyz", users.get(1).username);

        //logout
        mPresenter.broadcast(new AppEvent(AppEventType.LOGGED, null));
        mPresenter.broadcast(new AppEvent(AppEventType.LOBBY, null));
        state = mStateContainer.getState();
        assertTrue(state.get(AppEventType.LOGGED) == null);
        assertTrue(state.get(AppEventType.LOBBY) == null);
    }
}