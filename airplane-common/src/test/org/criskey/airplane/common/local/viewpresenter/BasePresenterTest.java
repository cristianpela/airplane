package org.criskey.airplane.common.local.viewpresenter;

import com.google.gson.Gson;
import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.JoinPresenter;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.bus.*;
import org.criskey.airplane.common.local.game.GameState;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.criskey.airplane.common.util.Position;
import org.junit.After;
import org.junit.Before;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 27/1/2017.
 */
public class BasePresenterTest {

    public static AirplaneModels models = AirplaneModels.get(null);
    public static PlaneConfiguration PLANE_CONFIG = new PlaneConfiguration(
            models.getRandomTypeId(), randomPosition(), Orientation.EST
    );

    public static final String GAME_KEY = "GAME_KEY";
    public static final String OTHER_PLAYER_ID = "other-player-id";


    ArgumentCaptor<String> mMsgArgCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Shot> mShotArgCaptor = ArgumentCaptor.forClass(Shot.class);
    ArgumentCaptor<Boolean> mTurnArgCaptor = ArgumentCaptor.forClass(Boolean.class);
    ArgumentCaptor<Map<String, Object>> mRestoreArgCaptor = ArgumentCaptor.forClass(Map.class);


    protected ArrayList<UserInfo> mUsers = new ArrayList<UserInfo>();

    {
        mUsers.add(new UserInfo("abc", "id_abc", false));
        mUsers.add(new UserInfo("xyz", "id_xyz", false));
    }

    protected  IAirplaneStompService mService;

    protected StateContainer mStateContainer;

    protected Gson mGson;

    protected LanguageSupport mLang;

    protected OnUIRunCallback mUiRunCallback;

    @Before
    public void setUp() throws Exception {
        mLang = mock(LanguageSupport.class);
        mUiRunCallback = mock(OnUIRunCallback.class);
        when(mUiRunCallback.isUIThread()).thenReturn(true);
        mService = mock(IAirplaneStompService.class);
        mGson = new Gson();
        mStateContainer = new StateContainer(new DataBus(mUiRunCallback));
    }


    @After
    public void tearDown() throws Exception {

    }

    protected Presentable.ArgsBatch newArgsBatch(){
        return new Presentable.ArgsBatch(
                mService,
                mStateContainer,
                mock(Router.class),
                mGson,
                mock(Storage.class),
                mLang = mock(LanguageSupport.class),
                mUiRunCallback
        );
    }

    protected void loginAndCreateLobby(){
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOGGED, Reducers.just(
                new JoinPresenter.JoinState("foo", "bar", true, true, false))));
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.just(new LobbyListPresenter.LobbyState(false, mUsers))));
    }

    protected void logout(){
        mStateContainer.reset();
    }

    protected void whenLanguage() {
        doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                return invocationOnMock.getArgument(0);
            }
        }).when(mLang).i18n(anyString());
    }

    protected void createGameSession() {
        createSetupGameSession();
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_PRINCIPAL_READY,
                                PlaneConfiguration.pack(PLANE_CONFIG)))));
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_START,
                                GameNetworkProtocol.PHASE_TAKE_TURN))));
    }


    protected void createSetupGameSession() {
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(GameNetworkProtocol.createEmpty(GAME_KEY, OTHER_PLAYER_ID, GameNetworkProtocol.PHASE_PREPARE))));
    }

    protected static Position randomPosition() {
        Random random = new Random();
        int bound = 10;
        return Position.asXY(random.nextInt(bound), random.nextInt(bound));
    }

    protected GameState getGameState() {
        return (GameState) mStateContainer
                .getState().get(AppEventType.GAME_SESSION);
    }


}
