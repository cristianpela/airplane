package org.criskey.airplane.common.local.router;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.viewpresenter.BasePresenterTest;
import org.junit.Test;

import static org.criskey.airplane.common.local.router.Screen.*;
import static org.criskey.airplane.common.local.service.IAirplaneStompService.Status;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 31/1/2017.
 */

public class AbstractRouterTest extends BasePresenterTest {

    AbstractRouter<String> mRouter;

    RouterMiddleware mInternalMiddleware;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        GameMiddleware gameMiddleware = new GameMiddleware(mStateContainer);
        LoginMiddleware loginMiddleware = new LoginMiddleware(mStateContainer);
        gameMiddleware.setNextInChain(loginMiddleware);
        loginMiddleware.setPreviousInChain(gameMiddleware);

        mStateContainer.addDataBusListener(gameMiddleware);
        mStateContainer.addDataBusListener(loginMiddleware);

        mInternalMiddleware = mock(RouterMiddleware.class);
        mRouter = new TestRouter(mInternalMiddleware, gameMiddleware);
    }

    @Override
    public void tearDown() throws Exception {
        logout();
    }

    @Test
    public void test_bypass_routing() {
        assertEquals("Should bypass middlewares", SETTINGS, mRouter.show(SETTINGS));
    }

    @Test
    public void test_routing_when_not_logged() {
        assertEquals("Should show join screen when not logged", JOIN, mRouter.show(LOBBY));
        assertEquals("Should show join screen when not logged", JOIN, mRouter.show(GAME_SESSION_));
    }

    @Test
    public void test_routing_when_logged() {
        loginAndCreateLobby();
        assertEquals("Should show lobby when logged", LOBBY, mRouter.show(LOBBY));
    }

    @Test
    public void test_routing_when_in_game_setup() {
        loginAndCreateLobby();
        createSetupGameSession();
        assertEquals("Should show game setup when in game setup mode", GAME_SETUP, mRouter.show(GAME_HIT_HISTORY));
        assertEquals("Should show game setup when in game setup mode", GAME_SETUP, mRouter.show(GAME_SESSION_));
    }

    @Test
    public void test_routing_when_in_game_sessoin() {
        loginAndCreateLobby();
        createGameSession();
        assertEquals("Should show game screen", GAME_HIT_HISTORY, mRouter.show(GAME_HIT_HISTORY));
        assertEquals("Should show game screen", GAME_SESSION_, mRouter.show(GAME_SESSION_));
        assertEquals("Should not show game setup screen", GAME_SESSION_, mRouter.show(GAME_SETUP));
    }

    @Test
    public void test_routing_when_logged_and_not_in_game() {
        loginAndCreateLobby();
        assertEquals("Should show join screen when not logged", LOBBY, mRouter.show(GAME_SESSION_));
    }

    @Test
    public void test_routing_when_logout_event() {
        loginAndCreateLobby();
        mStateContainer.reset();
        mStateContainer.announce(new AppEvent(AppEventType.NETWORK_CONNECTION, Status.DISCONNECTED_FROM_APP));
        verify(mInternalMiddleware).onPropagateUp(JOIN, ScreenGroup.getNonBypassScreens());
    }

    @Test
    public void test_routing_when_game_session_lost_event() {
        loginAndCreateLobby();
        createGameSession();
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, null));
        verify(mInternalMiddleware).onPropagateUp(LOBBY, ScreenGroup.GAME_GROUP.getScreens());
    }

    @Test
    public void test_routing_when_create_game_session() {
        loginAndCreateLobby();
        createGameSession();
        verify(mInternalMiddleware).onPropagateUp(GAME_SESSION_, GAME_SETUP);
    }

    class TestRouter extends AbstractRouter<String> {

        public TestRouter(RouterMiddleware internalMiddleware, RouterMiddleware mMiddleware) {
            super(internalMiddleware, mMiddleware);
        }

        @Override
        public Screen show(Screen toScreen) {
            return showInternal(toScreen);
        }

        @Override
        public void hide(Screen screen) {}

        @Override
        public void addScreen(Screen screen, String screenContainer) {}

    }
}