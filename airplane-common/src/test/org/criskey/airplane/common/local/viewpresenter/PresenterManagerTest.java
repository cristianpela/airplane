package org.criskey.airplane.common.local.viewpresenter;

import com.google.gson.Gson;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.router.Router;
import org.criskey.airplane.common.local.router.Screen;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.util.Position;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by criskey on 19/10/2016.
 */
public class PresenterManagerTest {

    static class DummyRouter implements Router<Void> {

        @Override
        public Screen show(Screen to) {
            return null;
        }

        @Override
        public void hide(Screen screen) {

        }

        @Override
        public void addScreen(Screen screen, Void screenContainer) {

        }
    }

    static class DummyView implements IView {

        @Override
        public void onResume() {

        }

        @Override
        public void onPause() {

        }

        @Override
        public void showMessage(String message, boolean pop) {

        }

        @Override
        public void showError(String error, boolean pop) {

        }

        @Override
        public void restore(Map<String, ?> dataMap) {

        }
    }

    static class DummyPresenter extends Presentable {

        private DummyView view;

        static int creationCount = 0;

        public DummyPresenter(ArgsBatch batch) {
            super(batch);
            creationCount ++;
        }

        @Override
        public void onEvent(AppEvent event) {

        }

        @Override
        public void onAttach(IView view) {
            this.view = (DummyView) view;
        }

        @Override
        public void onDetach() {
           view = null;
        }
    }

    static class DummyService implements IAirplaneStompService {

        @Override
        public void changeHost(String host) {

        }

        @Override
        public String getHost() {
            return null;
        }

        @Override
        public String addSubscription(String destination, ListenerSubscription listener) {

            return destination;
        }

        @Override
        public void removeSubscription(String subscriptionId) {

        }

        @Override
        public void connect(String username, String password, AsyncCallback<String> asyncCallback) {
            System.out.println("Connecting!");
        }

        @Override
        public void joinLobby() {

        }

        @Override
        public void disconnect() {

        }

        @Override
        public void sendInvitation(String username) {

        }

        @Override
        public void acceptInvite(String username, boolean b) {

        }

        @Override
        public void attemptHit(String gameKey, String otherPlayerId, Position position) {

        }

        @Override
        public String submitConfiguration(String gameKey, String otherPlayerId, PlaneConfiguration configuration) {
            return null;
        }

        @Override
        public void leaveSession(String gameKey, String otherPlayerId) {

        }
    }

    static class DummyStorage implements Storage {

        @Override
        public <F> void save(Prefix prefix, String fieldKey, F field) {

        }

        @Override
        public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
            asyncCallback.onReturn(null, new Error("No implementation for this platform"));
        }

        @Override
        public void wipe(Prefix prefix) {

        }

        @Override
        public void commit(Prefix prefix) {

        }

    }

    static class DummyOnUIRunCallback implements OnUIRunCallback {

        @Override
        public void runOnUI(OnUICodeInjector injector) {

        }

        @Override
        public boolean isUIThread() {
            return false;
        }
    }

    static class DummyLangSupport extends LanguageSupport{

        @Override
        public String i18n(String resourceKey, Object... p) {
            return "NULL";
        }

        @Override
        public void onLocaleChanged() {

        }
    }

    @Test
    public void reflectionTest() {
        OnUIRunCallback uiRunCallback = new DummyOnUIRunCallback();
        PresenterManager.createInstance(
                new DummyService(),
                new DummyRouter(),
                uiRunCallback,
                new Gson(),
                null,
                new DummyStorage(), new DummyLangSupport());
        DummyPresenter presenter = PresenterManager.plugReflect(DummyPresenter.class);
        presenter.onAttach(new DummyView());
        assertTrue(presenter.mService != null);
        assertTrue(presenter.view != null);
        presenter.mService.connect("foo", "bar",null);

        assertTrue(presenter.creationCount == 1);
        System.gc();
        PresenterManager.plugReflect(DummyPresenter.class);
        presenter.onAttach(new DummyView());
        assertTrue(presenter.creationCount == 1); // asure the mPresenter is cached and created only once
    }

}