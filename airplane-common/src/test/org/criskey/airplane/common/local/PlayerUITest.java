package org.criskey.airplane.common.local;

import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.platform.InteractionEvent;
import org.criskey.airplane.common.local.platform.PlayerUI;
import org.criskey.airplane.common.util.Position;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 4/10/2016.
 */
public class PlayerUITest {


    @Test
    public void testGameTurning() {
        PlayerUI playerUI = new PlayerUI(Position.asDimension(1,1), PlayerUI.Mode.GAME);
        assertTrue("Is not blocked", !playerUI.isBlocked());
        playerUI.onInteraction(new InteractionEvent(Position.unit(10), InteractionEvent.Type.CHECK));
        assertTrue("Must be blocked and wait for confirmation", playerUI.isBlocked());
        playerUI.mark(new Shot(Position.unit(1),TargetProtocol.HIT), true, false);
        assertTrue("Must be blocked", playerUI.isBlocked());
        playerUI.block(false);
        assertTrue("Is not blocked", !playerUI.isBlocked());
    }

}