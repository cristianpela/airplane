package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.Shot;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.criskey.airplane.common.local.game.GameHitHistoryPresenter;
import org.criskey.airplane.common.local.game.GameHitHistoryView;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 31/1/2017.
 */
public class GameHitHistoryPresenterTest extends BasePresenterTest {

    GameHitHistoryPresenter mPresenter;

    GameHitHistoryView mView;

    Shot[] mMockOppAttempts = new Shot[]{
            new Shot(randomPosition(), TargetProtocol.MISS),
            new Shot(randomPosition(), TargetProtocol.HIT),
            new Shot(randomPosition(), TargetProtocol.LOSER),
    };

    int mCurrentMockAttempt = -1;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mPresenter = new GameHitHistoryPresenter(newArgsBatch());
        mStateContainer.addDataBusListener(mPresenter);
        mPresenter.onAttach(mView = mock(GameHitHistoryView.class));
        loginAndCreateLobby();
        whenLanguage();
        createGameSession();
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        mCurrentMockAttempt = -1;
    }

    @Test
    public void test_hit_history() {

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_START, GameNetworkProtocol.PHASE_WAIT_TURN)
                )));
        assertEquals("Should not have the turn", false, getGameState().gameSessionState.hasTurn);

        //first round
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_TAKE_TURN,
                                Shot.pack(mMockOppAttempts[++mCurrentMockAttempt])
                        )
                )));
        verify(mView, atLeastOnce()).incomingBlow(mShotArgCaptor.capture());
        assertEquals("Shot must match", mMockOppAttempts[mCurrentMockAttempt], mShotArgCaptor.getValue());
        assertEquals("Should have the turn", true, getGameState().gameSessionState.hasTurn);

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(myTurnShot())));
        assertEquals("Should not have the turn", false, getGameState().gameSessionState.hasTurn);

        //second round
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_TAKE_TURN,
                                Shot.pack(mMockOppAttempts[++mCurrentMockAttempt])
                        )
                )));
        verify(mView, atLeastOnce()).incomingBlow(mShotArgCaptor.capture());
        assertEquals("Shot must match", mMockOppAttempts[mCurrentMockAttempt], mShotArgCaptor.getValue());
        assertEquals("Should have the turn", true, getGameState().gameSessionState.hasTurn);

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(myTurnShot())));
        assertEquals("Should not have the turn", false, getGameState().gameSessionState.hasTurn);

        //reattach the view - state should be preserved
        mPresenter.onAttach(mView);
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        Map<String, Object> restoreMap = mRestoreArgCaptor.getValue();
        assertEquals("Should opponent shots be size 2", 2, ((List<Shot>) restoreMap.get(GameHitHistoryPresenter.KEY_LIST_HIT_HISTORY)).size());

        //third round should mark a game lose
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_END,
                                mMockOppAttempts[++mCurrentMockAttempt]
                                        .targetProtocol
                                        .ordinal())
                )
        ));
        assertEquals("Should be in phase END", GameNetworkProtocol.PHASE_END, getGameState().phase);
    }

    private GameNetworkProtocol myTurnShot() {
        return new GameNetworkProtocol(
                GAME_KEY,
                OTHER_PLAYER_ID,
                GameNetworkProtocol.PHASE_WAIT_TURN,
                Shot.pack(new Shot(randomPosition(), TargetProtocol.MISS)));
    }


}