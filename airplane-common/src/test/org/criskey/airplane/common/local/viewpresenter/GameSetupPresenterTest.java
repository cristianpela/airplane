package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.criskey.airplane.common.local.game.GameSetupPresenter;
import org.criskey.airplane.common.local.game.GameSetupView;
import org.criskey.airplane.common.local.game.GameState;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Map;

import static org.criskey.airplane.common.local.game.GameSetupPresenter.KEY_PLANE_CONFIGURATION;
import static org.criskey.airplane.common.local.game.GameSetupPresenter.KEY_STRING_STATUS_MESSAGE;
import static org.criskey.airplane.common.local.i18n.LanguageSupport.SCREEN_GAME_SETUP_LBL_OTHER_READY;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 30/1/2017.
 */
public class GameSetupPresenterTest extends BasePresenterTest {

    GameSetupPresenter mPresenter;

    GameSetupView mView;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mPresenter = new GameSetupPresenter(newArgsBatch());
        mStateContainer.addDataBusListener(mPresenter);
        mView = mock(GameSetupView.class);
        mPresenter.onAttach(mView);
        loginAndCreateLobby();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                GameNetworkProtocol protocol = new GameNetworkProtocol(GAME_KEY, OTHER_PLAYER_ID, GameNetworkProtocol.PHASE_PRINCIPAL_READY,
                        PlaneConfiguration.pack(PLANE_CONFIG));
                mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, Reducers.gameSession(protocol)));
                return null;
            }
        }).when(mService).submitConfiguration(anyString(), anyString(), any(PlaneConfiguration.class));
        doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                return invocationOnMock.getArgument(0);
            }
        }).when(mLang).i18n(anyString());
    }

    @Test
    public void test_creating_game_setup() {

        GameNetworkProtocol gameNetworkProtocol = GameNetworkProtocol.createEmpty(
                GAME_KEY,
                OTHER_PLAYER_ID,
                GameNetworkProtocol.PHASE_PREPARE);

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(gameNetworkProtocol)));

        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        Map<String, Object> restoreMap = mRestoreArgCaptor.getValue();
        assertEquals("Should be no plane config", null, restoreMap.get(KEY_PLANE_CONFIGURATION));
        assertEquals("Should be no submit config yet", false, restoreMap.get(GameSetupPresenter.KEY_BOOLEAN_IS_CONFIG_SUBMIT));
        assertEquals("Should be no status message", "", restoreMap.get(KEY_STRING_STATUS_MESSAGE));

        GameState gameState = (GameState) mStateContainer.getState().get(AppEventType.GAME_SESSION);
        assertEquals("Game key must match", GAME_KEY, gameState.gameKey);
        assertEquals("Other player id must match", OTHER_PLAYER_ID, gameState.otherPlayerId);

        gameNetworkProtocol = GameNetworkProtocol.createEmpty(GAME_KEY, OTHER_PLAYER_ID, GameNetworkProtocol.PHASE_OTHER_READY);
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(gameNetworkProtocol)));

        ArgumentCaptor<String> messageArgCaptor = ArgumentCaptor.forClass(String.class);
        verify(mView, atLeastOnce()).showMessage(messageArgCaptor.capture(), anyBoolean());
        assertEquals("Should show other player ready message", SCREEN_GAME_SETUP_LBL_OTHER_READY, messageArgCaptor.getValue());

        mPresenter.setConfiguration(PLANE_CONFIG);
        mPresenter.onDetach();
        gameState = (GameState) mStateContainer.getState().get(AppEventType.GAME_SESSION);
        assertEquals("Plane config should match", PLANE_CONFIG, gameState.gameSetupState.planeConfiguration);

        mPresenter.onAttach(mView);
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        restoreMap = mRestoreArgCaptor.getValue();
        assertEquals("Should be a plane config", PLANE_CONFIG, restoreMap.get(KEY_PLANE_CONFIGURATION));
        assertEquals("Should be no submit config yet", false, restoreMap.get(GameSetupPresenter.KEY_BOOLEAN_IS_CONFIG_SUBMIT));
        assertEquals("Should other player is ready message", SCREEN_GAME_SETUP_LBL_OTHER_READY, restoreMap.get(KEY_STRING_STATUS_MESSAGE));

        mPresenter.setConfiguration(PLANE_CONFIG);

        mPresenter.submitConfiguration();
        gameState = (GameState) mStateContainer.getState().get(AppEventType.GAME_SESSION);
        assertEquals("Should be configuration submitted", true, gameState.gameSetupState.isConfigurationSubmitted);
        assertEquals("Should be a plane config", PLANE_CONFIG, gameState.gameSetupState.planeConfiguration);
    }

}