package org.criskey.airplane.common.local.bus;

import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.service.IAirplaneStompService.Status;
import org.criskey.airplane.common.local.service.websocket.Stomp;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * Created by criskey on 27/1/2017.
 */
public class DataBusListenerWSNetworkTest {


    @Test
    public void test_networkstatus(){
        OnUIRunCallback uiCb = mock(OnUIRunCallback.class);
        when(uiCb.isUIThread()).thenReturn(true);
        DataBus dataBus = new DataBus(uiCb);
        DataBusListenerWSNetwork listenerWSNetwork = new DataBusListenerWSNetwork(new StateContainer(dataBus));

        DataBusListener otherListener = mock(DataBusListener.class);
        when(otherListener.getAppEventsToListen()).thenReturn(new AppEventType[]{AppEventType.NETWORK_CONNECTION});
        dataBus.addListener(otherListener);


        listenerWSNetwork.onState(Stomp.DISCONNECTED_FROM_OTHER);
        verify(otherListener).onEvent(new AppEvent(AppEventType.NETWORK_CONNECTION, Status.DISCONNECTED_FROM_OTHER));

    }

}