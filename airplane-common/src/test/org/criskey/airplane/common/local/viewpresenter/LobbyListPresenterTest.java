package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.TargetProtocol;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.LobbyListPresenter;
import org.criskey.airplane.common.local.LobbyListView;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 30/1/2017.
 */
public class LobbyListPresenterTest extends BasePresenterTest {

    LobbyListPresenter mPresenter;

    private LobbyListView mView;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mPresenter = new LobbyListPresenter(newArgsBatch());
        mStateContainer.addDataBusListener(mPresenter);
        mView = mock(LobbyListView.class);
        mPresenter.onAttach(mView);
        loginAndCreateLobby();
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.just(new LobbyListPresenter.LobbyState(false, mUsers))));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        reset(mView);
        mStateContainer.removeDataBusListener(mPresenter);
    }

    @Test
    public void _1_lobby_users() {
        
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(2, getCapturedRestoreLobby(mRestoreArgCaptor).size());

        //try to add the same user -> should not add to lobby list
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyNewUser(mUsers.get(0))));
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(2, getCapturedRestoreLobby(mRestoreArgCaptor).size());

        //try add the principal -> should not add to lobby list
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyNewUser(new UserInfo("foo", "id_foo", false))));
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(2, getCapturedRestoreLobby(mRestoreArgCaptor).size());

        //change a user
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyChangeUser(new UserInfo("abc", "id_abc", true))));
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(true, getCapturedRestoreLobby(mRestoreArgCaptor).get(0).isPlaying);

        //add new user
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyNewUser(new UserInfo("def", "id_def", false))));
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(3, getCapturedRestoreLobby(mRestoreArgCaptor).size());

        //remove a user
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyRemoveUser(new UserInfo("def", "id_def", false))));
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals(2, getCapturedRestoreLobby(mRestoreArgCaptor).size());

    }

    @Test
    public void _2_should_throw_when_lobby_is_not_created() {
        mStateContainer.dispatchState(new AppEvent(AppEventType.LOBBY,
                Reducers.lobbyNewUser(new UserInfo("bad", "id", false))));
    }

    @Test
    public void _3_lobby_disabling_when_game_session_is_running() {
     
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        GameNetworkProtocol.createEmpty(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_PREPARE)
                )));

        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals("Lobby should be disabled", true, getCapturedDisabledLobby(mRestoreArgCaptor));

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        GameNetworkProtocol.createEmpty(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_SESSION_LOST)
                )));

        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals("Lobby should be disabled", false, getCapturedDisabledLobby(mRestoreArgCaptor));

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                GAME_KEY,
                                OTHER_PLAYER_ID,
                                GameNetworkProtocol.PHASE_END,
                                TargetProtocol.WINNER.ordinal()
                        ))));

        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        assertEquals("Lobby should be disabled", false, getCapturedDisabledLobby(mRestoreArgCaptor));
    }

    private List<UserInfo> getCapturedRestoreLobby(ArgumentCaptor<Map<String, Object>> restoreArgCaptor) {
        return (List<UserInfo>) restoreArgCaptor.getValue().get(LobbyListPresenter.KEY_LIST_LOBBY);
    }

    private Boolean getCapturedDisabledLobby(ArgumentCaptor<Map<String, Object>> restoreArgCaptor) {
        return (Boolean) restoreArgCaptor.getValue().get(LobbyListPresenter.KEY_BOOLEAN_ENTRIES_DISABLED);
    }

}