package org.criskey.airplane.common.local;

import com.google.gson.Gson;
import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.PlaneConfiguration;
import org.criskey.airplane.common.local.service.IAirplaneStompService;
import org.criskey.airplane.common.local.service.websocket.ListenerSubscription;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.util.Position;
import org.junit.Test;

/**
 * Created by criskey on 28/9/2016.
 */
public class GamePresenterTest {

   private static class MockService implements IAirplaneStompService {

        Gson gson;

        public MockService(Gson gson) {
            this.gson = gson;
        }

        @Override
        public void changeHost(String host) {

        }

        @Override
        public String getHost() {
             return null;
        }

        @Override
        public String addSubscription(String destination, ListenerSubscription listener) {

             return destination;
        }

        @Override
        public void removeSubscription(String subscriptionId) {

        }

        @Override
        public void connect(String username, String password, AsyncCallback<String> asyncCallback) {

        }

        @Override
        public void joinLobby() {

        }

        @Override
        public void disconnect() {

        }



        @Override
        public void sendInvitation(String username) {

        }

        @Override
        public void acceptInvite(String username, boolean b) {

        }

        @Override
        public void attemptHit(String gameKey, String otherPlayerId, Position position) {

        }

        @Override
        public String submitConfiguration(String gameKey, String otherPlayerId, PlaneConfiguration configuration) {
            GameNetworkProtocol protocol = new GameNetworkProtocol(gameKey, otherPlayerId,
                    GameNetworkProtocol.PHASE_CONFIG, PlaneConfiguration.pack(configuration));
            return gson.toJson(protocol);
        }

        @Override
        public void leaveSession(String gameKey, String otherPlayerId) {

        }
    }

//    private static class MockGameView implements  GameView{
//
//        @Override
//        public void onResume() {
//
//        }
//
//        @Override
//        public void onPause() {
//
//        }
//
//        @Override
//        public void showMessage(String message, boolean pop) {
//
//        }
//
//        @Override
//        public void showError(String error, boolean pop) {
//
//        }
//
//        @Override
//        public void responseAttempt(Shot shot) {
//
//        }
//
//        @Override
//        public void incomingBlow(Shot shot) {
//
//        }
//
//        @Override
//        public void startSetupGame() {
//
//        }
//
//        @Override
//        public void startGame(boolean hasTurn) {
//
//        }
//
//        @Override
//        public void resetGame() {
//
//        }
   // }

    @Test
    public void testingConfigJsonParse(){
//        Gson mGson = new Gson();
//        PresenterManager.createInstance(new MockService(mGson), null, null, mGson, null, null);
//        GamePresenter presenter = PresenterManager.plugReflect(GamePresenter.class);
//        presenter.onAttach(new MockGameView());
//        String jsonProtocol = presenter.submitConfiguration(new PlaneConfiguration(new Position(5, 5), Orientation.WEST));
//
//        GameNetworkProtocol protocol = mGson.fromJson(jsonProtocol, GameNetworkProtocol.class);
//
//        assertEquals("Should be phase config", GameNetworkProtocol.PHASE_CONFIG, protocol.getPhase());
//        PlaneConfiguration configuration = PlaneConfiguration.unpack(protocol.getData());
//        assertEquals("Should be position (5,5)", new Position(5,5), configuration.origin);
//        assertEquals("Should be orientation WEST", Orientation.WEST, configuration.orientation);
//
//        System.out.println(jsonProtocol);
    }
}