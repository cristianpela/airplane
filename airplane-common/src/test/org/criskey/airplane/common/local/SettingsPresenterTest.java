package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.i18n.LanguageSupport;
import org.criskey.airplane.common.local.persistance.Prefix;
import org.criskey.airplane.common.local.persistance.PrefixedStorageProxy;
import org.criskey.airplane.common.local.persistance.Storage;
import org.criskey.airplane.common.local.util.AsyncCallback;
import org.criskey.airplane.common.local.viewpresenter.PresenterManager;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by criskey on 25/11/2016.
 */
public class SettingsPresenterTest {

    static class SettingsStorageTest implements Storage {

        static String[] codes = {"en", "ro"};

        static String[] langs = {LanguageSupport.LANG_EN, LanguageSupport.LANG_RO};

        static String currLang = LanguageSupport.LANG_EN;

        static String server = "127.0.0.1:8080";

        @Override
        public <F> void save(Prefix prefix, String fieldKey, F field) {
            if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_NAME_KEY))
                currLang = (String) field;
            else if (fieldKey.equals(Storage.PRF_SERVER_ADDRESS))
                server = (String) field;
        }

        @Override
        public <F> void load(Prefix prefix, String fieldKey, AsyncCallback<F> asyncCallback) {
            if (fieldKey.equals(Storage.PRF_CURR_LANGUAGE_NAME_KEY)) {
                asyncCallback.onReturn((F) currLang, null);
            } else if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_CODES)) {
                asyncCallback.onReturn((F) codes, null);
            } else if (fieldKey.equals(Storage.PRF_AVAILABLE_LANGUAGE_NAME_KEYS)) {
                asyncCallback.onReturn((F) langs, null);
            } else if (fieldKey.equals(Storage.PRF_SERVER_ADDRESS)) {
                asyncCallback.onReturn((F) server, null);
            }
        }

        @Override
        public void wipe(Prefix prefix) {

        }

        @Override
        public void commit(Prefix prefix) {

        }
    }

    static class LanguageSupportTest extends LanguageSupport {

        @Override
        public String i18n(String resourceKey, Object... params) {
            if (resourceKey.equals(LanguageSupport.LANG_EN))
                return "english";
            else if (resourceKey.equals(LanguageSupport.LANG_RO))
                return "romanian";
            return null;
        }

        @Override
        public void onLocaleChanged() {
        }

        @Override
        public void setLocale(Locale locale) {
            assertEquals(new Locale("ro"), locale);
            super.setLocale(locale);
        }
    }

    static class SettingsViewTest implements SettingsView{

        @Override
        public void onCurrentLanguageChanged(String currentLanguage) {
            assertEquals("Curr mLang should be romanian",
                    "romanian", currentLanguage);
        }

        @Override
        public void onServerAddressChanged(String serverAddress) {
            assertEquals("Server address should be \"192.168.100.2:8080\"",
                    "192.168.100.2:8080", serverAddress);
        }

        @Override
        public void onResume() {

        }

        @Override
        public void onPause() {

        }

        @Override
        public void showMessage(String message, boolean pop) {

        }

        @Override
        public void showError(String error, boolean pop) {

        }

        boolean justTestFirstPass = false;

        @Override
        public void restore(Map<String, ?> dataMap) {
            if(!justTestFirstPass) {
                assertArrayEquals("Languages should be eq",
                        new String[]{"english", "romanian"},
                        (String[]) dataMap.get(SettingsPresenter.KEY_STRING_ARR_AVAILABLE_LANGUAGES));
                assertEquals("Server should be " + SettingsStorageTest.server,
                        SettingsStorageTest.server,
                        dataMap.get(SettingsPresenter.KEY_STRING_SERVER_ADDRESS));
                assertEquals("Curr mLang should be english",
                        "english", dataMap.get(SettingsPresenter.KEY_STRING_CURR_LANGUAGE));
                justTestFirstPass = true;
            }
        }
    }

    @Test
    public void testSettings() {
        PresenterManager.createInstance(null, null, null, null, null,
                new PrefixedStorageProxy(Prefix.PREF, new SettingsStorageTest()),
                new LanguageSupportTest());
        SettingsPresenter presenter = PresenterManager.plugReflect(SettingsPresenter.class);
        presenter.onAttach(new SettingsViewTest());
        final int romanianIndex = 1;
        presenter.setCurrentLanguageCodeIndex(romanianIndex);
        presenter.setServerAddress("192.168.100.2:8080");

        assertEquals("192.168.100.2:8080", SettingsStorageTest.server);
        assertEquals(LanguageSupport.LANG_RO, SettingsStorageTest.currLang);
    }

}