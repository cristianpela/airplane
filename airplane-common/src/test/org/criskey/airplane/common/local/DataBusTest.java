package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.DataBus;
import org.criskey.airplane.common.local.bus.DataBusListener;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;
import org.criskey.airplane.common.local.viewpresenter.IView;
import org.criskey.airplane.common.local.viewpresenter.Presentable;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by criskey on 13/10/2016.
 */
public class DataBusTest {

    private OnUIRunCallback uicb = new MockOnUIRunCallback();


    @Test
    public void testBus() {

        DataBus bus = new DataBus(uicb);

        Presentable p = new Presentable() {
            @Override
            public void onAttach(IView view) {

            }

            @Override
            public void onDetach() {

            }

            @Override
            public void onEvent(AppEvent event) {

            }
        };

        AppEventType[] eventsToListen = {
                AppEventType.LOGGED
        };
        p.setAppEventsToListen(eventsToListen);
        bus.addListener(p);

        Map<AppEventType, Set<DataBusListener>> listeners = bus.getListeners();
        Set<DataBusListener> logingroup = listeners.get(AppEventType.LOGGED);
        assertTrue(logingroup != null);
        assertTrue("Must have one presenter in logged in group", logingroup.contains(p));

        bus.addListener(p);

        bus.removeListener(p);

        assertTrue("There are no listeners", bus.getListeners().get(AppEventType.LOGGED).isEmpty());

    }

}