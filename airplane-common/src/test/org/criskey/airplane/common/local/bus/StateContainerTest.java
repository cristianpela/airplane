package org.criskey.airplane.common.local.bus;

import org.criskey.airplane.common.local.MockOnUIRunCallback;
import org.criskey.airplane.common.log.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by criskey on 24/1/2017.
 */
public class StateContainerTest {

    private static class AuthState {
        final String userName;
        final boolean loggedIn;

        public AuthState(String userName, boolean loggedIn) {
            this.userName = userName;
            this.loggedIn = loggedIn;
        }

        @Override
        public String toString() {
            return "AuthState{" +
                    "userName='" + userName + '\'' +
                    ", loggedIn=" + loggedIn +
                    '}';
        }
    }

    int stateStep;

    @Before
    public void before() {
        stateStep = 0;
    }

    @Test
    public void dispatch() throws Exception {
        StateContainer stateContainer = new StateContainer(new DataBus(new MockOnUIRunCallback()), new StateObserver() {

            @Override
            public void observe(Map<AppEventType, ?> oldState, Map<AppEventType, ?> newState) {
                Logger.out.printd(this, ": State -> Old:" + oldState + " New: " + newState);
            }
        });
        final DataBusListener listener = new LoggInDataBusListener();
        stateContainer.addDataBusListener(listener);
        stateContainer.dispatchState(new AppEvent(AppEventType.LOGGED, new AuthState("foo", false)));
        stateContainer.dispatchState(new AppEvent(AppEventType.LOGGED, new AuthState("bar", true)));
    }

    @Test
    public void getState() throws Exception {

    }

    private class LoggInDataBusListener implements DataBusListener {

        AuthState prevAuthState;

        @Override
        public void onEvent(AppEvent event) {
            Map<AppEventType, Object> state = (Map<AppEventType, Object>) event.data;
            assertTrue(state != null);
            AuthState authState = (AuthState) state.get(event.type);
            switch (event.type) {
                case LOGGED: {
                    if (stateStep == 0) {
                        assertEquals("Should be foo ", "foo", authState.userName);
                        assertFalse(authState.loggedIn);
                    }
                    if (stateStep == 1) {
                        assertEquals("Should be bar ", "bar", authState.userName);
                        assertTrue(authState.loggedIn);
                        assertNotEquals(authState, prevAuthState);
                    }
                    break;
                }
            }
            prevAuthState = authState;
            stateStep++;
        }

        @Override
        public AppEventType[] getAppEventsToListen() {
            return new AppEventType[]{
                    AppEventType.LOGGED
            };
        }
    }
}