package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.GameNetworkProtocol;
import org.criskey.airplane.common.UserInfo;
import org.criskey.airplane.common.local.InvitationListPresenter;
import org.criskey.airplane.common.local.InvitationListView;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 30/1/2017.
 */
public class InvitationListPresenterTest extends BasePresenterTest {

    InvitationListView mInvitationListView;

    InvitationListPresenter mPresenter;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        mPresenter = new InvitationListPresenter(newArgsBatch());
        mStateContainer.addDataBusListener(mPresenter);
        mPresenter.onAttach(mInvitationListView = mock(InvitationListView.class));

        loginAndCreateLobby();

    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        reset(mInvitationListView);
        mStateContainer.removeDataBusListener(mPresenter);
    }

    @Test
    public void test_adding_invitation() {

        ArgumentCaptor<Map<String, Object>> restoreArgCaptor = ArgumentCaptor.forClass(Map.class);

        UserInfo invitationSrc = mUsers.get(0);
        mStateContainer.dispatchState(new AppEvent(AppEventType.USER_INVITATION,
                Reducers.invitation(invitationSrc.username, true)
        ));

        verify(mInvitationListView).restore(restoreArgCaptor.capture());
        assertEquals(1, getCapturedRestoreInv(restoreArgCaptor).size());
        assertEquals("abc", getCapturedRestoreInv(restoreArgCaptor).get(0));
    }


    @Test
    public void test_disable_invitation() {
        ArgumentCaptor<Map<String, Object>> restoreArgCaptor = ArgumentCaptor.forClass(Map.class);
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(
                        new GameNetworkProtocol(
                                "game_key",
                                "other_player_id",
                                GameNetworkProtocol.PHASE_PREPARE,
                                -1)
                )));
        verify(mInvitationListView, atLeastOnce()).restore(restoreArgCaptor.capture());
        assertEquals("Invitations should be disabled", true, getCapturedDisabledLobby(restoreArgCaptor));
    }

    private Boolean getCapturedDisabledLobby(ArgumentCaptor<Map<String, Object>> restoreArgCaptor) {
        return (Boolean) restoreArgCaptor.getValue().get(InvitationListPresenter.KEY_BOOLEAN_ENTRIES_DISABLED);
    }

    private List<String> getCapturedRestoreInv(ArgumentCaptor<Map<String, Object>> restoreArgCaptor) {
        return (List<String>) restoreArgCaptor.getValue().get(InvitationListPresenter.KEY_LIST_INVITATIONS);
    }

}