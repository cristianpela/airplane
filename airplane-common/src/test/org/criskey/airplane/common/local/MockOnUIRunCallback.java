package org.criskey.airplane.common.local;

import org.criskey.airplane.common.local.platform.OnUICodeInjector;
import org.criskey.airplane.common.local.platform.OnUIRunCallback;

/**
 * Created by criskey on 24/1/2017.
 */
public final class MockOnUIRunCallback implements OnUIRunCallback {
    @Override
    public void runOnUI(OnUICodeInjector injector) {
        injector.inject();
    }

    @Override
    public boolean isUIThread() {
        return false;
    }
}
