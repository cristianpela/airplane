package org.criskey.airplane.common.local.viewpresenter;

import org.criskey.airplane.common.*;
import org.criskey.airplane.common.local.bus.AppEvent;
import org.criskey.airplane.common.local.bus.AppEventType;
import org.criskey.airplane.common.local.bus.Reducers;
import org.criskey.airplane.common.local.game.GameSessionPresenter;
import org.criskey.airplane.common.local.game.GameSessionView;
import org.criskey.airplane.common.local.game.GameState;
import org.criskey.airplane.common.util.Position;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.criskey.airplane.common.local.game.GameSessionPresenter.*;
import static org.criskey.airplane.common.local.i18n.LanguageSupport.SCREEN_GAME_SESSION_TAKE_TURN;
import static org.criskey.airplane.common.local.i18n.LanguageSupport.SCREEN_GAME_SESSION_WINNER;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 31/1/2017.
 */
public class GameSessionPresenterTest extends BasePresenterTest {

    GameSessionPresenter mPresenter;

    GameSessionView mView;

    Shot[] mMockAttempts = new Shot[]{
            new Shot(randomPosition(), TargetProtocol.MISS),
            new Shot(randomPosition(), TargetProtocol.HIT),
            new Shot(randomPosition(), TargetProtocol.WINNER),
    };

    int mCurrentMockAttempt = -1;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mPresenter = new GameSessionPresenter(newArgsBatch());
        mStateContainer.addDataBusListener(mPresenter);
        mPresenter.onAttach(mView = mock(GameSessionView.class));
        mPresenter.enableTurnCountDownTimer(false);
        loginAndCreateLobby();
        whenLanguage();
        createGameSession();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                Shot shot = mMockAttempts[++mCurrentMockAttempt];
                GameNetworkProtocol protocol;
                if (shot.targetProtocol != TargetProtocol.WINNER) {
                    protocol = new GameNetworkProtocol(
                            GAME_KEY,
                            OTHER_PLAYER_ID,
                            GameNetworkProtocol.PHASE_WAIT_TURN,
                            Shot.pack(shot));
                } else {
                    protocol = new GameNetworkProtocol(
                            GAME_KEY,
                            OTHER_PLAYER_ID,
                            GameNetworkProtocol.PHASE_END,
                            shot.targetProtocol.ordinal());
                }
                mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                        Reducers.gameSession(
                                protocol)));
                return null;
            }
        }).when(mService).attemptHit(anyString(), anyString(), any(Position.class));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        mCurrentMockAttempt = -1;
    }

    @Test
    public void test_game_session() {

        GameNetworkProtocol protocol = new GameNetworkProtocol(
                GAME_KEY,
                OTHER_PLAYER_ID,
                GameNetworkProtocol.PHASE_START, GameNetworkProtocol.PHASE_TAKE_TURN);
        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION,
                Reducers.gameSession(protocol)));

        verify(mView, atLeastOnce()).takeTurn(mTurnArgCaptor.capture());
        assertEquals("Should have turn", true, mTurnArgCaptor.getValue());
        verify(mView, atLeastOnce()).showMessage(mMsgArgCaptor.capture(), anyBoolean());
        assertEquals("Should show take turn message", SCREEN_GAME_SESSION_TAKE_TURN, mMsgArgCaptor.getValue());

        //first round
        mPresenter.attemptHit(randomPosition());
        verify(mView, atLeastOnce()).takeTurn(mTurnArgCaptor.capture());
        assertEquals("Should not have turn", false, mTurnArgCaptor.getValue());
        verify(mView, atLeastOnce()).responseAttempt(mShotArgCaptor.capture());
        assertEquals("Shot must match", mMockAttempts[mCurrentMockAttempt], mShotArgCaptor.getValue());

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, Reducers.gameSession(opponentTurnShot())));
        verify(mView, atLeastOnce()).takeTurn(mTurnArgCaptor.capture());
        assertEquals("Should have turn", true, mTurnArgCaptor.getValue());
        assertEquals("Should be 1 opponent size shots", 1, getGameState().gameSessionState.opponentAttempts.size());

        //second round
        mPresenter.attemptHit(randomPosition());
        verify(mView, atLeastOnce()).takeTurn(mTurnArgCaptor.capture());
        assertEquals("Should not have turn", false, mTurnArgCaptor.getValue());
        verify(mView, atLeastOnce()).responseAttempt(mShotArgCaptor.capture());
        assertEquals("Shot must match", mMockAttempts[mCurrentMockAttempt], mShotArgCaptor.getValue());

        mStateContainer.dispatchState(new AppEvent(AppEventType.GAME_SESSION, Reducers.gameSession(opponentTurnShot())));
        verify(mView, atLeastOnce()).takeTurn(mTurnArgCaptor.capture());
        assertEquals("Should have turn", true, mTurnArgCaptor.getValue());
        assertEquals("Should be 2 opponent size shots", 2, getGameState().gameSessionState.opponentAttempts.size());

        //meantime lets reattach view to presenter; the state should be preserved
        mPresenter.onAttach(mView);
        verify(mView, atLeastOnce()).restore(mRestoreArgCaptor.capture());
        Map<String, Object> restoreMap = mRestoreArgCaptor.getValue();
        assertEquals("Should have turn", true, restoreMap.get(KEY_BOOLEAN_HAS_TURN));
        assertEquals("Should have take turn status message", SCREEN_GAME_SESSION_TAKE_TURN, restoreMap.get(KEY_STRING_STATUS_MESSAGE));
        assertEquals("Should attempts list be size 2", 2, ((List<Shot>) restoreMap.get(KEY_LIST_ATTEMPTS)).size());

        //third round = must be WINNER
        mPresenter.attemptHit(randomPosition());
        verify(mView, atLeastOnce()).showMessage(mMsgArgCaptor.capture(), anyBoolean());
        assertEquals("Should show winning message", SCREEN_GAME_SESSION_WINNER, mMsgArgCaptor.getValue());
    }



    private GameNetworkProtocol opponentTurnShot() {
        return new GameNetworkProtocol(
                GAME_KEY,
                OTHER_PLAYER_ID,
                GameNetworkProtocol.PHASE_TAKE_TURN,
                Shot.pack(new Shot(randomPosition(), TargetProtocol.MISS)));
    }
}