package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Created by criskey on 30/8/2016.
 */
public class GameManagerTest {

    GameManager gameManager = GameManager.getInstance();

    String userAdam = "Adam";
    String userEve = "Eve";
    String sessionKey;


    @Test
    public void startGame() throws Exception {
        sessionKey = gameManager.createGame(userAdam, userEve);
        boolean ready = gameManager.startGame(sessionKey, userAdam, new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
        assertFalse(ready);
        ready = gameManager.startGame(sessionKey, userEve, new PlaneConfiguration(0,new Position(5, 4), Orientation.EST));
        assertEquals("Session key doesn't match", userAdam + "-" + userEve, sessionKey);
        assertTrue(ready);

        ready = gameManager.startGame(sessionKey, userEve, new PlaneConfiguration(0, new Position(5, 4), Orientation.EST)); // will not allow onother config
        assertFalse(ready);


        Array2D<Void> targets = new Array2D<Void>(GameSettings.BOARD_SIZE);
        Iterator<Position> tgIterator = targets.iterator();

        //eve plays until wins
        TargetProtocol targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 4));// seed body
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(2, 4)); // wing
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(1, 4)); // wing
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(4, 4)); // wing
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(5, 4)); // wing
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 3)); // body
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 2)); // tail
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(2, 2)); // tail
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(4, 2)); // tail
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        gameManager.target(sessionKey, userEve, tgIterator.next());
        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 5)); // origin
        assertEquals("Should win the game", TargetProtocol.WINNER, targetProtocol);
        gameManager.reset(sessionKey); // start a new game

        // restart the game and change player order
        gameManager.startGame(sessionKey, userAdam, new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
        boolean isGameReady = gameManager.startGame(sessionKey, userEve, new PlaneConfiguration(0,new Position(5, 4), Orientation.EST));

        assertTrue(isGameReady);

        assertEquals("Session key should match", userAdam + "-" + userEve, sessionKey);

        targets = new Array2D<Void>(GameSettings.BOARD_SIZE);
        tgIterator = targets.iterator();

        //eve plays and stop before winning
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(2, 4)); // wing
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(1, 4)); // wing
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(4, 4)); // wing
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(5, 4)); // wing
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 3)); // body
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 2)); // tail
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(2, 2)); // tail
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 4));// seed origin
//        assertEquals("Should win", TargetProtocol.WINNER, targetProtocol);
//        gameManager.target(sessionKey, userEve, tgIterator.next());
//        targetProtocol = gameManager.target(sessionKey, userAdam, new Position(3, 5)); // origin
//        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);

    }

    @Test(expected = Exception.class)
    public void startGameExceptionDuringSession() throws Exception {
        gameManager.startGame(sessionKey, userAdam, new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
        gameManager.startGame(sessionKey, userEve, new PlaneConfiguration(0,new Position(5, 4), Orientation.EST));
    }

    @Test(expected = Exception.class)
    public void startGameExceptionWithoutCreateOne() throws Exception {
        gameManager.removeSession(sessionKey);
        gameManager.startGame(sessionKey, userAdam, new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
    }

}