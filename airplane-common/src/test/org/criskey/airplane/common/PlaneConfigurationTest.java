package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Position;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 28/9/2016.
 */
public class PlaneConfigurationTest {

    @Test
    public void packTest(){
        Position p = new Position(29, 39);
        Orientation o = Orientation.NORTH;
        int id = 2;

        int data = PlaneConfiguration.pack(new PlaneConfiguration(id, p, o));

        PlaneConfiguration pc = PlaneConfiguration.unpack(data);

        assertEquals(id, pc.typeId);
        assertEquals(p, pc.origin);
        assertEquals(o, pc.orientation);

        p = Position.NaP;

        data = PlaneConfiguration.pack(new PlaneConfiguration(id, p, o));
        pc = PlaneConfiguration.unpack(data);
        System.out.println(pc);

        assertEquals(id, pc.typeId);
        assertEquals(p, pc.origin);
        assertEquals(o, pc.orientation);
    }

}