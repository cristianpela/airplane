package org.criskey.airplane.common;

import org.criskey.airplane.common.util.Array2D;
import org.criskey.airplane.common.util.Position;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Created by criskey on 30/8/2016.
 */
public class GameSessionTest {

    private Player playerAdam;
    private Player playerEve;
    private GameSession gameSession;

    @Before
    public void setUp() throws Exception {
        playerAdam = new Player("Adam");
        playerEve = new Player("Ever");
        gameSession = new GameSession(playerAdam, playerEve);
    }

    @Test
    public void target() throws Exception {

        Array2D<Void> targets = new Array2D<Void>(GameSettings.BOARD_SIZE);
        Iterator<Position> tgIterator = targets.iterator();

        assertEquals("Other player should be Eve", playerEve, gameSession.otherPlayer(playerAdam));
        assertEquals("Other player should be Adam", playerAdam, gameSession.otherPlayer(playerEve));
        assertNull(gameSession.otherPlayer(new Player()));

        playerAdam.configBoard(new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));

        //eve plays until wins
        TargetProtocol targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(3, 4));// seed body
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(2, 4)); // wing
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(1, 4)); // wing
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(4, 4)); // wing
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(5, 4)); // wing
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(3, 3)); // body
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(3, 2)); // tail
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(2, 2)); // tail
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should onBoardChanged", TargetProtocol.HIT, targetProtocol);
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(4, 2)); // tail
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        targetProtocol = gameSession.target(playerAdam.getUsername(), new Position(3, 5)); // origin
        gameSession.target(playerEve.getUsername(), tgIterator.next());
        assertEquals("Should win the game", TargetProtocol.WINNER, targetProtocol);

    }

    @Test
    public void shouldBeReadyBeforeTargeting(){
        gameSession = new GameSession(null); // key doesnt matter in this test
        gameSession.addPlayer(playerAdam);
        gameSession.addPlayer(playerAdam);
        playerAdam.configBoard(new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
        playerEve.configBoard(new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));

        assertFalse("Game session should not be ready. must be add to players", gameSession.isReady());
        gameSession.addPlayer(playerEve);
        assertTrue("Game session should be ready", gameSession.isReady());

    }

    @Test(expected = Exception.class)
    public void shouldNotTakeTurnTwice() throws  Exception{
        gameSession = new GameSession(null); // key doesnt matter in this test
        gameSession.addPlayer(playerAdam);
        gameSession.addPlayer(playerEve);

        gameSession.target(playerAdam.getUsername(), new Position(0, 0));
        gameSession.target(playerAdam.getUsername(), new Position(0, 0));
    }

    @Test(expected = Exception.class)
    public void shouldNotSwitchTurnsUntilTheMoveIsCorrect() throws  Exception{
        gameSession = new GameSession(null); // key doesnt matter in this test
        gameSession.addPlayer(playerAdam);
        gameSession.addPlayer(playerEve);

        gameSession.target(playerAdam.getUsername(), new Position(3032,435435)); //  invalid position
        gameSession.target(playerEve.getUsername(), new Position(0, 0));

    }

    @Test
    public void shouldSwitchInCaseOfNAPPosition() throws  Exception{
        gameSession = new GameSession(null); // key doesnt matter in this test
        gameSession.addPlayer(playerAdam);
        gameSession.addPlayer(playerEve);

        gameSession.target(playerAdam.getUsername(), Position.NaP); //  not a position
        assertEquals("Next player should be adam", playerAdam, gameSession.getNextPlayerToMove());

    }

}