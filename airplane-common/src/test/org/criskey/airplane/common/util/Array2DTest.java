package org.criskey.airplane.common.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 31/8/2016.
 */
public class Array2DTest {
    Array2D<Integer> arr;


    @Before
    public void setUp() throws Exception {
        //creating a 2 x 2 array
        arr = new Array2D<Integer>(2, 0);
    }

    @Test
    public void ops() throws Exception {
        arr.set(new Position(1, 1), 100);
        Object[] narr = arr.nativeArray();
        assertTrue(narr.length == 4);
        assertEquals("index 3 [1,1] should be 100", new Integer(100), (Integer) narr[3]);
    }

    @Test
    public void setCornerCasesCol1() throws Exception {
        assertFalse(arr.set(Position.horizontal(2), 1)); // column 3 is out of bounds
    }

    @Test
    public void setCornerCasesCol2() throws Exception {
        assertFalse(arr.set(Position.horizontal(-1), 1)); // column 3 is out of bounds
    }


    @Test
    public void setCornerCasesRow1() throws Exception {
        assertFalse(arr.set(Position.vertical(-1), 1)); // column 3 is out of bounds
    }

    @Test
    public void setCornerCasesRow2() throws Exception {
        assertFalse(arr.set(Position.vertical(2), 1)); // column 3 is out of bounds
    }

    @Test
    public void testRotation() {
        Array2D<Integer> array2D = new Array2D<Integer>(10, 0);

        Position origin = new Position(3, 3);

        Position offset = origin.copy();
        int subSize = 4, count = 0;
        for (int i = 0; i < subSize; i++) {
            for (int j = 0; j < subSize; j++) {
                array2D.set(offset, count++);
                offset.horizontal(1);
            }
            offset.vertical(1).horizontal(-subSize);
        }

        assertEquals(new Integer(0), array2D.get(new Position(3, 3)));
        assertEquals(new Integer(1), array2D.get(new Position(3, 4)));
        assertEquals(new Integer(2), array2D.get(new Position(3, 5)));
        assertEquals(new Integer(3), array2D.get(new Position(3, 6)));
        assertEquals(new Integer(15), array2D.get(new Position(6, 6)));

        array2D.rotate(subSize, origin);

        assertEquals(new Integer(12), array2D.get(new Position(3, 3)));
        assertEquals(new Integer(8), array2D.get(new Position(3, 4)));
        assertEquals(new Integer(4), array2D.get(new Position(3, 5)));
        assertEquals(new Integer(0), array2D.get(new Position(3, 6)));

        assertEquals(new Integer(13), array2D.get(new Position(4, 3)));
        assertEquals(new Integer(9), array2D.get(new Position(4, 4)));
        assertEquals(new Integer(5), array2D.get(new Position(4, 5)));
        assertEquals(new Integer(1), array2D.get(new Position(4, 6)));

        assertEquals(new Integer(14), array2D.get(new Position(5, 3)));
        assertEquals(new Integer(10), array2D.get(new Position(5, 4)));
        assertEquals(new Integer(6), array2D.get(new Position(5, 5)));
        assertEquals(new Integer(2), array2D.get(new Position(5, 6)));

        assertEquals(new Integer(15), array2D.get(new Position(6, 3)));
        assertEquals(new Integer(11), array2D.get(new Position(6, 4)));
        assertEquals(new Integer(7), array2D.get(new Position(6, 5)));
        assertEquals(new Integer(3), array2D.get(new Position(6, 6)));
    }

    @Test
    public void translationTest() {
        int subSize = 5;
        Position origin = new Position(0, 0);
        Array2D<Integer> array2D = createArray(origin.copy(), subSize, 10);

        array2D.translate(origin.copy(), subSize, null, new Position(5, 5), 0);
        assertEquals(new Integer(1), array2D.get(new Position(3, 3)));
        assertEquals(new Integer(25), array2D.get(new Position(7, 7)));


        origin = new Position(5, 5);
        array2D = createArray(origin, subSize, 10);
        array2D.translate(origin.copy(), subSize, null, new Position(4, 5), 0);
        assertEquals(new Integer(1), array2D.get(new Position(2, 3)));

        origin = new Position(5, 5);
        array2D = createArray(origin, subSize, 10);
        assertFalse(array2D.translate(origin.copy(), subSize, null, new Position(1, 5), 0));
        //assertEquals(new Integer(1), array2D.get(new Position(2, 3)));
//
//
//        origin = new Position(0, 0);
//        array2D = createArray(origin, subSize, 10);
//        boolean translated =  array2D.translate(origin.copy(), subSize, new Position(0, 1));
//        assertFalse(translated);
    }

    private Array2D<Integer> createArray(Position subArrOrigin, int subSize, int size){
        Array2D<Integer> array2D = new Array2D<Integer>(size, 0);

        Position offset = subArrOrigin.copy();
        int count = 1;
        for (int i = 0; i < subSize; i++) {
            for (int j = 0; j < subSize; j++) {
                array2D.set(offset, count++);
                offset.horizontal(1);
            }
            offset.vertical(1).horizontal(-subSize);
        }
        return array2D;
    }

}