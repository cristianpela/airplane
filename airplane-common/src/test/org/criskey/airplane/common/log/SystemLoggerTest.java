package org.criskey.airplane.common.log;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 21/10/2016.
 */
public class SystemLoggerTest {

    class A{
    }

    class TestableLogger extends SystemLogger{
        @Override
        protected String getDateFormat() {
            return "2016-10-31 09:46:11";
        }
    }

    @Test
    public void testLog(){

        Logger.change(new TestableLogger());

        PrintListener printListener = new PrintListener() {
            @Override
            public void onPrint(String log, ILogger.Level level) {
                assertEquals("[2016-10-31 09:46:11 A]: Test", log.trim());
            }
        };

        A a = new A();
        Logger.out.addPrintListener(printListener);
        Logger.out.print(a, "Test");
    }

}