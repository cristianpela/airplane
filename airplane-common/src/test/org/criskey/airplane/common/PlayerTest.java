package org.criskey.airplane.common;

import org.criskey.airplane.common.local.InteractivePlayer;
import org.criskey.airplane.common.util.Position;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 30/8/2016.
 */
public class PlayerTest {

    Player player;

    @Before
    public void setUp() throws Exception {
        player = new Player();
        player.configBoard(new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));
    }

    @Test
    public void target() throws Exception {
        Position position = new Position(3, 4);
        TargetProtocol result = player.target(position);
        assertEquals("Should onBoardChanged at (3,4)", TargetProtocol.HIT, result);
        assertEquals("Should update as onBoardChanged at (3,4)", TargetProtocol.HIT, player.getValue(position));
        result = player.target(position);
        assertEquals("Should be already checked at (3,4)", TargetProtocol.INVALID, result);

        position = new Position(7, 7);
        result = player.target(position);
        assertEquals("Should miss at (7,7)", TargetProtocol.MISS, result);
        assertEquals("Should update as miss at (7,7)", TargetProtocol.MISS, player.getValue(position));
        result = player.target(position);
        assertEquals("Should be already checked at (7,7)", TargetProtocol.INVALID, result);

        result = player.target(new Position(100, 100));
        assertEquals("Should be invalid pos at (100,100)", TargetProtocol.INVALID, result);
    }

    @Test
    public void configBoard() throws Exception {
        assertEquals("Head at (3, 5)", TargetProtocol.PLANE, player.getValue(new Position(3, 5)));
        assertEquals("Between wings is at (3, 4)", TargetProtocol.PLANE, player.getValue(new Position(3, 4)));
        assertEquals("WingUp 1x is at (2, 4)", TargetProtocol.PLANE, player.getValue(new Position(2, 4)));
        assertEquals("WingUp 2x is at (1, 4)", TargetProtocol.PLANE, player.getValue(new Position(1, 4)));
        assertEquals("WingDown 1x is at (4, 4)", TargetProtocol.PLANE, player.getValue(new Position(4, 4)));
        assertEquals("WingDown 2x is at (5, 4)", TargetProtocol.PLANE, player.getValue(new Position(4, 4)));
        assertEquals("Body is at (3, 3)", TargetProtocol.PLANE, player.getValue(new Position(3, 3)));
        assertEquals("Tail is at (3, 2)", TargetProtocol.PLANE, player.getValue(new Position(3, 2)));
        assertEquals("TailUp is at (2, 2)", TargetProtocol.PLANE, player.getValue(new Position(2, 2)));
        assertEquals("TailDown is at (4, 2)", TargetProtocol.PLANE, player.getValue(new Position(4, 2)));
    }

    @Test
    public void testRotation() throws  Exception{

        InteractivePlayer iplayer = new InteractivePlayer();
        iplayer.configBoard(new PlaneConfiguration(0,new Position(3, 5), Orientation.EST));

        iplayer.rotate();
        assertEquals("Head at (4, 4)", TargetProtocol.PLANE, player.getValue(new Position(4, 4)));
        assertEquals("Between wings is at (3, 4)", TargetProtocol.PLANE, player.getValue(new Position(3, 4)));
        assertEquals("WingUp 1x is at (3, 3)", TargetProtocol.PLANE, player.getValue(new Position(3, 3)));
        assertEquals("WingUp 2x is at (3, 2)", TargetProtocol.PLANE, player.getValue(new Position(3, 2)));
        assertEquals("WingDown 1x is at (3, 4)", TargetProtocol.PLANE, player.getValue(new Position(3, 4)));
        assertEquals("WingDown 2x is at (3, 5)", TargetProtocol.PLANE, player.getValue(new Position(3, 5)));
        assertEquals("Body is at (2, 4)", TargetProtocol.PLANE, player.getValue(new Position(2, 4)));
    }

    @Test
    public void testNewConfiguration(){
        PlaneConfiguration planeConfiguration  = new PlaneConfiguration(0,new Position(2, 2), Orientation.NORTH);
        player.configBoard(planeConfiguration);
        assertEquals("Head at (0, 2)", TargetProtocol.PLANE_HEAD, player.getValue(new Position(0, 2)));

        player.reset();
        planeConfiguration  = new PlaneConfiguration(0,new Position(2, 2), Orientation.SOUTH);
        player.configBoard(planeConfiguration);
        assertEquals("Head at (4, 2)", TargetProtocol.PLANE_HEAD, player.getValue(new Position(4, 2)));
    }

}