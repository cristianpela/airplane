package org.criskey.airplane.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 1/9/2016.
 */
public class LobbyTest {

    Lobby lobby;

    @Before
    public void setUp() throws Exception {
        lobby = new Lobby();
    }

    @Test
    public void join() throws Exception {
        assertTrue(lobby.join("Adam", null));
        assertFalse(lobby.join("Adam", null));//already joined
    }

    @Test
    public void leave() throws Exception {
        lobby.join("Adam", null);
        assertTrue(lobby.leave("Adam"));
        assertFalse(lobby.leave("Adam"));
    }

    @Test
    public void invite() throws Exception {
        lobby.join("Adam", null);
        lobby.join("Eve", null);

        assertTrue(lobby.invite("Adam", "Eve"));
        assertFalse("Already invited", lobby.invite("Adam", "Eve"));

        lobby.clearInvitation("Adam", "Eve");
        assertTrue(lobby.invite("Adam", "Eve"));
        assertFalse("Already invited", lobby.invite("Eve", "Adam"));
    }

    @Test
    public void getUzers() throws Exception {

    }

}