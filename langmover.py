import xml.etree.cElementTree as etree
import glob
import os
import errno
import re

REGEX_PATTERN = re.compile(r"\{[0-9]+\}")
REGEX_NUMBER_SUBPATTERN = re.compile(r"\{1,number,#*.#+\}")
LANG_FILES = [f.replace('\\', '/') for f in glob.glob("./airplane-desktop/strings*.properties")]
DESTINATION_PATH = "./airplane-android/app/src/main/res/"

def get_file_name_from_path(path):
    return os.path.basename(path).split('.')[0]

def create_dir_name_from_file_name(file_name):
    lang = "-" + file_name.split("_")[1]  if "_" in file_name else ""
    return "values" + lang

def parse(dir_name, content):
    resources = etree.Element("resources")
    for cnt in content:
        key, value = [_c.strip() for _c in cnt.decode().split("=")]
        etree.SubElement(resources, "string", name=key).text = parse_value(value)
    tree = etree.ElementTree(resources)
    make_sure_dir_exists(dir_name)
    tree.write(dir_name + "/strings.xml", encoding="utf-8", xml_declaration=True)

#Right now this is paring only strings and integers. TODO: assure floating numbers and date are supported
def parse_value(value):
    # closure hack
    count = [0]
    def replace_lambda(matched):
        count[0] += 1
        matched = matched.group()
        if matched == "{0}":
            matched = "%" + str(count[0]) + "$s"
        elif matched == "{1,number,integer}":
            matched = "%" + str(count[0]) + "$d"
        return matched
    return REGEX_PATTERN.sub(replace_lambda, value)
    

def make_sure_dir_exists(dir_name):
    try:
        os.makedirs(dir_name)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

for path in LANG_FILES[:3]:
    with open(path) as f:
        _content = f.readlines()
    f_name = get_file_name_from_path(path)
    dir_name = create_dir_name_from_file_name(f_name)
    print "Exporting : ", f_name, " to ", DESTINATION_PATH + dir_name
    parse(DESTINATION_PATH + dir_name, _content)



